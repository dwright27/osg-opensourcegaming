﻿var names = [];
var currentDisplay = 0;

$(function () {
    // Reference the auto-generated proxy for the hub.  
    var chat = $.connection.chatHub;
    // Create a function that the hub can call back to display messages.
    chat.client.addNewMessageToPage = function (name, message) {
        // Add the message to the page. 
        var found = false;
        for (var i = 0; i < names.length && !found; i++) {
            if (names[i] == name) {
                $('#discussion' + name).append('<li><strong>' + htmlEncode(name)
                     + '</strong>: ' + htmlEncode(message) + '</li>');
                found = true;
            }
        }

        if (!found) {
            add(name);
            $('#discussion' + name).append('<li><strong>' + htmlEncode(name)
                     + '</strong>: ' + htmlEncode(message) + '</li>');
        }


    };


    function add(name) {

        var container = $('#messageContainer');

        container.append('<input type="hidden" value=displayname' + name + '/>')
        $('#displayname' + name).val(name);
        names.push(name);
        container.append('<ul id=discussion' + name + '></ul>')
        container.append('<input type="text" id=' + name + 'message' + ' value=""' + ' />')
        container.append('<input type="button" id=' + name + 'sendmessage' + ' value="' + name + '"/>')
        $('#message' + name).focus();

        //<input type="button" id="sendmessage" value="Send" />

        $('#' + name + 'sendmessage').click(function () {
            // Call the Send method on the hub. 
            var name = $(this).val();
            $('#discussion' + name).append('<li><strong>' + "Me"
                    + '</strong>: ' + $('#' + name + 'message').val() + '</li>');
            chat.server.send(name, $('#' + name + 'message').val());
            // Clear text box and reset focus for next comment. 
            $('#' + name + 'message').val('');
            $('#' + name + 'message').focus();
        });
    }
    // Get the user name and store it to prepend to messages.
    var name = prompt('Enter name:', '');
    add(name);
    // Set initial focus to message input box.  

    // Start the connection.
    $.connection.hub.start().done(function () {
        //$('#sendmessage').click(function () {
        //    // Call the Send method on the hub. 
        //    chat.server.send($('#displayname').val(), $('#message').val());
        //    // Clear text box and reset focus for next comment. 
        //    $('#message').val('').focus();
        //});

        $('#addPerson').click(function () {
            // var container = $('#container');

            var name = prompt('Enter name:', '');
            add(name);
            //container.append('<input type="hidden" value=displayname' +name +'/>')
            //$('#displayname' + name).val(name);
            //names.push(name);
            //container.append('<input type="text" id=' + name +'message' + ' value=""/>')
            //container.append('<input type="button" id='+name + 'sendmessage'+' value=' + name + '/>')


            ////<input type="button" id="sendmessage" value="Send" />

            //$('#'+name +'sendmessage').click(function () {
            //    // Call the Send method on the hub. 
            //    chat.server.send($(this).val(), $('#'+this.val()+ 'message').val());
            //    // Clear text box and reset focus for next comment. 
            //    $('#' + this.val() + 'message').val('').focus();
            //});

        });
    });
});

function htmlEncode(value) {

    var encodedValue = $('<div/>').text(value).html();//('<li>' + value + '</li>');
    return encodedValue;
}