﻿using OSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSG.DAL
{
    public class ProfileRepository : IRepository<Profile>
    {
        public IEnumerable<Profile> All()
        {
            using (var db = new OSGContext())
            {
                return db.Profiles.ToList();
            }
        }

        public Profile Find(int id)
        {
            using (var db = new OSGContext())
            {
                return db.Profiles.Find(id);

            }
        }

        public void Insert(Profile insert)
        {
            using (var db = new OSGContext())
            {
                if (insert.ProfileID == default(int))
                {
                    // New entity
                    db.Profiles.Add(insert);
                }
                else
                {
                    // Existing entity
                    db.Entry(insert).State = System.Data.Entity.EntityState.Modified;
                }

                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var db = new OSGContext())
            {
                var info = db.Profiles.Find(id);
                db.Profiles.Remove(info);
                db.SaveChanges();
            }
        }

        public void Update(Profile update)
        {
            using (var db = new OSGContext())
            {

                // Existing entity
                db.Entry(update).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}