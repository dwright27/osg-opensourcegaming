﻿using OSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSG.DAL
{
    public class PostRepository : IRepository<Post>
    {
        public IEnumerable<Post> All()
        {
            using (var db = new OSGContext())
            {
                return db.Posts.ToList();
            }
        }

        public Post Find(int id)
        {
            using (var db = new OSGContext())
            {
                return db.Posts.Find(id);

            }
        }

        public void Insert(Post insert)
        {
            using (var db = new OSGContext())
            {
                if (insert.Id == default(int))
                {
                    // New entity
                    db.Posts.Add(insert);
                }
                else
                {
                    // Existing entity
                    db.Entry(insert).State = System.Data.Entity.EntityState.Modified;
                }

                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var db = new OSGContext())
            {
                var info = db.Posts.Find(id);
                db.Posts.Remove(info);
                db.SaveChanges();
            }
        }

        public void Update(Post update)
        {
            using (var db = new OSGContext())
            {

                // Existing entity
                db.Entry(update).State = System.Data.Entity.EntityState.Modified;

                db.SaveChanges();
            }
        }
    }
}