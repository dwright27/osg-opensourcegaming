﻿using OSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSG.DAL
{
    public class GameRepository : IRepository<Game>
    {
        public IEnumerable<Game> All()
        {
            using (var db = new OSGContext())
            {
                return db.Games.ToList();
            }
        }

        public Game Find(int id)
        {
            using (var db = new OSGContext())
            {
                return db.Games.Find(id);

            }
        }

        public void Insert(Game insert)
        {
            using (var db = new OSGContext())
            {
                if (insert.Id == default(int))
                {
                    // New entity
                    db.Games.Add(insert);
                }
                else
                {
                    // Existing entity
                    db.Entry(insert).State = System.Data.Entity.EntityState.Modified;
                }

                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var db = new OSGContext())
            {
                var info = db.Games.Find(id);
                db.Games.Remove(info);
                db.SaveChanges();
            }
        }

        public void Update(Game update)
        {
            using (var db = new OSGContext())
            {

                // Existing entity
                db.Entry(update).State = System.Data.Entity.EntityState.Modified;

                db.SaveChanges();
            }
        }
    }
}