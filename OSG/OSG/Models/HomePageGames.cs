﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OSG.Models
{
    public class HomePageGames
    {
        public IEnumerable<OSG.Models.Game> MostPlayed { get; set; }
        public IEnumerable<OSG.Models.Game> TopRated { get; set; }
        public IEnumerable<OSG.Models.Game> NewGames { get; set; }
        public IEnumerable<OSG.Models.Game> FeaturedGames { get; set; }
        
    }
}
