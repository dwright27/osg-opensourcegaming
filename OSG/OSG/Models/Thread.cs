﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OSG.Models
{
    [Table("Thread")]
    public class Thread
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string TopicName{get;set;}
        [Required]
        public DateTime Started { get; set; }
        [Required]
        public DateTime LastModified { get; set; } 
    }
}