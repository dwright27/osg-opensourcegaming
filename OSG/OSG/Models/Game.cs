﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OSG.Models
{
    public partial class Game
    {
        private static float FEATURED_RATING = 4.7f;
        private static float FEATURED_MONTHS_MAX = 3;
        private static float FEATURED_MONTHS_MIN = 1;

        public Game()
        {
            Tags = new List<Tag>();
        }
        [Key]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]

        public DateTime ReleaseDate { get; set; }
        [Required]

        public float Rating { get; set; }

        [Required]

        public int NumberOfRatings { get; set; }

        [Required]

        public List<Tag> Tags { get; set; }

        [Required]
        public int PlayCount { get; set; }

        [Required]
        public string Instructions { get; set; }

        public bool Featured
        {
            get
            {
                int numberOfMonths =ReleaseDate.Month - DateTime.Today.Month;
                return (ReleaseDate.Year == DateTime.Today.Year && Rating >= FEATURED_RATING &&
                        numberOfMonths >= FEATURED_MONTHS_MIN && numberOfMonths <= FEATURED_MONTHS_MAX );
            }
        }
    }
}