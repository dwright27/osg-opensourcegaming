﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OSG.Models
{
    [Table("Posts")]
    public class Post
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int ThreadId { get; set; }
        [Required]
        public DateTime PostDate { get; set; }
        [Required]
        public string Message { get; set; }

    }
}