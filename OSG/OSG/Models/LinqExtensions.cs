﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSG.Models
{
    public static class LinqExtensions
    {
        public static bool Contains<T>(this IEnumerable<T> enumerable, IEnumerable<T> list)
        {
            bool contains = true;

            foreach (var item in list)
            {
                contains = enumerable.Contains(item);
                if (!contains) break;
            }


            return contains;
        }
    }
}