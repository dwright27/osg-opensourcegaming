﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSG.Models
{
    public class ForumTopic
    {
        private static int topicNumber = 0;

        public ForumTopic()
        {
            Id = topicNumber++;
            SubTopics = new List<ForumTopic>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public List<ForumTopic> SubTopics { get; set; } 
        public List<ForumPost> Posts { get; set; }

       
    }
}