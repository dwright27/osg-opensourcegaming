﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSG.ViewModel
{
    public class ThreadVM
    {
        public int threadID { get; set; }
        public string topicName { get; set; }
        public DateTime dateStarted { get; set; }
        public DateTime dateLastModified { get; set; }
        public List<PostVM> threadPosts { get; set; }
    }
}