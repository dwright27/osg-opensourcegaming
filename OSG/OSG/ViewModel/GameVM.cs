﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSG.ViewModel
{
    public class GameVM
    {
        public int gameID { get; set; }
        public string Title { get; set; }
        public string creater { get; set; }
        public List<AchievementsVM> possibleAchievements { get; set; }
        public float rating { get; set; }
        public DateTime releaseDate { get; set; }
        public string description { get; set; }
        public string Tag { get; set; }
    }
}