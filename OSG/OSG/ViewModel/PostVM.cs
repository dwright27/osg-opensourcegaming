﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSG.ViewModel
{
    public class PostVM
    {
        public string message { get; set; }
        public int postID { get; set; }
        public int threadID { get; set; }
        public DateTime datePut { get; set; }
        public int posterUserID { get; set; }
    }
}