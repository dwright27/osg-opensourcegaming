﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSG.ViewModel
{
    public class ProfileVM
    {
        public int profileID { get; set; }
        public int userID { get; set; }
        public string AboutMe { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
    }
}