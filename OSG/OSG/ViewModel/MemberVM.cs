﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSG.ViewModel
{
    public class MemberVM
    {
        public string userName { get; set; }
        public DateTime birthDate { get; set; }
        public string Email { get; set; }
        public List<MemberVM> friends { get; set; }
        public List<ConversationsVM> userConversations { get; set; }
        public List<GameVM> gamesPlayed { get; set; }
        public int profileID { get; set; }
    }
}