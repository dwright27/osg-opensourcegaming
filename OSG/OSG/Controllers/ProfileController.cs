﻿using OSG.DAL;
using OSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OSG.Controllers
{
    public class ProfileController : Controller
    {
        //
        // GET: /Profile/

        public ActionResult ProfilePage()
        {
            UserRepository repo = new UserRepository();
            User temp = repo.Find(User.Identity.Name);

            return View(temp.Id);
        }

    }
}
