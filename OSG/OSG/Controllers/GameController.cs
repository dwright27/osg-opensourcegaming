﻿using OSG.DAL;
using OSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OSG.Controllers
{
    public class GameController : Controller
    {
        //
        // GET: /Game/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Display(int gameId)
        {
            GameRepository games = new GameRepository();
            return View(games.Find(gameId));
        }

        public ActionResult DisplayTagged(Tag tag)
        {
            GameRepository games = new GameRepository();
            return View(games.All().Where(g => g.Tags.Contains(tag)));
        }

    }
}
