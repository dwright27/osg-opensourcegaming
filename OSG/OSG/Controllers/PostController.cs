﻿using OSG.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OSG.Controllers
{
    public class PostController : Controller
    {
        //
        // GET: /Post/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Display(int threadId)
        {
            PostRepository postsRepo = new PostRepository(); 
            return View(postsRepo.All().Where(p=>p.ThreadId == threadId));
        }

    }
}
