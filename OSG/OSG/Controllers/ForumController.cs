﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using OSG.Models;

namespace OSG.Controllers
{
    public class ForumController : Controller
    {
        //
        // GET: /Forum/

        private static List<ForumTopic> topics = new List<ForumTopic>
        {
            new ForumTopic
            {
                Message = "Bugs Everywhere",
                Title = "IT",
                SubTopics = new List<ForumTopic>
                {
                    new ForumTopic
                    {
                        Title = "Gaming Crisis",
                        Message = "I hate buggy games",
                        Posts =
                            new List<ForumPost>
                            {
                                new ForumPost
                                {
                                    Message = "Hate Bugs",
                                    SubPosts = new List<ForumPost> {new ForumPost {Message = "Me Too"}}
                                }
                            }
                    }
                },

            },
            

            new ForumTopic
            {
                Message = "Brain Storming At Its Finest",
                Title = "Game Ideas",
                SubTopics = new List<ForumTopic>
                {
                    new ForumTopic
                    {
                        Title = "Happy Land",
                        Message = "Floating with the best of them",
                        Posts =
                            new List<ForumPost>
                            {
                                new ForumPost
                                {
                                    Message = "Loving it",
                                    SubPosts = new List<ForumPost> {new ForumPost {Message = "Me Too"}}
                                }
                            }
                    }
                },

            }
        };
            


        public ActionResult Index()
        {
            return View("ForumIndex", topics);
        }


        public ForumTopic FindTopic(IEnumerable<ForumTopic> topicList ,int id)
        {
            ForumTopic foundTopic = null;
            if(topicList.Any())
            foreach (var topic in topicList)
            {
                if (topic.Id == id)
                {
                    foundTopic = topic;
                    break;
                }
                if (topic.SubTopics.Any())
                {
                    foundTopic = FindTopic(topic.SubTopics, id);
                    if (foundTopic != null)
                    {
                        break;
                    }
                }
            }

            return foundTopic;
        }

        public ActionResult DisplayPosts(int mainTopicId)
        {
            ForumTopic topic = topics.FirstOrDefault(p => p.Id == mainTopicId) ?? FindTopic(topics,mainTopicId);


            List<ForumPost> posts = null;//topic.Posts;
            if (topic != null)
            {
                posts = topic.Posts;
                ViewBag.PanelTitle = topic.Title;
            }
            return View("DisplayPosts", posts);
        }

        public ActionResult DisplaySubTopics(int mainTopicId)
        {
            return View("DisplaySubTopics", topics.FirstOrDefault(t=>t.Id == mainTopicId));
        }
    }
}
