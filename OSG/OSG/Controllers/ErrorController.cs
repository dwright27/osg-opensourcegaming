﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OSG.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/

        public ActionResult Index(int statusCode, Exception e)
        {
            Response.StatusCode = statusCode;
            string message = "Whoops something went wrong...";
            
            
            switch (Response.StatusCode)
            {
                case 404:
                    message = "Sorry the page you are looking for either does not exist, or has moved.";
                    break;
            }
             
            
            return View(model: message);
            //return View();
        }

    }
}
