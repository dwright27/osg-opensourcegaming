﻿using OSG.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OSG.Controllers
{
    public class ThreadController : Controller
    {
        //
        // GET: /Thread/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Display()
        {
            ThreadRepository threadRepo = new ThreadRepository();
            return View(threadRepo.All());
        }

    }
}
