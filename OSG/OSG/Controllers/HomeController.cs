﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OSG.Models;

namespace OSG.Controllers
{
    public class HomeController : Controller
    {                                                                  
        //
        // GET: /Home/

        private const int NUM_GAMES_PER_SECTION = 1;
        static List<OSG.Models.Game> games = new List<OSG.Models.Game>
        {
            new OSG.Models.Game{Rating = 4.5f,ReleaseDate = new DateTime(2014,2,27),Title = "Bioshock" ,Tags =new List<Tag>{ Tag.Action}, PlayCount = 7354},
            new OSG.Models.Game{Rating = 2.5f,ReleaseDate = new DateTime(2014,2,27),Title = "Gears of War" , Tags =new List<Tag>{Tag.Action}, PlayCount = 1232},
            new OSG.Models.Game{Rating = 3.9f,ReleaseDate = new DateTime(2014,2,27),Title = "GTA" ,Tags =new List<Tag>{Tag.Classic}, PlayCount = 2000},
            new OSG.Models.Game{Rating = 4.2f,ReleaseDate = new DateTime(2014,2,27),Title = "GTA 5" , Tags =new List<Tag>{Tag.Action}, PlayCount = 275},
            new OSG.Models.Game{Rating = 3.1f,ReleaseDate = new DateTime(2014,2,27),Title = "Kingdom Hearts" , Tags =new List<Tag>{Tag.Puzzle},PlayCount = 400},
            new OSG.Models.Game{Rating = 4.8f,ReleaseDate = new DateTime(2014,2,27),Title = "Snake" ,Tags =new List<Tag>{Tag.Classic} , PlayCount = 27},
            new OSG.Models.Game{Rating = 4.75f,ReleaseDate = new DateTime(2014,2,27),Title = "Pong", Tags =new List<Tag>{Tag.Classic}, PlayCount = 50},
            new OSG.Models.Game{Rating = 2.85f,ReleaseDate = new DateTime(2014,2,27),Title = "Swap Image", Tags = new List<Tag>{Tag.Puzzle },PlayCount = 500},
            new OSG.Models.Game{Rating = 3.7f,ReleaseDate = new DateTime(2014,2,27),Title = "Stay Between the lines", Tags =new List<Tag>{Tag.Puzzle}, PlayCount = 5000},
        }; 

        static HomePageGames homeGames = new HomePageGames
        {
            MostPlayed = games.OrderByDescending(g => g.PlayCount).Take(5),
            TopRated = games.OrderByDescending(g => g.Rating).Take(5),
            NewGames = games.OrderByDescending(g => g.ReleaseDate).Take(5),
            FeaturedGames =  games.Where(g => g.Featured).Take(5),
        };

        [AllowAnonymous]
        public ActionResult Index()
        {
            //User.Identity.IsAuthenticated
            //int  i = User.Identity.Name;

            return View(homeGames);
        }

        public ActionResult DisplayGame(int gameId)
        {
            OSG.Models.Game returnGame = games.FirstOrDefault(g => g.Id == gameId);
            return (returnGame == null) ? View("~/Views/Error/Index.cshtml", null)  : View(model: returnGame);
        }
        
        public ActionResult TypeOfGames(string panelTitle, Tag tag)
        {
            ViewBag.Title = panelTitle;
            ViewBag.PanelTitle = panelTitle;
            return View("AllGames", games.Where(g => g.Tags.Contains(tag)));
        }

        public ActionResult MostPlayed()
        {
            ViewBag.Title = "MostPlayed";
            ViewBag.PanelTitle = "Most Played";
            return View("AllGames", games.OrderByDescending(g => g.PlayCount));
        }

        public ActionResult TopRated()
        {
            ViewBag.Title = "TopRated";
            ViewBag.PanelTitle = "Top Rated";
            return View("AllGames", games.OrderByDescending(g => g.Rating));
        }

        public ActionResult NewGames()
        {
            ViewBag.Title = "NewGames";
            ViewBag.PanelTitle = "New Games";
            return View("AllGames", games.OrderByDescending(g => g.ReleaseDate));
        }



        [HttpPost]
        public ActionResult SearchGames()
        {
            IEnumerable<OSG.Models.Game> gamesReturned = null;
            string title = Request.Params["gameTitle"];
            if (title != null)
            {
                ViewBag.Link = "Index";
                ViewBag.PanelTitle = "Games Found";
                 gamesReturned = games.Where(g => g.Title.ToLower().Contains(title.ToLower().Trim()));
            }

            return (gamesReturned == null) ? View("GameNotFound") : View("AllGames", gamesReturned);
        }
    }
}
