﻿using OSG.Models;
using OSG.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace OSG.Controllers
{
    public class ConversationController : Controller
    {
        ConversationRepository CR = new ConversationRepository();

        public ActionResult UserConversations(int id)
        {
            List<Conversation> send = new List<Conversation>();
            foreach (var pick in CR.All())
            {
                if(pick.fromUserID==id || pick.toUserID==id)
                {
                    //this is where you would do view model stuff
                    send.Add(pick);
                }
            }

            return View(send);
        }
    }
}
