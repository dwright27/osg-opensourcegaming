﻿

var StartRectangle = {
    x: 25,
    y: 25,
    width: 40,
    height: 40
};

var EndRectangle = {
    x: 200,
    y: 25,
    width: 40,
    height: 40
};


var KEY = {
    UP: 38,
    DOWN: 40,

};

var mouse = {
    x: 0,
    y: 0,
    width: 5,
    height: 5
};

var levelWidth = 400;

//var movingRectangle = {
//    x: 0,
//    y: 100,
//    width: 20,
//    height
//};

var level = 0;
var canvas = document.getElementById('meCanvas');

var co = canvas.getContext('2d');
setInterval(onTimerTick, 33); // 33 milliseconds = ~ 30 frames per sec
var startedPlaying = false;


$(function () {
    draw();
});

function onTimerTick() {
    update();
    draw();


}

function drawLevel() {
    if (level == 0) {
        drawLevelZero();
    }
    else if (level == 1) {
        drawLevelOne();
    }
    else if (level == 2)
    {
        drawLevelTwo();
    }
    else if (level >= 3)
    {
        co.drawImage(document.getElementById('YOUWIN'), levelWidth/2 - 100, levelWidth/2 -100);



        //canvasw.style.textAlign = 'center';
    }

}



function isPlayerInLevel() {
    var isInLevel = false;
    if (level == 0) {
        isInLevel = isPlayerInLevelZero();
    }
    if (level == 1) {
        isInLevel = isPlayerInLevelOne();
    }
    if (level == 2)
    {
        isInLevel = isPlayerInLevelTwo();
    }

    return isInLevel;
}


document.addEventListener('keydown', function (event) {
    if (event.keyCode == 37) {
        level -= 2;
        changeLevel();
    }
    else if (event.keyCode == 39) {
        changeLevel();
    }
});


function update() {
    levelUpdate();
    if (!startedPlaying) {
        startedPlaying = isPlayerInStartZone();
        if (startedPlaying)
        {
            document.getElementById('meCanvas').style.backgroundColor = 'lightblue';
        }
    }
    else {
        startedPlaying = (isPlayerInStartZone() || isPlayerInEndZone() || isPlayerInLevel());
        if (!startedPlaying)
        {
            document.getElementById('meCanvas').style.backgroundColor = 'red';
        }
    }
    
    if (startedPlaying && isPlayerInEndZone()) {
        //You have Won
        changeLevel();
    }
    //document.getElementById("Tester").textContent = ' ' + StartRectangle.x + ' ' + mouse.x;
}

function levelUpdate()
{

    if (level == 1) {
        levelOneUpdate();
    }
    if (level == 2)
    {
        levelTwoUpdate();
    }
}



function changeLevel() {
    level++;
    startedPlaying = false;
    if (level == 1) {
        document.getElementById("GameIDInput").setAttribute("Value", 5);
        document.getElementById("AchievementInput").setAttribute("Value", "Level2");
        document.getElementById("AchieveDescriptionInput").setAttribute("Value", "Made it to Level2");
        document.getElementById("AchievePointsInput").setAttribute("Value", 2);
        AchievementUnlocked();
        
        //document.getElementById("editPost").click();

        levelOneSetup();
    }
    if (level == 2)
    {
        document.getElementById("GameIDInput").setAttribute("Value", 5);
        document.getElementById("AchievementInput").setAttribute("Value", "Level3");
        document.getElementById("AchieveDescriptionInput").setAttribute("Value", "Almost there");
        document.getElementById("AchievePointsInput").setAttribute("Value", 3);
        AchievementUnlocked();


        levelTwoSetup();
    }
    else if (level == 3)
    {
        document.getElementById("GameIDInput").setAttribute("Value", 5);
        document.getElementById("AchievementInput").setAttribute("Value", "WinnerWinner");
        document.getElementById("AchieveDescriptionInput").setAttribute("Value", "You won the game. And Lost the game");
        document.getElementById("AchievePointsInput").setAttribute("Value", 4);
        AchievementUnlocked();

    }
}



//#region LevelZero
function levelZeroSetup()
{
    
}

function drawLevelZero() {
    co.beginPath();
    co.fillStyle = 'blue';
    co.moveTo(StartRectangle.x + StartRectangle.width / 3, StartRectangle.y + StartRectangle.height - 3);
    co.lineTo(StartRectangle.x + StartRectangle.width / 3, 200);
    co.lineTo(EndRectangle.x + EndRectangle.width * (2 / 3), 130);
    co.lineTo(EndRectangle.x + EndRectangle.width * (2 / 3), EndRectangle.y + EndRectangle.height - 3);
    co.lineTo(EndRectangle.x + EndRectangle.width / 3, EndRectangle.y + EndRectangle.height - 3);
    co.lineTo(EndRectangle.x + EndRectangle.width / 3, 115);
    co.lineTo(StartRectangle.x + StartRectangle.width * (2 / 3), 180);
    co.lineTo(StartRectangle.x + StartRectangle.width * (2 / 3), StartRectangle.y + StartRectangle.height - 3);
    co.closePath();
    co.stroke();
    co.fill();
}


function isPlayerInLevelZero() {
    co.beginPath();
    co.fillStyle = 'blue';
    co.moveTo(StartRectangle.x + StartRectangle.width / 3, StartRectangle.y + StartRectangle.height - 3);
    co.lineTo(StartRectangle.x + StartRectangle.width / 3, 200);
    co.lineTo(EndRectangle.x + EndRectangle.width * (2 / 3), 130);
    co.lineTo(EndRectangle.x + EndRectangle.width * (2 / 3), EndRectangle.y + EndRectangle.height - 3);
    co.lineTo(EndRectangle.x + EndRectangle.width / 3, EndRectangle.y + EndRectangle.height - 3);
    co.lineTo(EndRectangle.x + EndRectangle.width / 3, 115);
    co.lineTo(StartRectangle.x + StartRectangle.width * (2 / 3), 180);
    co.lineTo(StartRectangle.x + StartRectangle.width * (2 / 3), StartRectangle.y + StartRectangle.height - 3);
    co.closePath();

    return (co.isPointInPath(mouse.x - mouse.width / 2, mouse.y - mouse.height / 2) || (co.isPointInPath(mouse.x + mouse.width / 2, mouse.y + mouse.height / 2)));
}

//#endregion

//#region LevelOne
var LevelOneVelocity = 1;

function levelOneSetup() {
    StartRectangle.x = EndRectangle.x;
    StartRectangle.y = EndRectangle.y;
    EndRectangle.x = 5;
    EndRectangle.y = 0;

}


function levelOneUpdate() {
    EndRectangle.y += LevelOneVelocity;
    if (EndRectangle.y > 200||EndRectangle.y<0)
    {
        LevelOneVelocity = -LevelOneVelocity;
    }
}

function isPlayerInLevelOne() {
    co.beginPath();
    co.moveTo(StartRectangle.x + StartRectangle.width, StartRectangle.y + StartRectangle.height / 3);
    co.lineTo(StartRectangle.x + StartRectangle.width + 20, StartRectangle.y + StartRectangle.height / 3);
    co.lineTo(StartRectangle.x + StartRectangle.width + 20, EndRectangle.y + EndRectangle.height + 40);
    co.lineTo(EndRectangle.x + EndRectangle.width / 3, EndRectangle.y + EndRectangle.height + 100);
    co.lineTo(EndRectangle.x + EndRectangle.width / 3, EndRectangle.y + EndRectangle.height);

    co.lineTo(EndRectangle.x + EndRectangle.width * 2 / 3, EndRectangle.y + EndRectangle.height);

    co.lineTo(EndRectangle.x + EndRectangle.width * 2 / 3, EndRectangle.y + EndRectangle.height + 80);
    co.lineTo(StartRectangle.x + StartRectangle.width + 10, EndRectangle.y + EndRectangle.height + 30);
    co.lineTo(StartRectangle.x + StartRectangle.width + 10, StartRectangle.y + StartRectangle.height * (2 / 3));
    co.lineTo(StartRectangle.x + StartRectangle.width, StartRectangle.y + StartRectangle.height * (2 / 3));
    co.closePath();

    return (co.isPointInPath(mouse.x - mouse.width / 2, mouse.y - mouse.height / 2) || (co.isPointInPath(mouse.x + mouse.width / 2, mouse.y + mouse.height / 2)));
}


function drawLevelOne() {
    co.beginPath();
    co.fillStyle = 'black';
    co.moveTo(StartRectangle.x + StartRectangle.width, StartRectangle.y + StartRectangle.height/3);
    co.lineTo(StartRectangle.x + StartRectangle.width +20, StartRectangle.y + StartRectangle.height / 3);
    co.lineTo(StartRectangle.x + StartRectangle.width + 20, EndRectangle.y + EndRectangle.height + 40);
    co.lineTo(EndRectangle.x+EndRectangle.width/3, EndRectangle.y + EndRectangle.height + 100);
    co.lineTo(EndRectangle.x + EndRectangle.width / 3, EndRectangle.y + EndRectangle.height);

    co.lineTo(EndRectangle.x + EndRectangle.width * 2 / 3, EndRectangle.y + EndRectangle.height);

    co.lineTo(EndRectangle.x + EndRectangle.width*2/3, EndRectangle.y + EndRectangle.height + 80);
    co.lineTo(StartRectangle.x + StartRectangle.width + 10, EndRectangle.y + EndRectangle.height + 30);
    co.lineTo(StartRectangle.x + StartRectangle.width + 10, StartRectangle.y + StartRectangle.height *(2/3));
    co.lineTo(StartRectangle.x + StartRectangle.width, StartRectangle.y + StartRectangle.height*(2/3));
    co.closePath();
    co.stroke();
    co.fill();
}
//#endregion

//#region LevelTwo
var LevelTwoYVelocity = 3.0;
var LevelTwoXVelocity = 2.0;

function levelTwoSetup() {
    StartRectangle.x = EndRectangle.x;
    StartRectangle.y = EndRectangle.y;
    EndRectangle.x = levelWidth-EndRectangle.width;
    EndRectangle.y = 0;

}


function levelTwoUpdate() {
    EndRectangle.y += LevelTwoYVelocity;
    EndRectangle.x += LevelTwoXVelocity;

    if (EndRectangle.y > levelWidth/2 || EndRectangle.y < 0) {
        LevelTwoYVelocity = -LevelTwoYVelocity;
    }

    if (EndRectangle.x < levelWidth * 2 / 3) {
        EndRectangle.x = levelWidth * 2 / 3;
        LevelTwoXVelocity = -LevelTwoXVelocity;
    }

    if (EndRectangle.x > levelWidth - (EndRectangle.width + 10))
    {
        EndRectangle.x = levelWidth - (EndRectangle.width + 11);
        LevelTwoXVelocity = -LevelTwoXVelocity;
    }
}

function isPlayerInLevelTwo() {
    co.beginPath();
    co.moveTo(StartRectangle.x + StartRectangle.width / 3, StartRectangle.y + StartRectangle.height);
    co.lineTo(StartRectangle.x + StartRectangle.width / 3, levelWidth * 2 / 3);

    co.lineTo(levelWidth / 3, levelWidth * 2 / 3);
    co.lineTo(levelWidth * 2 / 3 + 5, levelWidth * 2 / 3);
    co.lineTo(levelWidth * 2 / 3 + 5, levelWidth / 3);
    co.lineTo(levelWidth * 2 / 3 - 10, levelWidth / 3);
    co.lineTo(levelWidth * 2 / 3 - 10, levelWidth * 2 / 3 - 10);

    co.lineTo(StartRectangle.x + StartRectangle.width * 2 / 3, levelWidth * 2 / 3 - 10);
    co.lineTo(StartRectangle.x + StartRectangle.width * 2 / 3, StartRectangle.y + StartRectangle.height);
    co.closePath();

    return (co.isPointInPath(mouse.x - mouse.width / 2, mouse.y - mouse.height / 2) || (co.isPointInPath(mouse.x + mouse.width / 2, mouse.y + mouse.height / 2)));
}


function drawLevelTwo() {
    co.beginPath();
    co.fillStyle = 'green';
    co.moveTo(StartRectangle.x + StartRectangle.width / 3, StartRectangle.y + StartRectangle.height);
    co.lineTo(StartRectangle.x + StartRectangle.width / 3, levelWidth * 2 / 3);

    co.lineTo(levelWidth / 3, levelWidth * 2 / 3);
    co.lineTo(levelWidth * 2 / 3 + 5, levelWidth * 2 / 3);
    co.lineTo(levelWidth * 2 / 3 + 5, levelWidth / 3);
    co.lineTo(levelWidth * 2 / 3 -10, levelWidth / 3);
    co.lineTo(levelWidth * 2 / 3 - 10, levelWidth *2/ 3-10);

    co.lineTo(StartRectangle.x + StartRectangle.width * 2 / 3, levelWidth * 2 / 3-10);
    co.lineTo(StartRectangle.x + StartRectangle.width * 2 / 3, StartRectangle.y + StartRectangle.height);
    co.closePath();
    co.stroke();
    co.fill();
}

//#endregion

//#region ThingsThatWorkThat we Dont worry about




function draw() {

    if (canvas.getContext) {
        canvas.width = levelWidth;
        canvas.height = levelWidth;
        drawLevel();
        if (level < 3) {
            drawStartBlock();

            co.drawImage(document.getElementById('StartImage'), StartRectangle.x, StartRectangle.y);

            drawEndBlock();
            co.drawImage(document.getElementById('EndImage'), EndRectangle.x, EndRectangle.y);
        }
        drawPlayer();
    }
}


document.addEventListener('mousemove', function (e) {

    if (e.offsetX) {
        mouse.x = e.offsetX;
        mouse.y = e.offsetY;
    }
    else if (e.layerX) {
        mouse.x = e.layerX;
        mouse.y = e.layerY;
    }
}, false);

function isPlayerInStartZone() {
    var result = false;
    co.beginPath();
    co.rect(StartRectangle.x, StartRectangle.y, StartRectangle.width, StartRectangle.height);
    co.closePath();
    result = (co.isPointInPath(mouse.x - mouse.width / 2, mouse.y - mouse.height / 2) || (co.isPointInPath(mouse.x + mouse.width / 2, mouse.y + mouse.height / 2)));

    return result ? true : false;
}

function isPlayerInEndZone() {

    co.beginPath();
    co.rect(EndRectangle.x, EndRectangle.y, EndRectangle.width, EndRectangle.height);
    co.closePath();
    var result = (co.isPointInPath(mouse.x - mouse.width / 2, mouse.y - mouse.height / 2) || (co.isPointInPath(mouse.x + mouse.width / 2, mouse.y + mouse.height / 2)));

    return result ? true : false;
}


function drawStartBlock() {

    co.beginPath();
    co.fillStyle = 'green';
    co.fillRect(StartRectangle.x, StartRectangle.y, StartRectangle.width, StartRectangle.height);
    co.closePath();
    co.stroke();
    co.fill();
}

function drawEndBlock() {
    co.beginPath();
    co.fillStyle = 'red';
    co.fillRect(EndRectangle.x, EndRectangle.y, EndRectangle.width, EndRectangle.height);
    co.closePath();
    co.stroke();
    co.fill();
}


function drawPlayer() {
    co.fillStyle = 'white';
    co.fillRect(mouse.x - mouse.width / 2, mouse.y - mouse.height / 2, mouse.width, mouse.height);
}
//#endregion