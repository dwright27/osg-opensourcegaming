﻿var names = [];
var currentDisplay = 0;

$(function () {
    // Reference the auto-generated proxy for the hub.  
    var chat = $.connection.chatHub;
    
    chat.client.updateList = function(names)
    {
        var list = $('#users');
        list.empty();
        for (var i = 0; i < names.length; i++)
            list.append('<li>' + names[i] + '</li>');
    };

    chat.client.addNewMessageToPage = function (name, message) {
        // Add the message to the page. 
        var found = false;
        for (var i = 0; i < names.length && !found; i++) {
            if (names[i] == name) {
                $('#discussion' + name).append('<li><strong>' + htmlEncode(name)
                     + '</strong>: ' + htmlEncode(message) + '</li>');
                found = true;
            }
        }

        if (!found) {
            add(name);
            $('#discussion' + name).append('<li><strong>' + htmlEncode(name)
                     + '</strong>: ' + htmlEncode(message) + '</li>');
        }


    };


    function add(name) {

        var container = $('#messageGroup');
      // var container = container.find(".panel-group");
        container.append(
            '<li class="pull-right " ><div class="panel panel-default"> <div class="panel-heading"> <h4 class="panel-title">  <a data-toggle="collapse" data-parent="#accordion" href="#collapse' +name + '"> ' +name +
        '</a> </h4> </div>' + 
    '<div id="collapse' + name + '" class="panel-collapse collapse in">' +
     ' <div class="panel-body">' +    ' </div> </div> </div></li>'

            );
        var innerPanel= container.find('#collapse'+name);
        innerPanel.append('<input type="hidden" value=displayname' + name + '/>')
        $('#displayname' + name).val(name);
        names.push(name);
        innerPanel.append('<ul id=discussion' + name + '></ul>')
        innerPanel.append('<input type="text" id=' + name + 'message' + ' value=""' + ' />')
        innerPanel.append('<input type="button" id=' + name + 'sendmessage' + ' value="' + name + '"/>')
        $('#message' + name).focus();

        //<input type="button" id="sendmessage" value="Send" />

        $('#' + name + 'sendmessage').click(function () {
            // Call the Send method on the hub. 
            var name = $(this).val();
            $('#discussion' + name).append('<li><strong>' + "Me"
                    + '</strong>: ' + $('#' + name + 'message').val() + '</li>');
            chat.server.send(name, $('#' + name + 'message').val());
            // Clear text box and reset focus for next comment. 
            $('#' + name + 'message').val('');
            $('#' + name + 'message').focus();
        });
    }
    // Get the user name and store it to prepend to messages.
    //var name = prompt('Enter name:', '');
    //add(name);
    // Set initial focus to message input box.  

    // Start the connection.
    $.connection.hub.start().done(function () {

        $('#addPerson').click(function () {

            var name = prompt('Enter name:', '');
            add(name);


        });
    });
});

function htmlEncode(value) {

    var encodedValue = $('<div/>').text(value).html();//('<li>' + value + '</li>');
    return encodedValue;
}