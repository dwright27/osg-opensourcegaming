﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectOSG.ViewModel
{
    public class GameVM
    {
        public int gameID { get; set; }
        public string Title { get; set; }
        public string creater { get; set; }
        public double rating { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime releaseDate { get; set; }
        public string description { get; set; }
    }
}