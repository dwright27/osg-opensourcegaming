﻿using ProjectOSG.DAL;
using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSG.ViewModel
{
    public class ProfileVM
    {
        public int profileID { get; set; }
        public int userID { get; set; }
        public string AboutMe { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<UserAchievement> Achievements { get; set; }
        public ProfileVM() { }

        public ProfileVM(Profile p)
        {
            profileID = p.ProfileID;
            userID = p.UserID;
            AboutMe = p.AboutMe;
            FirstName = p.FirstName;
            LastName = p.LastName;
            UserAchievementRepository temp = new UserAchievementRepository();
            Achievements = new List<UserAchievement>();
            Achievements = temp.FindUsersAchievs(userID);
        }

    }
}