﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSG.ViewModel
{
    public class ConversationsVM
    {
        public string subject { get; set; }
        public List<MessageVM> messages { get; set; }
        public int userIDTo { get; set; }
        public int userIDFrom { get; set; }
    }
}