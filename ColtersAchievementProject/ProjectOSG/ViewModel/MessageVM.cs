﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OSG.ViewModel
{
    public class MessageVM
    {
        public DateTime timeSent { get; set; }
        public string content { get; set; }
        public bool read { get; set; }
    }
}