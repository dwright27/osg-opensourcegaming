﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectOSG.Models;
using WebMatrix.WebData;

namespace ProjectOSG.Controllers
{
    public class HomeController : Controller
    {                                                                  
        //
        // GET: /Home/

        private const int NUM_GAMES_PER_SECTION = 1;

        [AllowAnonymous]
        public ActionResult Index()
        {
            HomePageGames homeGames = new HomePageGames
            {
                MostPlayed = GameController.getAllGames().OrderByDescending(g => g.PlayCount).Take(5),
                TopRated = GameController.getAllGames().OrderByDescending(g => g.Rating).Take(5),
                NewGames = GameController.getAllGames().OrderByDescending(g => g.ReleaseDate).Take(5),
                FeaturedGames = GameController.getAllGames().Where(g => g.Featured).Take(5),
            };

            return View(homeGames);
        }

        public ActionResult DisplayGame(int gameId)
        {
            ProjectOSG.Models.Game returnGame = GameController.getAllGames().FirstOrDefault(g => g.Id == gameId);
            return (returnGame == null) ? View("~/Views/Error/Index.cshtml", null)  : View(model: returnGame);
        }
        
        public ActionResult TypeOfGames(string panelTitle, Tag tag)
        {
            ViewBag.Title = panelTitle;
            ViewBag.PanelTitle = panelTitle;
            return View("AllGames");
        }

        public ActionResult MostPlayed()
        {
            ViewBag.Title = "MostPlayed";
            ViewBag.PanelTitle = "Most Played";
            return View("AllGames", GameController.getAllGames().OrderByDescending(g => g.PlayCount));
        }

        public ActionResult TopRated()
        {
            ViewBag.Title = "TopRated";
            ViewBag.PanelTitle = "Top Rated";
            return View("AllGames", GameController.getAllGames().OrderByDescending(g => g.Rating));
        }

        public ActionResult NewGames()
        {
            ViewBag.Title = "NewGames";
            ViewBag.PanelTitle = "New Games";
            return View("AllGames", GameController.getAllGames().OrderByDescending(g => g.ReleaseDate));
        }



        [HttpPost]
        public ActionResult SearchGames()
        {
            IEnumerable<ProjectOSG.Models.Game> gamesReturned = null;
            string title = Request.Params["gameTitle"];
            if (title != null)
            {
                ViewBag.Link = "Index";
                ViewBag.PanelTitle = "Games Found";
                gamesReturned = GameController.getAllGames().Where(g => g.Title.ToLower().Contains(title.ToLower().Trim()));
            }

            return (gamesReturned == null) ? View("GameNotFound") : View("AllGames", gamesReturned);
        }
    }
}
