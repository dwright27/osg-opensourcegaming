﻿using OSG.ViewModel;
using ProjectOSG.DAL;
using ProjectOSG.Models;
using ProjectOSG.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectOSG.Controllers
{
    public class ColterGameTestController : Controller
    {
        //
        // GET: /ColterGameTest/
        static ColterTestVM vm;
        [HttpGet]
        public ActionResult GamePage(ProfileVM p)
        {
            vm = new ColterTestVM();
            vm.profile = new ProfileVM { profileID = p.profileID, Achievements = p.Achievements, AboutMe = p.AboutMe, FirstName = p.FirstName, LastName = p.LastName, userID = p.userID };
            vm.gameID = 5;
            return View(model: vm);
        }

        [HttpPost]
        public ActionResult GamePage(int gameID, string achievId, string description, int points)
        {
            AchievementRepository repo = new AchievementRepository();
            Achievement temp = repo.FindByGameIdAndName(gameID, achievId);

            if (temp == null)
            {
                repo.Insert(new Achievement{GameBelongerID = gameID,Description = description, AchievementPoints = points, Name = achievId});
                temp = repo.FindByGameIdAndName(gameID, achievId);
            
            }
            UserAchievementRepository userRepo = new UserAchievementRepository();

            if (!userRepo.UserHasAchieve(UserController.GrabID(User.Identity.Name), temp.AchievementID))
            {
                userRepo.Insert(new UserAchievement { AchievementBelongerID = temp.AchievementID, UserBelongerID = UserController.GrabID(User.Identity.Name) });
            }
            return View(model: vm);
        }
    }
}
