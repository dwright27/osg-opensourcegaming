﻿using ProjectOSG.Models;
using ProjectOSG.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace ProjectOSG.Controllers
{
    [Authorize]
    public class ConversationController : Controller
    {
        ConversationRepository CR = new ConversationRepository();

        public ActionResult UserConversations()
        {
            List<Conversation> send = new List<Conversation>();
            foreach (var pick in CR.All())
            {
                if(pick.fromUserID==UserController.GrabID(User.Identity.Name) || pick.toUserID==UserController.GrabID(User.Identity.Name))
                {
                    //this is where you would do view model stuff
                    send.Add(pick);
                }
            }

            return View(send);
        }

        public ActionResult LookAtConversation(int id)
        {
            return RedirectToAction("Index", "Message", new { convId = id });
        }

        public ActionResult Create()
        {
            Conversation send = new Conversation();
            send.fromUserID = UserController.GrabID(User.Identity.Name);
            return View(send);
        }

        [HttpPost]
        public ActionResult Create(Conversation CONV)
        {
            if (ModelState.IsValid)
            {
                Conversation dataConv = new Conversation { fromUserID = CONV.fromUserID, toUserID = UserController.GrabID(CONV.tempTo), Subject = CONV.Subject };
                CR.InsertOrUpdate(dataConv);
                return RedirectToAction("UserConversations", "Conversation");
            }
            else
            {
                return View();
            }
        }
    }
}