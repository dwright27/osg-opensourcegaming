﻿using ProjectOSG.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectOSG.Controllers
{
    public class PostController : Controller
    {
        //
        // GET: /Post/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DisplayAll(int threadId)
        {
            PostRepository postsRepo = new PostRepository();
            ThreadRepository threadRepo = new ThreadRepository();
            ViewBag.Title = threadRepo.Find(threadId).TopicName;
            return View(postsRepo.All().Where(p=>p.ThreadId == threadId));
        }

        public ActionResult Create()
        {
            return View();
        }
    }
}
