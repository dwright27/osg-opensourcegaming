﻿using ProjectOSG.DAL;
using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectOSG.Controllers
{
    public class RelationshipController : Controller
    {
        //
        // GET: /Relationship/

        static RelationshipRepository RR = new RelationshipRepository();

        public ActionResult Index()
        {
            return RedirectToAction("Index", "User");
        }

        public ActionResult sendRequest(int id)
        {
            Relationship rel = new Relationship();
            rel.friendID=id;
            rel.userID=UserController.GrabID(User.Identity.Name);

            RR.InsertOrUpdate(rel);

            Relationship flipRel = new Relationship();
            flipRel.friendID = UserController.GrabID(User.Identity.Name);
            flipRel.userID = id;

            RR.InsertOrUpdate(flipRel);

            return View();
        }

        static public IEnumerable<string> GrabFriends(int userID)
        {
            return RR.AllFromUser(userID);
        }
    }
}