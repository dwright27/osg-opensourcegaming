﻿using ProjectOSG.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectOSG.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/

        static UserRepository UR = new UserRepository();

        public ActionResult Index()
        {
            return View(UR.All());
        }

        static public string GrabName(int id)
        {
            string ret=UR.Find(id).UserName;

            return ret;
        }

        static public int GrabID(string name)
        {
            int ret = UR.Find(name).Id;
            return ret;
        }
    }
}
