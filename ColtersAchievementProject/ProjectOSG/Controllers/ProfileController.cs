﻿using OSG.ViewModel;
using ProjectOSG.DAL;
using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectOSG.Controllers
{
    public class ProfileController : Controller
    {
        //
        // GET: /Profile/

        ProfileRepository pr = new ProfileRepository();
        UserRepository ur = new UserRepository();
        UserAchievementRepository ap = new UserAchievementRepository();

        public ActionResult ProfilePage()
        {
            
            User temp = ur.Find(User.Identity.Name);
            Profile pv = pr.FindByUserID(temp.Id);
           
            if (pv == null)
            {
                pr.Insert(new Profile { AboutMe = "", FirstName = User.Identity.Name, LastName = "", UserID  = temp.Id });
                pv = pr.FindByUserID(temp.Id);
            }

            ProfileVM profile = new ProfileVM(pv);
            return View(model : profile);
        }

        [HttpGet]
        public ActionResult ProfileEditPage()
        {
            User temp = ur.Find(User.Identity.Name);
            Profile pv = pr.Find(temp.Id);
            return View(model:pv);
            
        }

        [HttpPost]
        public ActionResult ProfileEditPage(ProfileVM pv)
        {
            Profile temp = new Profile { ProfileID = pv.profileID, AboutMe = pv.AboutMe, FirstName = pv.FirstName, LastName = pv.LastName, UserID = pv.userID };
            pr.Insert(temp);
            return View("ProfilePage", model: pv);
        }
    }
}
