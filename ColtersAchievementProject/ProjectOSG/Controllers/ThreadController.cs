﻿using ProjectOSG.DAL;
using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectOSG.Controllers
{
    public class ThreadController : Controller
    {
        //
        // GET: /Thread/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DisplayAll()
        {
          
            ThreadRepository threadRepo = new ThreadRepository();
            return View(threadRepo.All());
        }

        public ActionResult Display(int threadId)
        {
            ThreadRepository threadRepo = new ThreadRepository();
            return View(threadRepo.All().FirstOrDefault(t => t.Id == threadId));
        }

        public ActionResult Create()
        {
            string username = User.Identity.Name;
            return  string.IsNullOrEmpty(username) ? View("~/Views/Account/Login.cshtml"): View();
        }
        [HttpPost]
        public ActionResult Create(Thread thread)
        {
            ThreadRepository threads = new ThreadRepository();
            threads.Insert(thread);
            return View("~/Views/Thread/Display.cshtml", thread.Id);
        }

    }
}
