﻿using ProjectOSG.Models;
using ProjectOSG.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectOSG.ViewModel;
namespace ProjectOSG.Controllers
{
    public class GameController : Controller
    {
        //
        // GET: /Game/
        static GameRepository GR = new GameRepository();

        public ActionResult Index()
        {
            List<GameVM> GVM = new List<GameVM>();

            foreach (var VM in GR.All())
            {
                GameVM temp=new GameVM();
                temp.gameID = VM.Id;
                temp.rating = VM.Rating;
                temp.releaseDate = VM.ReleaseDate;
                temp.Title = VM.Title;

                GVM.Add(temp);
            }

            return View(GVM);
        }
        public ActionResult SingleGame(GameVM singleGame)
        {
            return View(singleGame);
        }
       

        public ActionResult Display(int gameId)
        {
            GameRepository games = new GameRepository();
            return View(games.Find(gameId));
        }

        public ActionResult DisplayTagged(Tag tag)
        {
            ViewBag.Title = tag.ToString();
            ViewBag.PanelTitle = tag.ToString();
            //List<GameVM> tagGames = TheGames.Where(g => g.Tags.Contains(tag.ToString())).ToList();
           
            return View("Index",GR.All());
        }

        public ActionResult MostPlayed()
        {
            GameRepository games = new GameRepository();

            ViewBag.Title = "MostPlayed";
            ViewBag.PanelTitle = "Most Played";
            return View("AllGames", GR.All().OrderByDescending(g => g.PlayCount));
        }

        public ActionResult TopRated()
        {
            GameRepository games = new GameRepository();

            ViewBag.Title = "TopRated";
            ViewBag.PanelTitle = "Top Rated";
            return View("AllGames", GR.All().OrderByDescending(g => g.Rating));
        }

        public ActionResult NewGames()
        {
            GameRepository games = new GameRepository();

            ViewBag.Title = "NewGames";
            ViewBag.PanelTitle = "New Games";
            return View("AllGames", GR.All().OrderByDescending(g => g.ReleaseDate));
        }


        [HttpPost]
        public ActionResult SearchGames()
        {
            IEnumerable<ProjectOSG.Models.Game> gamesReturned = null;
            string title = Request.Params["gameTitle"];
            GameRepository games = new GameRepository();

            if (title != null)
            {
                ViewBag.Link = "Index";
                ViewBag.PanelTitle = "Games Found";
                gamesReturned = games.All().Where(g => g.Title.ToLower().Contains(title.ToLower().Trim()));
            }

            return (gamesReturned == null) ? View("GameNotFound") : View("AllGames", gamesReturned);
        }
        [HttpGet]
        public ActionResult AddGame()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddGame(GameVM gvm)
        {
           //GR.Insert(gvm);
            return View("SingleGame",gvm);
        }
        [HttpGet]
        public ActionResult EdditGame(string Title)
        {
            var result = GR.All().Where(m => m.Title == Title).FirstOrDefault();
            return View("AddGame",result);
        }
        [HttpPost]
        public ActionResult EdditGame(GameVM gvm)
        {
            //GR.Update(gvm);
            return View("SingleGames",gvm);
        }

        static public IEnumerable<Game> getAllGames()
        {
            return GR.All();
        }
    }
}
