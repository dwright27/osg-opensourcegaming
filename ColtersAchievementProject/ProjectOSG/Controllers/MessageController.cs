﻿using ProjectOSG.DAL;
using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectOSG.Controllers
{
    public class MessageController : Controller
    {
        //
        // GET: /Message/
        MessageRepository MR = new MessageRepository();

        static int currentConv = 0;
        public ActionResult Index(int convId)
        {
            List<Message> send = MR.AllFromConvo(convId);
            currentConv = convId;
            return View(send);
        }

        public ActionResult Create()
        {
            Message send = new Message();
            send.convoID = currentConv;
            send.read = false;
            send.sentBy = User.Identity.Name;
            send.Timestamp = DateTime.Now;
            return View(send);
        }

        [HttpPost]
        public ActionResult Create(Message MESS)
        {
            if (ModelState.IsValid)
            {
                Message dataConv = new Message { content=MESS.content, convoID=MESS.convoID, read=MESS.read, sentBy=MESS.sentBy, Timestamp=MESS.Timestamp};
                MR.InsertOrUpdate(dataConv);
                return RedirectToAction("Index", "Message", new { convId = dataConv.convoID });
            }
            else
            {
                return View();
            }
        }
    }
}
