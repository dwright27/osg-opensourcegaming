﻿using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectOSG.DummyDal
{
    public class PostRepository : DAL.IRepository<Post>
    {
        static List<Post> posts = new List<Post>()
        {
            new Post{Id = 0, Message = "I have see errors in this game too.",PostDate = new DateTime(2014,1,1,7,23,05),ThreadId = 0, Username = "theDestorierOfWorlds"},
            new Post{Id = 1, Message = "Lazy Programming I suppose.",PostDate = new DateTime(2014,1,12,12,23,05),ThreadId = 0,Username = "Peter"},
            new Post{Id = 2, Message = "Sadly I agree, Gears of war could use some improvement.",PostDate = new DateTime(2014,2,17,21,8,15),ThreadId =1,Username = "That Guy"},
            new Post{Id = 3, Message = "There's nothing sad about it, it's the truth...",PostDate = new DateTime(2014,4,17,9,20,25),ThreadId = 1,Username = "Batman"},
        };

        public IEnumerable<Post> All()
        {
            return posts;
        }

        public Post Find(int id)
        {
            return posts.FirstOrDefault(t => t.Id == id);
        }

        public void Insert(Post insert)
        {
            insert.Id = posts[posts.Count - 1].Id + 1;
            posts.Add(insert);
        }

        public void Delete(int id)
        {
            posts.Remove(posts.FirstOrDefault(t => t.Id == id));
        }

        public void Update(Post update)
        {
            Post thread = posts.FirstOrDefault(t => t.Id == update.Id);
            posts.Remove(update);
            if (update != null)
            {
                posts.Add(update);
            }
        }
    }
} 