﻿using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectOSG.DummyDal
{
    public class ThreadRepository : DAL.IRepository<Thread>
    {
        static List<Thread> threads = new List<Thread>()
        {
            new Thread{Username = "Dameon" ,Description = "I have noticed that there have been some errors lately with the game", LastModified = new DateTime(2014,1,12,12,23,05), Started = new DateTime(2013,6,12, 3,12,45), Id = 0, TopicName= "Snake is broken"},
            new Thread{Username = "GOWHater",Description = "Make a better story", LastModified = new DateTime(2014,4,17,9,20,25), Started = new DateTime(2013,10,12,13,12,34), Id = 1, TopicName= "Gears of war sucks"},
        };


        public IEnumerable<Thread> All()
        {
            return threads;
        }

        public Thread Find(int id)
        {
            return threads.FirstOrDefault(t => t.Id == id);
        }

        public void Insert(Thread insert)
        {
            insert.Id = threads[threads.Count-1].Id +1;
            threads.Add(insert);
        }

        public void Delete(int id)
        {
            threads.Remove(threads.FirstOrDefault(t => t.Id == id));
        }

        public void Update(Thread update)
        {
            Thread thread = threads.FirstOrDefault(t => t.Id == update.Id);
            threads.Remove(update);
            if (update != null)
            {
                threads.Add(update);
            }
        }
    }
}