﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjectOSG.Models
{
    [Table("Relationships")]
    public class Relationship
    {
        [Key]
        [Required]
        public int relId { get; set; }

        [Required]
        public int friendID { get; set; }

        [Required]
        public int userID { get; set; }
    }
}