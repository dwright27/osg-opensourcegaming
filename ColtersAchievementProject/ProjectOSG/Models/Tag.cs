﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjectOSG.Models
{
    [Table("Tags")]
    public class Tag
    {
        [Key]
        [Required]
        public int tagID { get; set; }

        [Required]
        public string tagType { get; set; }
    }
}