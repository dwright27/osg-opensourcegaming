﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectOSG.Models
{
    public class HomePageGames
    {
        public IEnumerable<ProjectOSG.Models.Game> MostPlayed { get; set; }
        public IEnumerable<ProjectOSG.Models.Game> TopRated { get; set; }
        public IEnumerable<ProjectOSG.Models.Game> NewGames { get; set; }
        public IEnumerable<ProjectOSG.Models.Game> FeaturedGames { get; set; }
        
    }
}
