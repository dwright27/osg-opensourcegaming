﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjectOSG.Models
{
    [Table("GameTags")]
    public class GameTag
    {
        [Key]
        [Required]
        public int GameTagID { get; set; }

        [Required]
        public int gameBelongerID { get; set; }

        [Required]
        public string TagType { get; set; }
    }
}