﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectOSG.Models
{
    public class GameModel
    {
        public int GameID { get; set; }
        public string Title { get; set; }
        public int CreatorID { get; set; }
        public float Rating { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Description { get; set; }
        public List<Tag> GameTags { get; set; }
        public List<Achievement> Achievements { get; set; }
    }
}