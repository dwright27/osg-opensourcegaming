﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjectOSG.Models
{
    [Table("Achievements")]
    public class Achievement
    {
        [Key]
        public int AchievementID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int AchievementPoints { get; set; }
        public int GameBelongerID { get; set; }

    }
}