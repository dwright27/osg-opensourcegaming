-- Script Date: 6/3/2014 2:33 PM  - ErikEJ.SqlCeScripting version 3.5.2.38
-- Database information:
-- Locale Identifier: 1033
-- Encryption Mode: 
-- Case Sensitive: False
-- Database: D:\02-ProjectsClass\OSG\ProjectOSG\ProjectOSG\App_Data\OSGDatabase.sdf
-- ServerVersion: 4.0.8876.1
-- DatabaseSize: 276 KB
-- SpaceAvailable: 3.999 GB
-- Created: 5/30/2014 4:43 PM

-- User Table information:
-- Number of tables: 9
-- Conversations: 0 row(s)
-- Posts: 0 row(s)
-- Profiles: 1 row(s)
-- Thread: 0 row(s)
-- Users: 7 row(s)
-- webpages_Membership: 7 row(s)
-- webpages_OAuthMembership: 0 row(s)
-- webpages_Roles: 0 row(s)
-- webpages_UsersInRoles: 0 row(s)
CREATE DATABASE OSGDatabase on (name='OSGD', filename = 'd:\03-Project')
go
USE OSGDatabse
go

CREATE TABLE [webpages_Roles] (
  [RoleId] int IDENTITY (1,1) NOT NULL
, [RoleName] nvarchar(256) NOT NULL
);
GO
CREATE TABLE [webpages_OAuthMembership] (
  [Provider] nvarchar(30) NOT NULL
, [ProviderUserId] nvarchar(100) NOT NULL
, [UserId] int NOT NULL
);
GO
CREATE TABLE [webpages_Membership] (
  [UserId] int NOT NULL
, [CreateDate] datetime NULL
, [ConfirmationToken] nvarchar(128) NULL
, [IsConfirmed] bit DEFAULT 0 NULL
, [LastPasswordFailureDate] datetime NULL
, [PasswordFailuresSinceLastSuccess] int DEFAULT 0 NOT NULL
, [Password] nvarchar(128) NOT NULL
, [PasswordChangedDate] datetime NULL
, [PasswordSalt] nvarchar(128) NOT NULL
, [PasswordVerificationToken] nvarchar(128) NULL
, [PasswordVerificationTokenExpirationDate] datetime NULL
);
GO
CREATE TABLE [Users] (
  [Id] int IDENTITY (8,1) NOT NULL
, [UserName] nvarchar(56) NOT NULL
);
GO
CREATE TABLE [webpages_UsersInRoles] (
  [UserId] int NOT NULL
, [RoleId] int NOT NULL
);
GO
CREATE TABLE [Thread] (
  [Id] int IDENTITY (1,1) NOT NULL
, [TopicName] nvarchar(100) NOT NULL
, [Started] datetime NOT NULL
, [LastModified] datetime NOT NULL
);
GO
CREATE TABLE [Profiles] (
  [ProfileID] int IDENTITY (2,1) NOT NULL
, [UserID] int NOT NULL
, [FirstName] nvarchar(100) NULL
, [Lastname] nvarchar(100) NULL
, [AboutMe] nvarchar(500) NULL
);
GO
CREATE TABLE [Posts] (
  [Id] int IDENTITY (1,1) NOT NULL
, [ThreadId] nvarchar(100) NOT NULL
, [PostDate] datetime NOT NULL
, [Message] nvarchar(1000) NOT NULL
);
GO
CREATE TABLE [Conversations] (
  [conversationID] int IDENTITY (1,1) NOT NULL
, [Subject] nvarchar(100) NOT NULL
, [toUserID] nvarchar(100) NOT NULL
, [fromUserID] nvarchar(100) NOT NULL
);
GO
SET IDENTITY_INSERT [webpages_Roles] OFF;
GO
INSERT INTO [webpages_Membership] ([UserId],[CreateDate],[ConfirmationToken],[IsConfirmed],[LastPasswordFailureDate],[PasswordFailuresSinceLastSuccess],[Password],[PasswordChangedDate],[PasswordSalt],[PasswordVerificationToken],[PasswordVerificationTokenExpirationDate]) VALUES (1,{ts '2014-05-27 20:31:49.530'},NULL,1,NULL,0,N'AK2PNJHvCZE2DNijNss6Dpla1SUzrw8jV2e7+15iuQ4nTaOESRLVvUY8fu/Z+NTcVQ==',{ts '2014-05-27 20:31:49.530'},N'',NULL,NULL);
GO
INSERT INTO [webpages_Membership] ([UserId],[CreateDate],[ConfirmationToken],[IsConfirmed],[LastPasswordFailureDate],[PasswordFailuresSinceLastSuccess],[Password],[PasswordChangedDate],[PasswordSalt],[PasswordVerificationToken],[PasswordVerificationTokenExpirationDate]) VALUES (2,{ts '2014-05-28 19:46:53.537'},NULL,1,NULL,0,N'AOLbeRf3Omf4qRn1ir/R0Uk3nftIAxVYyMtCqZNOIJwE1mCEAq8Swl5RQ4DNnWnitg==',{ts '2014-05-28 19:46:53.537'},N'',NULL,NULL);
GO
INSERT INTO [webpages_Membership] ([UserId],[CreateDate],[ConfirmationToken],[IsConfirmed],[LastPasswordFailureDate],[PasswordFailuresSinceLastSuccess],[Password],[PasswordChangedDate],[PasswordSalt],[PasswordVerificationToken],[PasswordVerificationTokenExpirationDate]) VALUES (3,{ts '2014-06-02 20:40:21.990'},NULL,1,NULL,0,N'AC42/pfmGk47D7fmF44/V0hbMcHvbceudRfKKeDt8yX+FO4qC3l60tsVs4ZCefl0zQ==',{ts '2014-06-02 20:40:21.990'},N'',NULL,NULL);
GO
INSERT INTO [webpages_Membership] ([UserId],[CreateDate],[ConfirmationToken],[IsConfirmed],[LastPasswordFailureDate],[PasswordFailuresSinceLastSuccess],[Password],[PasswordChangedDate],[PasswordSalt],[PasswordVerificationToken],[PasswordVerificationTokenExpirationDate]) VALUES (4,{ts '2014-06-03 01:02:41.840'},NULL,1,NULL,0,N'AA+x/lEzYeBwyeIU2tFD2l1dgUVeF82UiY4+GpOIeNOoEOGg/UgWyEoKVMSvqMz/0A==',{ts '2014-06-03 01:02:41.840'},N'',NULL,NULL);
GO
INSERT INTO [webpages_Membership] ([UserId],[CreateDate],[ConfirmationToken],[IsConfirmed],[LastPasswordFailureDate],[PasswordFailuresSinceLastSuccess],[Password],[PasswordChangedDate],[PasswordSalt],[PasswordVerificationToken],[PasswordVerificationTokenExpirationDate]) VALUES (5,{ts '2014-06-03 01:15:43.980'},NULL,1,NULL,0,N'AIzMJb5hBG/bwadBK4JB6wHl+RuR1KXptp65u3wrY/7ZtQavyIFMnY+VT49GAw4fRg==',{ts '2014-06-03 01:15:43.980'},N'',NULL,NULL);
GO
INSERT INTO [webpages_Membership] ([UserId],[CreateDate],[ConfirmationToken],[IsConfirmed],[LastPasswordFailureDate],[PasswordFailuresSinceLastSuccess],[Password],[PasswordChangedDate],[PasswordSalt],[PasswordVerificationToken],[PasswordVerificationTokenExpirationDate]) VALUES (6,{ts '2014-06-03 04:10:10.530'},NULL,1,NULL,0,N'AHlWZuunpBOWKAfmO7QRzkA92iC+H331iG+OqJt2skbeSgwnuHjQZSj9OVJfjFkJ0A==',{ts '2014-06-03 04:10:10.530'},N'',NULL,NULL);
GO
INSERT INTO [webpages_Membership] ([UserId],[CreateDate],[ConfirmationToken],[IsConfirmed],[LastPasswordFailureDate],[PasswordFailuresSinceLastSuccess],[Password],[PasswordChangedDate],[PasswordSalt],[PasswordVerificationToken],[PasswordVerificationTokenExpirationDate]) VALUES (7,{ts '2014-06-03 04:15:48.147'},NULL,1,NULL,0,N'ALbewVLjN255JhplM73uIe55vgrRlytpWB0iA3xj2EAOC0xqNqQ4yBWpmlaFjzt6Lw==',{ts '2014-06-03 04:15:48.147'},N'',NULL,NULL);
GO
SET IDENTITY_INSERT [Users] ON;
GO
INSERT INTO [Users] ([Id],[UserName]) VALUES (1,N'Geekleak');
GO
INSERT INTO [Users] ([Id],[UserName]) VALUES (2,N'Cvach');
GO
INSERT INTO [Users] ([Id],[UserName]) VALUES (3,N'dwright');
GO
INSERT INTO [Users] ([Id],[UserName]) VALUES (4,N'jake');
GO
INSERT INTO [Users] ([Id],[UserName]) VALUES (5,N'user');
GO
INSERT INTO [Users] ([Id],[UserName]) VALUES (6,N'james');
GO
INSERT INTO [Users] ([Id],[UserName]) VALUES (7,N'person');
GO
SET IDENTITY_INSERT [Users] OFF;
GO
SET IDENTITY_INSERT [Thread] OFF;
GO
SET IDENTITY_INSERT [Profiles] ON;
GO
INSERT INTO [Profiles] ([ProfileID],[UserID],[FirstName],[Lastname],[AboutMe]) VALUES (1,1,N'Devon',N'Truman',N'I am awesome!');
GO
SET IDENTITY_INSERT [Profiles] OFF;
GO
SET IDENTITY_INSERT [Posts] OFF;
GO
SET IDENTITY_INSERT [Conversations] OFF;
GO
ALTER TABLE [webpages_Roles] ADD CONSTRAINT [PK__webpages_Roles__0000000000000043] PRIMARY KEY ([RoleId]);
GO
ALTER TABLE [webpages_OAuthMembership] ADD CONSTRAINT [PK__webpages_OAuthMembership__0000000000000017] PRIMARY KEY ([Provider],[ProviderUserId]);
GO
ALTER TABLE [webpages_Membership] ADD CONSTRAINT [PK__webpages_Membership__0000000000000039] PRIMARY KEY ([UserId]);
GO
ALTER TABLE [Users] ADD CONSTRAINT [PK__Users__0000000000000006] PRIMARY KEY ([Id]);
GO
ALTER TABLE [webpages_UsersInRoles] ADD CONSTRAINT [PK__webpages_UsersInRoles__0000000000000052] PRIMARY KEY ([UserId],[RoleId]);
GO
ALTER TABLE [Thread] ADD CONSTRAINT [PK_Thread] PRIMARY KEY ([Id]);
GO
ALTER TABLE [Profiles] ADD CONSTRAINT [PK_Profiles] PRIMARY KEY ([ProfileID]);
GO
ALTER TABLE [Posts] ADD CONSTRAINT [PK_Posts] PRIMARY KEY ([Id]);
GO
ALTER TABLE [Conversations] ADD CONSTRAINT [PK_Conversations] PRIMARY KEY ([conversationID]);
GO
CREATE UNIQUE INDEX [UQ__webpages_Roles__0000000000000048] ON [webpages_Roles] ([RoleName] ASC);
GO
CREATE UNIQUE INDEX [UQ__Users__000000000000000B] ON [Users] ([UserName] ASC);
GO
CREATE UNIQUE INDEX [UQ__Thread__0000000000000083] ON [Thread] ([Id] ASC);
GO
CREATE UNIQUE INDEX [UQ__Profiles__000000000000006C] ON [Profiles] ([ProfileID] ASC);
GO
CREATE UNIQUE INDEX [UQ__Profiles__0000000000000071] ON [Profiles] ([UserID] ASC);
GO
CREATE UNIQUE INDEX [UQ__Posts__00000000000000A7] ON [Posts] ([Id] ASC);
GO
CREATE UNIQUE INDEX [UQ__Conversations__0000000000000095] ON [Conversations] ([conversationID] ASC);
GO
ALTER TABLE [webpages_UsersInRoles] ADD CONSTRAINT [fk_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [webpages_Roles]([RoleId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
GO
ALTER TABLE [webpages_UsersInRoles] ADD CONSTRAINT [fk_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users]([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

