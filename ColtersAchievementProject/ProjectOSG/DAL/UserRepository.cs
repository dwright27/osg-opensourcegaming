﻿using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace ProjectOSG.DAL
{
    public class UserRepository : IRepository<User>
    {
        public IEnumerable<User> All()
        {
            using (var db = new ProjectOSGContext())
            {
                return db.Users.ToList();
            }
        }

        public IEnumerable<User> AllIncluding(params Expression<Func<User, object>>[] includeProperties)
        {
            using (var db = new ProjectOSGContext())
            {
                List<User> query = db.Users.ToList();
                foreach (var includeProperty in includeProperties)
                {
                    query = query;
                }
                return query;
            }
        }

        public User Find(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                return db.Users.Find(id);
            }
        }

        public User Find(string name)
        {
            using (var db = new ProjectOSGContext())
            {
                foreach (var us in db.Users)
                {
                    if (us.UserName == name)
                    {
                        return us;
                    }
                }
            }
            return null;
        }

        public void InsertOrUpdate(User info)
        {
            using (var db = new ProjectOSGContext())
            {
                if (info.Id == default(int))
                {
                    // New entity
                    db.Users.Add(info);
                }
                else
                {
                    // Existing entity
                    db.Entry(info).State = System.Data.Entity.EntityState.Modified;
                }

                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                var info = db.Users.Find(id);
                db.Users.Remove(info);
                db.SaveChanges();
            }
        }



        

        public void Insert(User insert)
        {
            using (var db = new ProjectOSGContext())
            {
                // New entity
                db.Users.Add(insert);
                db.SaveChanges();
            }
        }


        public void Update(User update)
        {
            using (var db = new ProjectOSGContext())
            {

                // Existing entity
                db.Entry(update).State = System.Data.Entity.EntityState.Modified;


                db.SaveChanges();
            }
        }
    }

}