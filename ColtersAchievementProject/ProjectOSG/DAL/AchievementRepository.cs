﻿using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectOSG.DAL
{
    public class AchievementRepository:IRepository<Achievement>
    {
        public IEnumerable<Achievement> All()
        {
            using (var db = new ProjectOSGContext())
            {
                return db.Achievements.ToList();
            }
        }

        public Achievement Find(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                return db.Achievements.Find(id);
            }
        }

        public Achievement FindByGameIdAndName(int id, string achievName)
        {
            Achievement ret = new Achievement();
            using (var db = new ProjectOSGContext())
            {
                return db.Achievements.Where(p => p.Name == achievName && p.GameBelongerID == id).FirstOrDefault();
            }
            return ret;
        }


        public void Insert(Achievement insert)
        {
            using (var db = new ProjectOSGContext())
            {
                if (insert.AchievementID == default(int))
                {
                    // New entity
                    db.Achievements.Add(insert);
                }
                else
                {
                    // Existing entity
                    db.Entry(insert).State = System.Data.Entity.EntityState.Modified;
                }

                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                var info = db.Achievements.Find(id);
                db.Achievements.Remove(info);
                db.SaveChanges();
            }
        }

        public void Update(Achievement update)
        {
            using (var db = new ProjectOSGContext())
            {

                // Existing entity
                db.Entry(update).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}