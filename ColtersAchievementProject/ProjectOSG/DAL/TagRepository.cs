﻿using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace ProjectOSG.DAL
{
    public class TagRepository : IRepository<Tag>
    {
        public IEnumerable<Tag> All()
        {
            using (var db = new ProjectOSGContext())
            {
                return db.Tags.ToList();
            }
        }

        public IEnumerable<Tag> AllIncluding(params Expression<Func<Tag, object>>[] includeProperties)
        {
            using (var db = new ProjectOSGContext())
            {
                List<Tag> query = db.Tags.ToList();
                foreach (var includeProperty in includeProperties)
                {
                    query = query;
                }
                return query;
            }
        }

        public Tag Find(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                return db.Tags.Find(id);
            }
        }

        public void Insert(Tag insert)
        {

        }

        public void Update(Tag update)
        {

        }

        public void InsertOrUpdate(Tag info)
        {
            using (var db = new ProjectOSGContext())
            {
                if (info.tagID == default(int))
                {
                    // New entity
                    db.Tags.Add(info);
                }
                else
                {
                    // Existing entity
                    db.Entry(info).State = System.Data.Entity.EntityState.Modified;
                }

                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                var info = db.Tags.Find(id);
                db.Tags.Remove(info);
                db.SaveChanges();
            }
        }
    }
}