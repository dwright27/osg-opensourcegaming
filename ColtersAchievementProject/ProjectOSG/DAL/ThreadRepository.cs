﻿using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectOSG.DAL
{
    public class ThreadRepository : IRepository<Thread>
    {

        public IEnumerable<Thread> All()
        {
            using (var db = new ProjectOSGContext())
            {
                return db.Threads.ToList();
            }
        }

        public Thread Find(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                return db.Threads.Find(id);

            }
        }

        public void Insert(Thread insert)
        {
            using (var db = new ProjectOSGContext())
            {
                if (insert.Id == default(int))
                {
                    // New entity
                    db.Threads.Add(insert);
                }
                else
                {
                    // Existing entity
                    db.Entry(insert).State = System.Data.Entity.EntityState.Modified;
                }

                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                var info = db.Threads.Find(id);
                db.Threads.Remove(info);
                db.SaveChanges();
            }
        }

        public void Update(Thread update)
        {
            using (var db = new ProjectOSGContext())
            {

                // Existing entity
                db.Entry(update).State = System.Data.Entity.EntityState.Modified;

                db.SaveChanges();
            }
        }
    }
}