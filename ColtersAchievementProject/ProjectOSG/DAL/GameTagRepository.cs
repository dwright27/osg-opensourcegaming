﻿using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace ProjectOSG.DAL
{
    public class GameTagRepository : IRepository<GameTag>
    {
        public IEnumerable<GameTag> All()
        {
            using (var db = new ProjectOSGContext())
            {
                return db.GameTags.ToList();
            }
        }

        public IEnumerable<GameTag> AllIncluding(params Expression<Func<GameTag, object>>[] includeProperties)
        {
            using (var db = new ProjectOSGContext())
            {
                List<GameTag> query = db.GameTags.ToList();
                foreach (var includeProperty in includeProperties)
                {
                    query = query;
                }
                return query;
            }
        }

        public IEnumerable<GameTag> FindAllBelongingInGame(int gameId)
        {
            using (var db = new ProjectOSGContext())
            {
                List<GameTag> gameBelonger=new List<GameTag>();
                foreach (var grab in db.GameTags)
                {
                    if (grab.gameBelongerID == gameId)
                    {
                        gameBelonger.Add(grab);
                    }
                }
                return gameBelonger;
            }
        }

        public GameTag Find(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                return db.GameTags.Find(id);
            }
        }

        public void Insert(GameTag insert)
        {

        }

        public void Update(GameTag update)
        {

        }

        public void InsertOrUpdate(GameTag info)
        {
            using (var db = new ProjectOSGContext())
            {
                if (info.GameTagID == default(int))
                {
                    // New entity
                    db.GameTags.Add(info);
                }
                else
                {
                    // Existing entity
                    db.Entry(info).State = System.Data.Entity.EntityState.Modified;
                }

                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                var info = db.GameTags.Find(id);
                db.GameTags.Remove(info);
                db.SaveChanges();
            }
        }
    }
}