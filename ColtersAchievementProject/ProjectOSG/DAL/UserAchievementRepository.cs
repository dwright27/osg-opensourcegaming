﻿using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectOSG.DAL
{
    public class UserAchievementRepository:IRepository<UserAchievement>
    {

        public IEnumerable<UserAchievement> All()
        {
            List<UserAchievement> temp = new List<UserAchievement>();
            using (var db = new ProjectOSGContext())
            {
                temp = db.UserAchievements.ToList();
            }
            return temp;
        }

        public UserAchievement Find(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                return db.UserAchievements.Find(id);
            }
        }

        public List<UserAchievement> FindUsersAchievs(int id)
        {
            List<UserAchievement> achievs = new List<UserAchievement>();
            using (var db = new ProjectOSGContext())
            {
                achievs = db.UserAchievements.Where(p => p.UserBelongerID == id).ToList();
            }
            return achievs;
        }

        public bool UserHasAchieve(int id,int achievID)
        {
            List<UserAchievement> achievs = new List<UserAchievement>();
            using (var db = new ProjectOSGContext())
            {
                achievs = db.UserAchievements.Where(p => p.UserBelongerID == id&&p.AchievementBelongerID==achievID).ToList();
            }
            return achievs.Count()>0;
        }

        public void Insert(UserAchievement insert)
        {
            using (var db = new ProjectOSGContext())
            {
                if (insert.UserAchievementsID == default(int))
                {
                    // New entity
                    db.UserAchievements.Add(insert);
                }
                else
                {
                    // Existing entity
                    db.Entry(insert).State = System.Data.Entity.EntityState.Modified;
                }

                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                var info = db.UserAchievements.Find(id);
                db.UserAchievements.Remove(info);
                db.SaveChanges();
            }
        }

        public void Update(UserAchievement update)
        {
            using (var db = new ProjectOSGContext())
            {

                // Existing entity
                db.Entry(update).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}