﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectOSG.DAL
{
    interface IRepository<T>
    {
        IEnumerable<T> All();
        T Find(int id);
        void Insert(T insert);
        void Delete(int id);
        void Update(T update);
    }
}
