﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
namespace ProjectOSG.Hubs
{
    public class ChatHub : Hub
    {
        //public void Send(string name, string message)
        //{
        //    Clients.All.addNewMessageToPage(name, message);
        //}

        //public void Send(string userId, string message)
        //{
        //    var check = Clients.User(userId);
        //    Clients.User(userId).send(message);
        //}

        public readonly static ConnectionMapping<string> _connections =
      new ConnectionMapping<string>();


        public void Send(string who, string message)
        {
            string name = Context.User.Identity.Name;

            foreach (var connectionId in _connections.GetConnections(who))
            {
                Clients.Client(connectionId).addNewMessageToPage(name, message);

            }
            Clients.All.updateList(_connections.Users.Distinct().Where(n => n != name).ToArray());
        }

        public override Task OnConnected()
        {
            string name = Context.User.Identity.Name;
            if (!string.IsNullOrEmpty(name))
            {
                if(_connections.Users.ToList().IndexOf(name) == -1)
                {
                  _connections.Add(name, Context.ConnectionId);
                IEnumerable<string> list = _connections.Users.Where(n=>n != name);
                Clients.All.updateList(list.ToArray());
                }
            }

  
            return base.OnConnected();
        }

        public override Task OnDisconnected()
        {
            string name = Context.User.Identity.Name;

            _connections.Remove(name, Context.ConnectionId);
            IEnumerable<string> list = _connections.Users.Where(n => n != name);
            Clients.All.updateList(list.ToArray());
            return base.OnDisconnected();
        }

        public override Task OnReconnected()
        {
            string name = Context.User.Identity.Name;

            if (!_connections.GetConnections(name).Contains(Context.ConnectionId))
            {
                _connections.Add(name, Context.ConnectionId);
            }
            Clients.All.updateList(_connections.Users.Distinct().ToArray());


            return base.OnReconnected();
        }

    }
}