﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace EngineProject.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult VertexShader(string shader)
        {
            XmlDocument doc = new XmlDocument();
            string path = Server.MapPath("~/");
            doc.Load( path + "WebGL/scripts/Shader/" + shader + ".xml");
            string text = doc.InnerText;
            return this.Content(doc.InnerText, "text/text");
        }

        public ActionResult PixelShader(string shader)
        {
            XmlDocument doc = new XmlDocument();
            string path = Server.MapPath("~/");
            doc.Load(path + "WebGL/scripts/Shader/" + shader + ".xml");
            return this.Content(doc.InnerText, "text/text");//new FileContentResult("~/WebGL/scripts/Shader/" + shader + ".xml", "text/xml");
           // return new FileRe("~/WebGL/scripts/Shader/" + shader + ".xml");
        }

        public ActionResult GetFile(string modelName)
        {
            return new FilePathResult("~/WebGL/model/" + modelName + ".json","text/json");
        }

        public ActionResult GetImage(string imageName)
        {
            var dir = Server.MapPath("~/WebGL/images/");
            var path = Path.Combine(dir, imageName + ".png");
            return base.File(path, "image/png");
            //return new FilePathResult("~/WebGL/images/" + "cubetexture" + ".png", "image/png");
            //return new FilePathResult(@"D:\SchoolWork\C#2\MasterProject\EngineProject\EngineProject\WebGL\images\cubetexture.png", "image/png");
        }

    }
}
