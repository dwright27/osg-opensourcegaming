﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace EngineProject
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(
    name: "ImageLoad",
    url: "Home/GetImage/{imageName}",
    defaults: new { controller = "Home", action = "GetImage", shader = "" });


            routes.MapRoute(
    name: "VertexShaderLoad",
    url: "Home/VertexShader/{shader}",
    defaults: new { controller = "Home", action = "VertexShader", shader = "" }

    ); routes.MapRoute(
     name: "PixelShaderLoad",
     url: "Home/PixelShader/{shader}",
     defaults: new { controller = "Home", action = "PixelShader", shader = "" }

     );

            routes.MapRoute(
                name: "LoadModel",
                url: "Home/GetFile/{modelName}",
                defaults: new { controller = "Home", action = "GetFile", modelName = "" }

                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}