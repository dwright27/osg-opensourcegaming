﻿window.onload = start;

var shaderProgram;
var vertexBufffer;
var indexBuffer;
var colorBuffer;
var pMatrix = mat4.create();
var mvMatrix = mat4.create();
var normalMatrix = mat3.create();
var gl;
var path = "Home/GetFile/cube";
var lightPosition = [0.0, 0.0, -4.0];
var lightDiffuse = vec4.fromValues(0.7, 0.0, 0.1, 1.0);
var lightAmbient = vec4.fromValues(0.2, 0.5, 0.3, 1.0);
var lightSpecular = vec4.fromValues(0.2, 0.2, 0.7, 1.0);

var diffuseColor = vec4.fromValues(0.5, 0.0, 0.9, 1.0);
var ambientColor = [0.9, 0.2, 0.2];
var specularColor = [0.5, 0.5, 0.9];

var vs_source = null,
    fs_source = null;
var scene;


function start() {
    var canvas = $('#squareWithDrawArrays')[0];
    initGL(canvas);
    initShaders();
    scene = new Scene(gl);
    //scene.addSceneObject(path, [0.0, 0.0, 0.0], 0.0, 0.0, 0.0);
    initModels();
    initScene();
    drawScene();
}

function initModels() {
    $.getJSON(path,
        function (data) {
            scene.addSceneObject(data);
        });

}


function setMatrixUniforms() {
    gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
    gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
    gl.uniformMatrix3fv(shaderProgram.normalMatrixUniform, false, normalMatrix);
    gl.uniform4fv(shaderProgram.diffuseColor, diffuseColor);
    gl.uniform3fv(shaderProgram.specularColor, specularColor);
    gl.uniform3fv(shaderProgram.ambientColor, ambientColor);

    
}

function drawScene() {
    window.requestAnimationFrame(drawScene);
    gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    mat4.perspective(40, gl.viewportWidth / gl.viewportHeight, 0.1, 1000.0, pMatrix);
    for (var i = 0; i < scene.sceneObjects.length; i++) {
        diffuseColor = scene.sceneObjects[i].diffuseColor;
        ambientColor = scene.sceneObjects[i].ambientColor;
        specularColor = scene.sceneObjects[i].specularColor;

        setLightUniform();
        setMatrixUniforms();
    }
    
}

function initScene() {
   // initBuffers();
    gl.clearColor(0.2, 0.7, 0.4, 1.0);
    //enable depth and color
    gl.enable(gl.DEPTH_TEST);
    drawScene();

}

var KeyCode = {
    UP: 87,
    LEFT: 65,
    RIGHT: 68,
    DOWN: 83,
}


function handleKeyPress(event) {
    if (event.which == KeyCode.UP) {
        rotateY += 0.1;
    }
    if (event.which == KeyCode.DOWN) {
        rotateY -= 0.1;
    }
    if (event.which == KeyCode.RIGHT) {
        rotateZ += 0.1;
    }
    if (event.which == KeyCode.LEFT) {
        rotateZ -= 0.1;
    }

}


function setLightUniform() {
    gl.uniform3fv(shaderProgram.lightPosition, lightPosition);
    gl.uniform4fv(shaderProgram.lightDiffuse, lightDiffuse);
    gl.uniform4fv(shaderProgram.lightSpecular, lightSpecular);
    gl.uniform4fv(shaderProgram.lightAmbient, lightAmbient);
}


function initShaders() {
    var vertexShader = createShader(gl, "vertex", vs_source);
    var fragmentShader = createShader(gl, "fragment", fs_source);

    // var vertexShader = createShader(gl, "shader-vs");
    // var fragmentShader = createShader(gl, "shader-fs");


    shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert("Shaders cannot be initalized");
    }
    gl.useProgram(shaderProgram);

    shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
    shaderProgram.vertexNormalAttribute = gl.getAttribLocation(shaderProgram, "aVertexNormal");
    gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);
    gl.enableVertexAttribArray(shaderProgram.vertexNormalAttribute);

    //shaderProgram.vertexColorAttribute = gl.getAttribLocation(shaderProgram, "vertexColor");
    //gl.enableVertexAttribArray(shaderProgram.vertexColorAttribute);

    shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "pMatrix");
    shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "mvMatrix");
    shaderProgram.normalMatrixUniform = gl.getUniformLocation(shaderProgram, "normalMatrix");
    shaderProgram.diffuseColor = gl.getUniformLocation(shaderProgram, "materialDiffuseColor");
    shaderProgram.ambientColor = gl.getUniformLocation(shaderProgram, "materialAmbientColor");
    shaderProgram.specularColor = gl.getUniformLocation(shaderProgram, "materialSpecularColor");
    shaderProgram.lightPosition = gl.getUniformLocation(shaderProgram, "lightPosition");
    shaderProgram.lightDiffuse = gl.getUniformLocation(shaderProgram, "lightDiffuse");
    shaderProgram.lightAmbient = gl.getUniformLocation(shaderProgram, "lightAmbient");
    shaderProgram.lightSpecular = gl.getUniformLocation(shaderProgram, "lightSpecular");


}
//check
function initGL(canvas) {
    var names = ["webgl", "experimental-webgl", "webkit-3d", "moz-webgl"];

    for (var i = 0; i < names.length; ++i) {
        try {
            gl = canvas.getContext(names[i]);
        }
        catch (e)
        { }
        if (gl) {
            break;
        }


    }
    if (gl == null) {
        alert("Could not initialize WebGL");
        return null;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    return true;
}

function createShader(gl, type, shaderCode) {

    // shaderCode = shaderCode.substring(2, shaderCode.length-2);
    var shader;
    if (type == "fragment") {
        shader = gl.createShader(gl.FRAGMENT_SHADER);
    }
    else if (type == "vertex") {
        shader = gl.createShader(gl.VERTEX_SHADER);
    }
    else {
        return null;
    }

    gl.shaderSource(shader, shaderCode);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        return null;
    }

    return shader;
}


/*
window.onload = start;

var shaderProgram;
var vertexBufffer;
var indexBuffer;
var colorBuffer;
var pMatrix = mat4.create();
var mvMatrix = mat4.create();
var normalMatrix = mat3.create();
var gl;
var path = "/Home/GetFile/cube";
var lightPosition = [0.0, 0.0, -4.0];
var lightDiffuse = vec4.fromValues(0.7, 0.0, 0.1, 1.0);
var lightAmbient = vec4.fromValues(0.2, 0.5, 0.3, 1.0);
var lightSpecular = vec4.fromValues(0.2, 0.2, 0.7, 1.0);

var diffuseColor = vec4.fromValues(0.5, 0.0, 0.9, 1.0);
var ambientColor = vec4.fromValues(0.9, 0.2, 0.2, 1.0);
var specularColor = vec4.fromValues(0.5, 0.5, 0.9, 1.0);

var vs_source = null,
    fs_source = null;


function loadShaderText() {
    var httpsRequest = new XMLHttpRequest();
    httpsRequest.open("GET", "Home/VertexShader/gouraudShadingVS", false);
    httpsRequest.send();
    vs_source = httpsRequest.responseText.substring(1,httpsRequest.responseText.length-1);

    httpsRequest.open("GET", "Home/PixelShader/gouraudShadingPS", false);
    httpsRequest.send();
    fs_source = httpsRequest.responseText.substring(1, httpsRequest.responseText.length - 1);

}

function start() {

    // var canvas = document.getElementById("squareWithDrawArrays");
    var canvas = $('#squareWithDrawArrays')[0];
    initGL(canvas);
    loadShaderText();
    initShaders();
   
    initModels();
    //initScene();
   // document.onkeydown = handleKeyDown;
    document.onkeydown = handleKeyPress;
    //gl.drawArrays(gl.TRIANGLES,0,vertices.numItems);
    //gl.drawElements(gl.TRIANGLES)	
};

function objectChanged() {
    path = "Home/GetFile/" + document.getElementById("objectSelect").value.toString();
    initModels();
}

function initModels() {
    $.getJSON(path,
    function (data) {
        scene.addSceneObject(data);
    });
    //$.getJSON(path,
    //    function (data) {
    //        vertices = data.vertices;
    //        var faces = parseJSON(data);
    //        indices = getIndicesFromFaces(faces);
    //        if (data.materials.length > 0) {
    //            diffuseColor = data.materials[0].colorDiffuse;
    //            diffuseColor.push(1.0); // added alpha channel
    //            ambientColor = data.materials[0].colorAmbient;
    //            ambientColor.push(1.0);
    //            specularColor = data.materials[0].colorSpecular;
    //            specularColor.push(1.0);
    //        }
    //        normals = calculateVertexNormals(vertices, indices);
    //        initScene();
    //    });
}

var rotateY = 0.0;
var rotateZ = 0.0;

function initScene() {
    initBuffers();
    gl.clearColor(0.2, 0.7, 0.4, 1.0);
    //enable depth and color
    gl.enable(gl.DEPTH_TEST);
    drawScene();

}

var KeyCode = {
    UP : 87,
    LEFT : 65,
    RIGHT : 68,
    DOWN : 83,
}


function handleKeyPress(event) {
    if (event.which == KeyCode.UP) {
        rotateY += 0.1;
    }
    if (event.which == KeyCode.DOWN) {
        rotateY -= 0.1;
    }
    if (event.which == KeyCode.RIGHT) {
        rotateZ += 0.1;
    }
    if (event.which == KeyCode.LEFT) {
        rotateZ -= 0.1;
    }

}

function drawScene() {

    window.requestAnimationFrame(drawScene);
    gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    mat4.perspective(40, gl.viewportWidth / gl.viewportHeight, 0.1, 1000.0, pMatrix);
    for (var i = 0; i < scene.sceneObjects.length; i++) {
        diffuseColor = scene.sceneObjects[i].diffuseColor;
        ambientColor = scene.sceneObjects[i].ambientColor;
        specularColor = scene.sceneObjects[i].specularColor;

        setLightUniform();
        setMatrixUniforms();
    }


    window.requestAnimationFrame(drawScene);
    gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    mat4.perspective(40, gl.viewportWidth / gl.viewportHeight, 0.1, 1000.0, pMatrix);
    mat4.identity(mvMatrix);
    mat4.identity(normalMatrix);
    mat4.translate(mvMatrix, [0, 0, -7]);
    mat4.rotateY(mvMatrix, rotateY, mvMatrix);
    mat4.rotateZ(mvMatrix, rotateZ, mvMatrix);
    mat4.toInverseMat3(mvMatrix, normalMatrix);
    mat3.transpose(normalMatrix);
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBufffer);
    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, vertexBufffer.itemSize, gl.FLOAT, false, 0, 0);
    setMatrixUniforms();

    // gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
    // gl.vertexAttribPointer(shaderProgram.vertexColorAttribute, 4, gl.FLOAT, false, 0, 0);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
    gl.drawElements(gl.TRIANGLES, indices.length, gl.UNSIGNED_SHORT, 0);
}

var vertices;
var normals;
var colors;

var indices; 
var normalBuffer;
function initBuffers() {
    vertexBufffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, null); // deactivate the current buffer
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBufffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW)
    vertexBufffer.itemSize = 3;
    vertexBufffer.numItems = vertices.length/3;
    indexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

    normalBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, normalBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(normals), gl.STATIC_DRAW);
    normalBuffer.itemSize = 3;
    normalBuffer.numItems = vertices.length / 3;

    colorBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);

    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBufffer);
    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, vertexBufffer.itemSize, gl.FLOAT, false, 0, 0);
    gl.vertexAttribPointer(shaderProgram.normalPositionAttribute,normalBuffer.itemSize,gl.FLOAT, false,0,0);
}



function calculateVertexNormals(vertices, indices) {
    var vertexVectors = [];
    var normalVectors = [];
    var normals = [];
    for (var i = 0; i < vertices.length; i = i + 3) {
        var vector = [vertices[i], vertices[i + 1], vertices[i + 2]];
        var normal = vec3.create();//Intiliazed normal array
        normalVectors.push(normal);
        vertexVectors.push(vector);
    }
    for (var j = 0; j < indices.length; j = j + 3)//Since we are using triads of indices to represent one primitive
    {
        //v1-v0
        var vector1 = vec3.create();
        var sub1 = vertexVectors[indices[j + 1]];
        var sub2 = vertexVectors[indices[j]];
        var sub3 = vertexVectors[indices[j + 2]];
        //vec3.subtract(vector1,sub1 ,sub2 );
        vector1 = subtractV3(sub1, sub2);
        //v2-v1
        var vector2 = vec3.create();
        // vec3.subtract(vector2,sub3, sub1);
        vector2 = subtractV3(sub3, sub1);
        var normal = vec3.create();
        //cross product of two vector
        vec3.cross(vector2, vector1, normal);
        //Since the normal caculated from three vertices is same for all the three vertices(same face/surface), the contribution from each normal to the corresponding vertex  is the same 
        vec3.add(normal, normalVectors[indices[j]], normalVectors[indices[j]]);
        vec3.add(normal, normalVectors[indices[j + 1]], normalVectors[indices[j + 1]]);
        vec3.add(normal, normalVectors[indices[j + 2]], normalVectors[indices[j + 2]]);
    }
    for (var j = 0; j < normalVectors.length; j = j + 1) {
        vec3.normalize(normalVectors[j], normalVectors[j]);
        normals.push(normalVectors[j][0]);
        normals.push(normalVectors[j][1]);
        normals.push(normalVectors[j][2]);

    }
    return normals;
}

function initShaders() {
    var vertexShader = createShader(gl, "vertex", vs_source);
    var fragmentShader = createShader(gl, "fragment", fs_source);
    
   // var vertexShader = createShader(gl, "shader-vs");
   // var fragmentShader = createShader(gl, "shader-fs");


    shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert("Shaders cannot be initalized");
    }
    gl.useProgram(shaderProgram);

    shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
    shaderProgram.vertexNormalAttribute = gl.getAttribLocation(shaderProgram, "aVertexNormal");
    gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);
    gl.enableVertexAttribArray(shaderProgram.vertexNormalAttribute);

    //shaderProgram.vertexColorAttribute = gl.getAttribLocation(shaderProgram, "vertexColor");
    //gl.enableVertexAttribArray(shaderProgram.vertexColorAttribute);

    shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "pMatrix");
    shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "mvMatrix");
    shaderProgram.normalMatrixUniform = gl.getUniformLocation(shaderProgram, "normalMatrix");
    shaderProgram.diffuseColor = gl.getUniformLocation(shaderProgram, "materialDiffuseColor");
    shaderProgram.ambientColor = gl.getUniformLocation(shaderProgram, "materialAmbientColor");
    shaderProgram.specularColor = gl.getUniformLocation(shaderProgram, "materialSpecularColor");
    shaderProgram.lightPosition = gl.getUniformLocation(shaderProgram, "lightPosition");
    shaderProgram.lightDiffuse = gl.getUniformLocation(shaderProgram, "lightDiffuse");
    shaderProgram.lightAmbient = gl.getUniformLocation(shaderProgram, "lightAmbient");
    shaderProgram.lightSpecular = gl.getUniformLocation(shaderProgram, "lightSpecular");


}
//check
function initGL(canvas) {
    var names = ["webgl", "experimental-webgl", "webkit-3d", "moz-webgl"];

    for (var i = 0; i < names.length; ++i) {
        try {
            gl = canvas.getContext(names[i]);
        }
        catch (e)
        { }
        if (gl) {
            break;
        }


    }
    if (gl == null) {
        alert("Could not initialize WebGL");
        return null;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    return true;
}


function setMatrixUniforms() {
    gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
    gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
    gl.uniformMatrix3fv(shaderProgram.normalMatrixUniform, false, normalMatrix);
    gl.uniform4fv(shaderProgram.diffuseColor, diffuseColor);
    gl.uniform4fv(shaderProgram.specularColor, specularColor);
    gl.uniform4fv(shaderProgram.ambientColor, ambientColor);
    gl.uniform3fv(shaderProgram.lightPosition, lightPosition);
    gl.uniform4fv(shaderProgram.lightDiffuse, lightDiffuse);
    gl.uniform4fv(shaderProgram.lightSpecular, lightSpecular);
    gl.uniform4fv(shaderProgram.lightAmbient, lightAmbient);
}



//function createShader(gl, id) {
//    var shaderScript = document.getElementById(id);
//    if (!shaderScript) {
//        return null;
//    }
//    var str = "";
//    var k = shaderScript.firstChild;
//    while (k) {
//        if (k.nodeType == 3) {
//            str += k.textContent;
//        }
//        k = k.nextSibling;
//    }

//    var shader;
//    if (shaderScript.type == "x-shader/x-fragment") {
//        shader = gl.createShader(gl.FRAGMENT_SHADER);
//    }
//    else if (shaderScript.type == "x-shader/x-vertex") {
//        shader = gl.createShader(gl.VERTEX_SHADER);
//    }
//    else {
//        return null;
//    }

//    gl.shaderSource(shader, str);
//    gl.compileShader(shader);

//    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
//        return null;
//    }

//    return shader;
//}
function createShader(gl, type, shaderCode) {

   // shaderCode = shaderCode.substring(2, shaderCode.length-2);
    var shader;
    if (type == "fragment") {
        shader = gl.createShader(gl.FRAGMENT_SHADER);
    }
    else if (type == "vertex") {
        shader = gl.createShader(gl.VERTEX_SHADER);
    }
    else {
        return null;
    }

    gl.shaderSource(shader, shaderCode);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        return null;
    }

    return shader;
}


*/