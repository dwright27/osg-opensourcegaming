﻿
Scene = function (gl) {
    this.sceneObjects = [];
    this.gl = gl;
};

Scene.prototype = {
    addModel: function (sceneObject) {
        if ((this.gl != undefined)) {
            sceneObject.createBuffers(this.gl);
        }
        this.sceneObjects.push(sceneObject);
    },

    addSceneObject: function (path, location, rotateX, rotateY, rotateZ) {
        var sceneObject
        $.getJSON(path,
     function (data) {
         sceneObject = new SceneObject();
         sceneObject.loadObject(data);
         sceneObject.location = location;
         sceneObject.rotateX = rotateX;
         sceneObject.rotateY = rotateY;
         sceneObject.rotateZ = rotateZ;


     });
        this.addModel(sceneObject);
    },

    addSceneObject: function (data, location) {
        var sceneObject
        sceneObject = new SceneObject();
        sceneObject.loadObject(data);
        sceneObject.location = location;
        sceneObject.rotateX = 0.0;
        sceneObject.rotateY = 0.0;
        sceneObject.rotateZ = 0.0;
        this.addModel(sceneObject);
    }
    ,
    addSceneObject: function (data, location,textCoords) {
    var sceneObject
    sceneObject = new SceneObject();
    sceneObject.loadObject(data);
    //sceneObject.geometry.textureCoords = textCoords;
    sceneObject.location = location;
    sceneObject.rotateX = 0.0;
    sceneObject.rotateY = 0.0;
    sceneObject.rotateZ = 0.0;
    this.addModel(sceneObject);
}

}