﻿




window.onload = start;
var shaderProgram;
var vertexBufffer;
var indexBuffer;
var colorBuffer;
var pMatrix = mat4.create();
var mvMatrix = mat4.create();
var normalMatrix = mat3.create();
var gl;

var cubeTexture;
var cubeVerticesTextureCoordBuffer;
var vs_source;
var fs_source;


var vertices = [
    // Front face
    -1.0, -1.0, 1.0,
     1.0, -1.0, 1.0,
     1.0, 1.0, 1.0,
    -1.0, 1.0, 1.0,

    // Back face
    -1.0, -1.0, -1.0,
    -1.0, 1.0, -1.0,
     1.0, 1.0, -1.0,
     1.0, -1.0, -1.0,

    // Top face
    -1.0, 1.0, -1.0,
    -1.0, 1.0, 1.0,
     1.0, 1.0, 1.0,
     1.0, 1.0, -1.0,

    // Bottom face
    -1.0, -1.0, -1.0,
     1.0, -1.0, -1.0,
     1.0, -1.0, 1.0,
    -1.0, -1.0, 1.0,

    // Right face
     1.0, -1.0, -1.0,
     1.0, 1.0, -1.0,
     1.0, 1.0, 1.0,
     1.0, -1.0, 1.0,

    // Left face
    -1.0, -1.0, -1.0,
    -1.0, -1.0, 1.0,
    -1.0, 1.0, 1.0,
    -1.0, 1.0, -1.0
];

var cubeVertexIndices = [
  0, 1, 2, 0, 2, 3,    // front
  4, 5, 6, 4, 6, 7,    // back
  8, 9, 10, 8, 10, 11,   // top
  12, 13, 14, 12, 14, 15,   // bottom
  16, 17, 18, 16, 18, 19,   // right
  20, 21, 22, 20, 22, 23    // left
]
var texCoords =
    [
        1.0, 1.0,
        0.0, 1.0,
        1.0, 0.0,
        0.0, 0.0
    ];

var textureCoordinates;

var path = "/Home/GetFile/monkey";

function loadShaderText() {
    var httpsRequest = new XMLHttpRequest();
    httpsRequest.open("GET", "Home/VertexShader/cubeTextureTestVS", false);
    httpsRequest.send();
    vs_source = httpsRequest.responseText.substring(1, httpsRequest.responseText.length - 1);

    httpsRequest.open("GET", "Home/PixelShader/cubeTextureTestPS", false);
    httpsRequest.send();
    fs_source = httpsRequest.responseText.substring(1, httpsRequest.responseText.length - 1);

}

function initAnimaitonFrame() {
    window.requestAnimationFrame = (function () {
        //var element = $('#squareWithDrawArrays')[0];
        return window.requestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function (callback, element) {
            window.setTimeout(callback, 1 / 60);
        };
    })();
}

function start() {
    var canvas = $('#squareWithDrawArrays')[0];
    initAnimaitonFrame();
    loadShaderText();
    initGL(canvas);
    initShaders();
    initBuffer();

    initTextures();
    gl.clearColor(0.4, 0.8, 0.5, 1.0);
    document.onkeydown = handleKeyPress;
    scene = new Scene(gl);
    initModels();

}

function initScene() {
    // initBuffers();
    gl.clearColor(0.2, 0.7, 0.4, 1.0);
    //enable depth and color
    gl.enable(gl.DEPTH_TEST);
    tick();

}


var KeyCode = {
    UP: 87,
    LEFT: 65,
    RIGHT: 68,
    DOWN: 83,
}

function handleKeyPress(event) {
    if (event.which == KeyCode.UP) {
        rotateY += 0.1;
    }
    if (event.which == KeyCode.DOWN) {
        rotateY -= 0.1;
    }
    if (event.which == KeyCode.RIGHT) {
        rotateX += 0.1;
    }
    if (event.which == KeyCode.LEFT) {
        rotateX -= 0.1;
    }

}


function initTextures() {
    cubeTexture = gl.createTexture();
    cubeImage = new Image();
    cubeImage.onload = function () { handleTextureLoaded(cubeImage, cubeTexture); }
    cubeImage.src = "Home/GetImage/cubetexture";
}

function tick() {
    requestAnimationFrame(tick);
    drawScene();
}



function initModels() {
    //$.getJSON(path,
    //function (data) {
    //    scene.addSceneObject(data, [-2, 0, -7.0], textureCoordinates);
    //    initScene();
    //});

    $.getJSON("/Home/GetFile/cube",
function (data) {
    scene.addSceneObject(data, [2, 0, -7.0], textureCoordinates);
    initScene();
});


    //    $.getJSON("/Home/GetFile/cylinder",
    //function (data) {
    //    scene.addSceneObject(data, [2, 0, -7.0], textureCoordinates);
    //    initScene();
    //});




}

var rotateX = 0.0;
var rotateY = 0.0;
function drawScene() {

    gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    mat4.perspective(40, gl.viewportWidth / gl.viewportHeight, 0.1, 1000.0, pMatrix);
    mat4.identity(mvMatrix);
    mat4.translate(mvMatrix, [0, 0, -7], mvMatrix);
    mat4.rotateX(mvMatrix, rotateX, mvMatrix);
    mat4.rotateY(mvMatrix, rotateY, mvMatrix);
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, cubeTexture);
    gl.uniform1i(gl.getUniformLocation(shaderProgram, "uSampler"), 0);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);

    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexBuffer);
    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, cubeVertexBuffer.itemSize, gl.FLOAT, false, 0, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, cubeTextureCoordBuffer);
    gl.vertexAttribPointer(shaderProgram.vertexTextureAttribute, cubeTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);


    setMatrixUniforms();
    gl.drawElements(gl.TRIANGLES, 36, gl.UNSIGNED_SHORT, 0);
    drawSceneObjects();
}

function drawSceneObjects() {
    for (var i = 0; i < scene.sceneObjects.length; i++) {

        mat4.identity(mvMatrix);
        // mat4.identity(normalMatrix);
        mat4.translate(mvMatrix, scene.sceneObjects[i].location, mvMatrix);
        mat4.rotateX(mvMatrix, scene.sceneObjects[i].rotateX, mvMatrix);
        mat4.rotateY(mvMatrix, scene.sceneObjects[i].rotateY, mvMatrix);
        mat4.rotateZ(mvMatrix, scene.sceneObjects[i].rotateZ, mvMatrix);
        scene.sceneObjects[i].rotateX += 0.01;

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, cubeTexture);
        gl.uniform1i(gl.getUniformLocation(shaderProgram, "uSampler"), 0);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, scene.sceneObjects[i].ibo);

        gl.bindBuffer(gl.ARRAY_BUFFER, scene.sceneObjects[i].vbo);
        gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, scene.sceneObjects[i].vbo.itemSize, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ARRAY_BUFFER, scene.sceneObjects[i].tbo);
        gl.vertexAttribPointer(shaderProgram.vertexTextureAttribute, scene.sceneObjects[i].tbo.itemSize, gl.FLOAT, false, 0, 0);

        // gl.bindBuffer(gl.ARRAY_BUFFER, cubeTextureCoordBuffer);
        // gl.vertexAttribPointer(shaderProgram.vertexTextureAttribute, cubeTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);



        //gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexBuffer);
        //gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, cubeVertexBuffer.itemSize, gl.FLOAT, false, 0, 0);

        //gl.bindBuffer(gl.ARRAY_BUFFER, cubeTextureCoordBuffer);
        //gl.vertexAttribPointer(shaderProgram.vertexTextureAttribute, cubeTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);


        setMatrixUniforms()
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, scene.sceneObjects[i].ibo);
        gl.drawElements(gl.TRIANGLES, scene.sceneObjects[i].geometry.indices.length, gl.UNSIGNED_SHORT, 0);

    }
}

function setMatrixUniforms() {
    gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
    gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
}

function initShaders() {
    var vertexShader = createShader(gl, "vertex", vs_source);
    var fragmentShader = createShader(gl, "fragment", fs_source);

    shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert("Shaders cannot be initalized");
    }
    gl.useProgram(shaderProgram);

    shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
    shaderProgram.vertexTextureAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");
    gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);
    gl.enableVertexAttribArray(shaderProgram.vertexTextureAttribute);

    shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "pMatrix");
    shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "mvMatrix");


}
function initBuffer() {
    cubeVertexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, null); // deactivate the current buffer
    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW)
    cubeVertexBuffer.itemSize = 3;
    cubeVertexBuffer.numItems = vertices.length / 3;
    indexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeVertexIndices), gl.STATIC_DRAW);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);



    cubeTextureCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, cubeTextureCoordBuffer);

    textureCoordinates = [
      // Front
      0.0, 0.0,
      1.0, 0.0,
      1.0, 1.0,
      0.0, 1.0,
      // Back
      0.0, 0.0,
      1.0, 0.0,
      1.0, 1.0,
      0.0, 1.0,
      // Top
      0.0, 0.0,
      1.0, 0.0,
      1.0, 1.0,
      0.0, 1.0,
      // Bottom
      0.0, 0.0,
      1.0, 0.0,
      1.0, 1.0,
      0.0, 1.0,
      // Right
      0.0, 0.0,
      1.0, 0.0,
      1.0, 1.0,
      0.0, 1.0,
      // Left
      0.0, 0.0,
      1.0, 0.0,
      1.0, 1.0,
      0.0, 1.0
    ];

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoordinates),
                  gl.STATIC_DRAW);

    cubeTextureCoordBuffer.itemSize = 2;
    cubeTextureCoordBuffer.numItems = textureCoordinates.length / 2;


    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexBuffer);
    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, cubeVertexBuffer.itemSize, gl.FLOAT, false, 0, 0);
    gl.bindBuffer(gl.ARRAY_BUFFER, cubeTextureCoordBuffer);
    gl.vertexAttribPointer(shaderProgram.texureCoordAttribute, cubeTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);


}


function handleTextureLoaded(image, texture) {
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
    gl.generateMipmap(gl.TEXTURE_2D);
    gl.uniform1i(gl.getUniformLocation(shaderProgram, "uSampler"), 0);
    gl.bindTexture(gl.TEXTURE_2D, null);

}

function createShader(gl, type, shaderCode) {

    // shaderCode = shaderCode.substring(2, shaderCode.length-2);
    var shader;
    if (type == "fragment") {
        shader = gl.createShader(gl.FRAGMENT_SHADER);
    }
    else if (type == "vertex") {
        shader = gl.createShader(gl.VERTEX_SHADER);
    }
    else {
        return null;
    }

    gl.shaderSource(shader, shaderCode);
    gl.compileShader(shader);


    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert(gl.getShaderInfoLog(shader));
        return null;
    }

    return shader;
}

function initShaders() {
    // initLights();
    var vertexShader = createShader(gl, "vertex", vs_source);
    var fragmentShader = createShader(gl, "fragment", fs_source);

    shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert("Shaders cannot be initalized");
    }
    gl.useProgram(shaderProgram);

    shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
    shaderProgram.vertexTextureAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");
    gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);
    gl.enableVertexAttribArray(shaderProgram.vertexTextureAttribute);

    //initTextBuffer();
    //shaderProgram.vertexColorAttribute = gl.getAttribLocation(shaderProgram, "vertexColor");
    //gl.enableVertexAttribArray(shaderProgram.vertexColorAttribute);

    shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "pMatrix");
    shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "mvMatrix");
}

//check
function initGL(canvas) {
    var names = ["webgl", "experimental-webgl", "webkit-3d", "moz-webgl"];

    for (var i = 0; i < names.length; ++i) {
        try {
            gl = canvas.getContext(names[i]);
        }
        catch (e)
        { }
        if (gl) {
            break;
        }


    }
    if (gl == null) {
        alert("Could not initialize WebGL");
        return null;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    return true;
}




//var shaderProgram;
//var vertexBufffer;
//var indexBuffer;
//var colorBuffer;
//var pMatrix = mat4.create();
//var mvMatrix = mat4.create();
//var normalMatrix = mat3.create();
//var gl;
//var path = "/Home/GetFile/monkey";
//var lightPosition = [0.0, 0.0, -4.0];
//var lightDiffuse = vec4.fromValues(0.7, 0.0, 0.1, 1.0);
//var lightAmbient = vec4.fromValues(0.2, 0.5, 0.3, 1.0);
//var lightSpecular = vec4.fromValues(0.2, 0.2, 0.7, 1.0);

//var diffuseColor = vec4.fromValues(0.5, 0.0, 0.9, 1.0);
//var ambientColor = [0.9, 0.2, 0.2];
//var specularColor = [0.5, 0.5, 0.9];
//var scene;
//var vs_source = null,
//    fs_source = null;
//var matrixStack = [];


//function pushMatrix() {
//    var copy = mat4.create();
//    //mat4.copy(copy, mvMatrix);
//    matrixStack.push(mvMatrix);
//}

//function popMatrix() {
//    if (matrixStack.length == 0) {
//        throw "Invalid pop Matrix";
//    }
//    mvMatrix = matrixStack.pop();
//}

//function loadShaderText() {
//    var httpsRequest = new XMLHttpRequest();
//    httpsRequest.open("GET", "Home/VertexShader/textureVS", false);
//    httpsRequest.send();
//    vs_source = httpsRequest.responseText.substring(1, httpsRequest.responseText.length - 1);

//    httpsRequest.open("GET", "Home/PixelShader/texturePS", false);
//    httpsRequest.send();
//    fs_source = httpsRequest.responseText.substring(1, httpsRequest.responseText.length - 1);

//}

//function start() {

//    // var canvas = document.getElementById("squareWithDrawArrays");
//    var canvas = $('#squareWithDrawArrays')[0];
//    initGL(canvas);
//    initAnimaitonFrame();
//    loadShaderText();
//    initShaders();
//    //initTextBuffer();
//     initTextures();
//    //initBuffers();
//    //initTextures();

//    scene = new Scene(gl);
//    //OVER 200 FOR BOTH
//    //alert(gl.getParameter(gl.MAX_VERTEX_UNIFORM_VECTORS));
//    //alert(gl.getParameter(gl.MAX_FRAGMENT_UNIFORM_VECTORS));

//    initModels();

//    //initScene();
//    // document.onkeydown = handleKeyDown;
//    document.onkeydown = handleKeyPress;
//    //gl.drawArrays(gl.TRIANGLES,0,vertices.numItems);
//    //gl.drawElements(gl.TRIANGLES)	
//};

//function objectChanged() {
//    // path = "Home/GetFile/" + document.getElementById("objectSelect").value.toString();
//    //initModels();
//}

//function initModels() {
//    //$.getJSON(path,
//    //function (data) {
//    //    scene.addSceneObject(data, [-2, 0, -7.0], texCoords);
//    //    initScene();
//    //});

//    $.getJSON("/Home/GetFile/cube",
//function (data) {
//    scene.addSceneObject(data, [0, 0, -7.0], texCoords);
//    initScene();
//});


////    $.getJSON("/Home/GetFile/cylinder",
////function (data) {
////    scene.addSceneObject(data, [2, 0, -7.0], texCoords);
////    initScene();
////});




//}

//function initAnimaitonFrame() {
//    window.requestAnimationFrame = (function () {
//        //var element = $('#squareWithDrawArrays')[0];
//        return window.requestAnimationFrame||
//  window.webkitRequestAnimationFrame || 
//        window.mozRequestAnimationFrame || 
//        window.oRequestAnimationFrame ||
//        window.msRequestAnimationFrame ||
//        function(callback, element) 
//            {
//                window.setTimeout(callback, 1 / 60);
//            };
//    })();
//}

//var rotateY = 0.0;
//var rotateZ = 0.0;

//function initScene() {
//    // initBuffers();
//    gl.clearColor(0.2, 0.7, 0.4, 1.0);
//    //enable depth and color
//    gl.enable(gl.DEPTH_TEST);
//    tick();

//}

//var KeyCode = {
//    UP: 87,
//    LEFT: 65,
//    RIGHT: 68,
//    DOWN: 83,
//}


//function handleKeyPress(event) {
//    if (event.which == KeyCode.UP) {
//        rotateY += 0.1;
//    }
//    if (event.which == KeyCode.DOWN) {
//        rotateY -= 0.1;
//    }
//    if (event.which == KeyCode.RIGHT) {
//        rotateZ += 0.1;
//    }
//    if (event.which == KeyCode.LEFT) {
//        rotateZ -= 0.1;
//    }

//}


//function tick() {
//    requestAnimationFrame(tick);
//    drawScene();
//}

//function drawScene() {

//    gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
//    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
//    mat4.perspective(40, gl.viewportWidth / gl.viewportHeight, 0.1, 1000.0, pMatrix);
//    for (var i = 0; i < scene.sceneObjects.length; i++) {
//        //pushMatrix();
//        diffuseColor = scene.sceneObjects[i].diffuseColor;
//        ambientColor = scene.sceneObjects[i].ambientColor;
//        specularColor = scene.sceneObjects[i].specularColor;

//        mat4.identity(mvMatrix);
//        mat4.identity(normalMatrix);
//        mat4.translate(mvMatrix, scene.sceneObjects[i].location, mvMatrix);
//        mat4.rotateX(mvMatrix, scene.sceneObjects[i].rotateX, mvMatrix);
//        mat4.rotateY(mvMatrix, scene.sceneObjects[i].rotateY, mvMatrix);
//        mat4.rotateZ(mvMatrix, scene.sceneObjects[i].rotateZ, mvMatrix);
//        scene.sceneObjects[i].rotateX += 0.01;



//        mat4.toInverseMat3(mvMatrix, normalMatrix);
//        mat3.transpose(normalMatrix);


//        setLightUniform();

//        // gl.bindBuffer(gl.ARRAY_BUFFER, vertexBufffer);
//        //gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, vertexBufffer.itemSize, gl.FLOAT, false, 0, 0);
//        setMatrixUniforms();

//        gl.bindBuffer(gl.ARRAY_BUFFER, scene.sceneObjects[i].vbo);
//        gl.vertexAttribPointer(shaderProgram.normalPositionAttribute, scene.sceneObjects[i].vbo.itemSize, gl.FLOAT, false, 0, 0);

//        gl.bindBuffer(gl.ARRAY_BUFFER, scene.sceneObjects[i].nbo);
//        gl.vertexAttribPointer(shaderProgram.normalPositionAttribute, scene.sceneObjects[i].nbo.itemSize, gl.FLOAT, false, 0, 0);

//        gl.bindBuffer(gl.ARRAY_BUFFER, scene.sceneObjects[i].tbo);
//        gl.vertexAttribPointer(shaderProgram.normalPositionAttribute, scene.sceneObjects[i].tbo.itemSize, gl.FLOAT, false, 0, 0);


//        //gl.activeTexture(gl.TEXTURE0);
//        //gl.bindTexture(gl.TEXTURE_2D, texture);
//        //gl.uniform1i(gl.getUniformLocation(shaderProgram,"uSampler"), 0 );

//        gl.bindBuffer(gl.ARRAY_BUFFER, scene.sceneObjects[i].vbo);
//        gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, scene.sceneObjects[i].vbo.itemSize, gl.FLOAT, false, 0, 0);
// //       gl.bindBuffer(gl.ARRAY_BUFFER, scene.sceneObjects[i].vbo);
//  //      gl.vertexAttribPointer(shaderProgram.vertexTextureAttribute, 2, gl.FLOAT, false, 0, 0);


//        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, scene.sceneObjects[i].ibo);
//        gl.drawElements(gl.TRIANGLES, scene.sceneObjects[i].geometry.indices.length, gl.UNSIGNED_SHORT, 0);
//        //popMatrix();
//    }

//}

//var vertices;
//var normals;
//var colors;
//var indices;
//var normalBuffer;
//function initBuffers() {
//    vertexBufffer = gl.createBuffer();
//    gl.bindBuffer(gl.ARRAY_BUFFER, null); // deactivate the current buffer
//    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBufffer);
//    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW)
//    vertexBufffer.itemSize = 3;
//    vertexBufffer.numItems = vertices.length / 3;
//    indexBuffer = gl.createBuffer();
//    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
//    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);
//    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

//    normalBuffer = gl.createBuffer();
//    gl.bindBuffer(gl.ARRAY_BUFFER, normalBuffer);
//    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(normals), gl.STATIC_DRAW);
//    normalBuffer.itemSize = 3;
//    normalBuffer.numItems = vertices.length / 3;

//    colorBuffer = gl.createBuffer();
//    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
//    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);

//    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBufffer);
//    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, vertexBufffer.itemSize, gl.FLOAT, false, 0, 0);
//    gl.vertexAttribPointer(shaderProgram.normalPositionAttribute, normalBuffer.itemSize, gl.FLOAT, false, 0, 0);

//}
//var lights = new Lights();

//var texCoords =
//    [
//        1.0, 1.0,
//        0.0, 1.0,
//        1.0, 0.0,
//        0.0, 0.0
//    ];

//var image;
//var texture;
//function initTextBuffer() {
//    verticesTextureBuffer = gl.createBuffer();
//    gl.bindBuffer(gl.ARRAY_BUFFER, verticesTextureBuffer);

//    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(texCoords), gl.STATIC_DRAW);
//    gl.vertexAttribPointer(shaderProgram.vertexTextureAttribute, 2, gl.FLOAT, false, 0, 0);

//    initTextures();
//}




//function initTextures() {
//    texture = gl.createTexture();
//    image = new Image();
//    image.src = "Home/GetImage/cubetexture";
//    image.onload = function () {
//        handleTextureLoaded(image, texture);
//    }
//}

//function handleTextureLoaded(img, texture) {
//    //gl.bindTexture(gl.TEXTURE_2D, texture);
//    //gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, img)
//    //gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
//    //gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
//    //gl.bindTexture(gl.TEXTURE_2D, null);
//    //gl.clearColor(0.0, 0.0, 0.0, 1.0);
//    // gl.enable(gl.DEPTH_TEST);
//    gl.bindTexture(gl.TEXTURE_2D, texture);
//    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, img);
//    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
//    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
//    gl.bindTexture(gl.TEXTURE_2D, null);
//    gl.clearColor(0.2, 0.7, 0.8, 1.0);
//    gl.enable(gl.DEPTH_TEST);
//    gl.activeTexture(gl.TEXTURE0);
//    gl.bindTexture(gl.TEXTURE_2D, texture);
//    gl.uniform1i(gl.getUniformLocation(shaderProgram, "uSampler"), 0);

//    //drawScene();

//}




//function initLights() {
//    var light = new Light();
//    light.ambientColor = [0.1, 0.1, 0.1];
//    light.diffuseColor = [0.5, 0.5, 0.5];
//    light.specularColor = [1.0, 1.0, 1.0];
//    light.direction = [0, -1.25, -1.25];
//    var light1 = new Light();
//    light1.ambientColor = [0.5, 0.5, 0.0];
//    light1.diffuseColor = [0.45, 1.0, 0.23];
//    light1.specularColor = [0.4, 0.2, 0.8];
//    light1.position = [20.0, 5.0, -100.0];
//  //  light1.direction = [0, 1.25, -1.25];

//    var light2 = new Light();
//    light2.ambientColor = [1.0, 1.0, 1.0];
//    light2.diffuseColor = [1.0, 1.0, 1.0];
//    light2.specularColor = [0.0, 0.0, 0.0];
//    light2.position = [20.0, 5.0, 200.0];
//   // light2.direction = [0, -1.25, 1.25];

//    lights.addLight(light);
//    lights.addLight(light1);
//    lights.addLight(light2);
//}


//function initShaders() {
//    initLights();
//    var vertexShader = createShader(gl, "vertex", vs_source);
//    var fragmentShader = createShader(gl, "fragment", fs_source);

//    shaderProgram = gl.createProgram();
//    gl.attachShader(shaderProgram, vertexShader);
//    gl.attachShader(shaderProgram, fragmentShader);
//    gl.linkProgram(shaderProgram);

//    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
//        alert("Shaders cannot be initalized");
//    }
//    gl.useProgram(shaderProgram);

//    shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
//    shaderProgram.vertexNormalAttribute = gl.getAttribLocation(shaderProgram, "aVertexNormal");
//    shaderProgram.vertexTextureAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");
//    gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);
//    gl.enableVertexAttribArray(shaderProgram.vertexNormalAttribute);
//    gl.enableVertexAttribArray(shaderProgram.vertexTextureAttribute);

//    //initTextBuffer();
//    //shaderProgram.vertexColorAttribute = gl.getAttribLocation(shaderProgram, "vertexColor");
//    //gl.enableVertexAttribArray(shaderProgram.vertexColorAttribute);

//    shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "pMatrix");
//    shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "mvMatrix");
//    shaderProgram.normalMatrixUniform = gl.getUniformLocation(shaderProgram, "normalMatrix");
//    shaderProgram.diffuseColor = gl.getUniformLocation(shaderProgram, "materialDiffuseColor");
//    shaderProgram.ambientColor = gl.getUniformLocation(shaderProgram, "materialAmbientColor");
//    shaderProgram.specularColor = gl.getUniformLocation(shaderProgram, "materialSpecularColor");
//    shaderProgram.lightPosition = gl.getUniformLocation(shaderProgram, "lightPosition");
//    shaderProgram.lightDiffuse = gl.getUniformLocation(shaderProgram, "lightDiffuse");
//    shaderProgram.lightAmbient = gl.getUniformLocation(shaderProgram, "lightAmbient");
//    shaderProgram.lightSpecular = gl.getUniformLocation(shaderProgram, "lightSpecular");
//    shaderProgram.position1=gl.getUniformLocation(shaderProgram,"uLightPositionLamp1");
//    shaderProgram.position2=gl.getUniformLocation(shaderProgram,"uLightPositionLamp2");
//    shaderProgram.lightColor1 = gl.getUniformLocation(shaderProgram, "positionColor1");
//    shaderProgram.lightColor2 = gl.getUniformLocation(shaderProgram, "positionColor2");

//    shaderProgram.positionSpecularColor = gl.getUniformLocation(shaderProgram, "positionSpecularColor");
//    shaderProgram.positionDiffuseColor = gl.getUniformLocation(shaderProgram, "positionDiffuseColor");
//    shaderProgram.positionAmbientColor = gl.getUniformLocation(shaderProgram, "positionAmbientColor");
//    shaderProgram.lightDirection = gl.getUniformLocation(shaderProgram, "lightDirection");
//}

////check
//function initGL(canvas) {
//    var names = ["webgl", "experimental-webgl", "webkit-3d", "moz-webgl"];

//    for (var i = 0; i < names.length; ++i) {
//        try {
//            gl = canvas.getContext(names[i]);
//        }
//        catch (e)
//        { }
//        if (gl) {
//            break;
//        }


//    }
//    if (gl == null) {
//        alert("Could not initialize WebGL");
//        return null;
//    }

//    gl.viewportWidth = canvas.width;
//    gl.viewportHeight = canvas.height;

//    return true;
//}


//function setMatrixUniforms() {
//    gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
//    gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
//    gl.uniformMatrix3fv(shaderProgram.normalMatrixUniform, false, normalMatrix);
//    gl.uniform4fv(shaderProgram.diffuseColor, diffuseColor);
//    gl.uniform3fv(shaderProgram.specularColor, specularColor);
//    gl.uniform3fv(shaderProgram.ambientColor, ambientColor);
//}

//var position1 = [-3, 0, 2];
//var position2 = [5,0,-7];
//var color1 = [0.9,0.9,0.0];
//var color2 = [0.7, 0.3, 0.2];

//function setLightUniform() {
//    gl.uniform3fv(shaderProgram.lightPosition, lights.getDataByType("position"));
//    gl.uniform3fv(shaderProgram.lightDirection, lights.getDataByType("direction"));
//    gl.uniform3fv(shaderProgram.lightDiffuse, lights.getDataByType("diffuseColor", "direction"));
//    gl.uniform3fv(shaderProgram.lightSpecular, lights.getDataByType("specularColor", "direction"));
//    gl.uniform3fv(shaderProgram.lightAmbient, lights.getDataByType("ambientColor"));
//    gl.uniform3fv(shaderProgram.lightColor, lights.getDataByType("specularColor", "position"));

//}



//function calculateVertexNormals(vertices, indices) {
//    var vertexVectors = [];
//    var normalVectors = [];
//    var normals = [];
//    for (var i = 0; i < vertices.length; i = i + 3) {
//        var vector = [vertices[i], vertices[i + 1], vertices[i + 2]];
//        var normal = vec3.create();//Intiliazed normal array
//        normalVectors.push(normal);
//        vertexVectors.push(vector);
//    }
//    for (var j = 0; j < indices.length; j = j + 3)//Since we are using triads of indices to represent one primitive
//    {
//        //v1-v0
//        var vector1 = vec3.create();
//        var sub1 = vertexVectors[indices[j + 1]];
//        var sub2 = vertexVectors[indices[j]];
//        var sub3 = vertexVectors[indices[j + 2]];
//        //vec3.subtract(vector1,sub1 ,sub2 );
//        vector1 = subtractV3(sub1, sub2);
//        //v2-v1
//        var vector2 = vec3.create();
//        // vec3.subtract(vector2,sub3, sub1);
//        vector2 = subtractV3(sub3, sub1);
//        var normal = vec3.create();
//        //cross product of two vector
//        vec3.cross(vector2, vector1, normal);
//        //Since the normal caculated from three vertices is same for all the three vertices(same face/surface), the contribution from each normal to the corresponding vertex  is the same 
//        vec3.add(normal, normalVectors[indices[j]], normalVectors[indices[j]]);
//        vec3.add(normal, normalVectors[indices[j + 1]], normalVectors[indices[j + 1]]);
//        vec3.add(normal, normalVectors[indices[j + 2]], normalVectors[indices[j + 2]]);
//    }
//    for (var j = 0; j < normalVectors.length; j = j + 1) {
//        vec3.normalize(normalVectors[j], normalVectors[j]);
//        normals.push(normalVectors[j][0]);
//        normals.push(normalVectors[j][1]);
//        normals.push(normalVectors[j][2]);

//    }
//    return normals;
//}


//function createShader(gl, type, shaderCode) {

//    // shaderCode = shaderCode.substring(2, shaderCode.length-2);
//    var shader;
//    if (type == "fragment") {
//        shader = gl.createShader(gl.FRAGMENT_SHADER);
//    }
//    else if (type == "vertex") {
//        shader = gl.createShader(gl.VERTEX_SHADER);
//    }
//    else {
//        return null;
//    }

//    gl.shaderSource(shader, shaderCode);
//    gl.compileShader(shader);


//    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
//        alert(gl.getShaderInfoLog(shader));
//        return null;
//    }

//    return shader;
//}
