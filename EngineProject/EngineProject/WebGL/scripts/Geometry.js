﻿Geometry = function () {
    this.vertices = [];
    this.uvs = [[]];
    this.colors = [];
    this.normals = [];
    this.indices = [];
    this.textureCoords = [];
    this.faces = [];
    this.faceUvs = [[]];
    this.faceVertexUvs = [[]];
    this.materials = [];
};
Geometry.prototype = {

    indicesFromFaces: function () {
        var indices = [];
        for (var i = 0; i < this.faces.length; i++) {
            this.indices.push(this.faces[i].a);
            this.indices.push(this.faces[i].b);
            this.indices.push(this.faces[i].c);
        }

    },

    morphedVertexNormalsFromObj: function () {
        var vertexVectors = [];
        var normalVectors = [];
        for (var i = 0; i < this.faces.length; i++) {
            if (!(this.faces[i].normal === undefined) && this.faces[i].vertexNormals.length === 0) {
                this.faces[i].vertexNormals["a"] = vec3.clone(this.faces[i].normal);
                this.faces[i].vertexNormals["b"] = vec3.clone(this.faces[i].normal);
                this.faces[i].vertexNormals["c"] = vec3.clone(this.faces[i].normal);

            }

            try {
                if (normalVectors[this.faces[i].a] === undefined) {
                    normalVectors[this.faces[i].a] = vec3.clone(this.faces[i].vertexNormals["a"]);
                }
                else {
                    vec3.add(normalVectors[this.faces[i].a], normalVectors[this.faces[i].a], this.faces[i].vertexNormals["a"]);
                }
                if (normalVectors[this.faces[i].b] === undefined) {
                    normalVectors[this.faces[i].b] = vec3.clone(this.faces[i].vertexNormals["b"]);
                }
                else {
                    vec3.add(normalVectors[this.faces[i].b], normalVectors[this.faces[i].b], this.faces[i].vertexNormals["b"]);
                }
                if (normalVectors[this.faces[i].c] === undefined) {
                    normalVectors[this.faces[i].c] = vec3.clone(this.faces[i].vertexNormals["c"]);
                }
                else {
                    vec3.add(normalVectors[this.faces[i].c], normalVectors[this.faces[i].c], this.faces[i].vertexNormals["c"]);
                }
            } catch (e) {
                alert(e);
            }

            //if (normalVectors[this.faces[i].a] == undefined) {
            //    alert(this.faces[i].a);
            //    normalVectors[this.faces[i].a] = [this.faces[i].vertexNormals["a"].x, this.faces[i].vertexNormals["a"].y, this.faces[i].vertexNormals["a"].z];
            //}
            //else {
            //    vec3.add(this.faces[i].vertexNormals["a"], normalVectors[this.faces[i].a], normalVectors[this.faces[i].a]);
            //    vec3.add(this.faces[i].vertexNormals["b"], normalVectors[this.faces[i].b], normalVectors[this.faces[i].b]);
            //    vec3.add(this.faces[i].vertexNormals["c"], normalVectors[this.faces[i].c], normalVectors[this.faces[i].c]);
            //}

        }
        this.normals = [];
        for (var i = 0; i < normalVectors.length; i++) {
            vec3.normalize(normalVectors[i], normalVectors[i]);
            this.normals.push(normalVectors[i][0]);
            this.normals.push(normalVectors[i][1]);
            this.normals.push(normalVectors[i][2]);
        }

    },

    verticesFromFaceUvs: function (vertices, uvs, materialIndex) {
        var vertexVectors = [];
        var redundantVertexVectors = [];
        var vertexCovered = [];

        for (var i = 0; i < vertices.length; i++) {
            var vector = [vertices[i], vertices[i + 1], vertices[i + 2]];
            vertexVectors.push(vector);
        }
        var count = 0;
        for (var i = 0; i < this.faceVertexUvs[materialIndex].length; i++) {
            var face = this.face[i];
            var textureIndices = this.faceVertexUvs[materialIndex][i];
            var aVertexIndices = ["a", "b", "c"];
            for (var i = 0; i < aVertexIndices.length; i++) {
                var aVertexIndex = aVertexIndices[j];
                if (redundantVertexVectors[textureIndices[aVertexIndex]] == face[aVertexIndex] || redundantVertexVectors[textureIndices[aVertexIndex]] == undefined) {
                    redundantVertexVectors[textureIndices[aVertexIndex]] = face[aVertexIndex];
                    face[aVertexIndex] = textureIndices[aVertexIndex];

                }
                else {
                    uvs[materialIndex].push(uvs[materialIndex][textureIndices[aVertexIndex] * 2]);
                    uvs[materialIndex].push(uvs[materialIndex][textureIndices[aVertexIndex] * 2 + 1]);
                    var newIndex = Math.floor(uvs[material].length / 2) - 1;
                    redundantVertexVectors[newIndex] = face[aVertexIndex];
                    face[aVertexIndex] = newIndex;
                }
            }
        }
        for (var i = 0; i < redundantVertexVectors.length; i++) {
            var vector = vertexVectors[redundantVertexVectors[i]];
            this.vertices.push(vector[0]);
            this.vertices.push(vector[1]);
            this.vertices.push(vector[2]);
        }
        this.uvs = uvs;
    }
    ,


    calculateVertexNormals: function () {
        var vertexVectors = [];
        var normalVectors = [];
        for (var i = 0; i < this.vertices.length; i = i + 3) {
            var vector = [this.vertices[i], this.vertices[i + 1], this.vertices[i + 2]];
            var normal = vec3.create();//Intiliazed normal array
            normalVectors.push(normal);
            vertexVectors.push(vector);
        }
        for (var j = 0; j < this.indices.length; j = j + 3)//Since we are using triads of indices to represent one primitive
        {
            //v1-v0
            var vector1 = vec3.create();
            var sub1 = vertexVectors[this.indices[j + 1]];
            var sub2 = vertexVectors[this.indices[j]];
            var sub3 = vertexVectors[this.indices[j + 2]];
            //vec3.subtract(vector1,sub1 ,sub2 );
            vector1 = subtractV3(sub1, sub2);
            //v2-v1
            var vector2 = vec3.create();
            // vec3.subtract(vector2,sub3, sub1);
            vector2 = subtractV3(sub3, sub1);
            var normal = vec3.create();
            //cross product of two vector
            vec3.cross(vector2, vector1, normal);
            //Since the normal caculated from three vertices is same for all the three vertices(same face/surface), the contribution from each normal to the corresponding vertex  is the same 
            vec3.add(normal, normalVectors[this.indices[j]], normalVectors[this.indices[j]]);
            vec3.add(normal, normalVectors[this.indices[j + 1]], normalVectors[this.indices[j + 1]]);
            vec3.add(normal, normalVectors[this.indices[j + 2]], normalVectors[this.indices[j + 2]]);
        }
        for (var j = 0; j < normalVectors.length; j = j + 1) {
            vec3.normalize(normalVectors[j], normalVectors[j]);
            this.normals.push(-normalVectors[j][0]);
            this.normals.push(-normalVectors[j][1]);
            this.normals.push(-normalVectors[j][2]);

        }

    }
};



    
function subtractV3(vec1, vec2) {
    return [vec1[0] - vec2[0], vec1[1] - vec2[1], vec1[2] - vec2[2]];
}
