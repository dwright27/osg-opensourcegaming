window.onload =function()
{
	body = document.getElementById("body");
	var outputVector = vec3.create();
	var outputVector1 = vec3.create();


	var vecOne = vec3.fromValues(2,3,4);
	var vecTwo = vec3.fromValues(5,4,6);
	var vecThree = vec4.fromValues(2,2,2,1);
	var I = mat4.create();
	var scale = mat4.create();
	//var finalMat = mat4.create();
	var scaleVector = vec3.fromValues(2,2,2);
	mat4.scale(scale,I,scaleVector);
	//mat4.rotate(finalMat,scale,.4);
	addHtml("vecOne : "  + vec3Html(vecOne));
	addHtml("vecTwo : " + vec3Html(vecTwo));
	addHtml("vecOne x vecTwo : " + vec3Html(vec3.cross(outputVector,vecOne,vecTwo)))
	addHtml("normalize (vecOne x vecTwo ): " + vec3Html(vec3.normalize(outputVector1,outputVector)));
	addHtml("vecOne dot vecTwo : " + vec3.dot(vecOne,vecTwo));
	var newVert = vec4.create(); 
	vec4.transformMat4(newVert,vecThree,scale);
	addHtml("scale vecOne : " + vec3Html(newVert));

};


function addHtml(htmlText)
{
	body = document.getElementById("body");
	body.innerHTML = body.innerHTML + htmlText + "<br/>";
}

function vec3Html(vector)
{
	return "( " + vector[0] + ", " + vector[1] + ", " + vector[2]+ ")";
}

/*matrix functions 
mat4.
create() : void
identity(mat4 out) : void 
determinant(mat4 matrix) : mat4 result
invert(mat4 out, mat4 matrix) : void 
transpose(mat4 out, mat4 matrix) : void 
rotate(mat4 out, mat4 matrix, float radians ) : void 
scale(mat4 out, mat4 matrix, vec3 vector)
perspective(mat4 out, float fovy, float aspect, float near, float far)
*/
