﻿Camera = function () {
    this.left = [1.0, 0.0, 0.0];
    this.up = [0.0, 1.0, 0.0];
    this.dir = [0.0, 0.0, 1.0];
    this.position = [0.0, 0.0, 0.0];
    this.projectionTransform = null;
    this.projMatrix;
    this.viewMarix;
    this.fieldOfView = 55;
    this.nearClippingPlane = 0.1;
    this.farClippingPlane = 1000.0;
};

Camera.prototype.getLeft = function () {
    return vec3.clone(this.left);
}

Camera.prototype.getPosition = function ()
{
    return vec3.clone(this.position);
}

Camera.prototype.getProjectionMatrix = function ()
{
    return mat4.clone(this.projMatrix);
}

Camera.prototype.getViewMatrix = function ()
{
    return mat4.clone(this.viewMatrix);
}

Camera.prototype.getUp = function ()
{
    return vec3.clone(this.up);
}

Camera.prototype.getNearClippingPlane = function () {
    return this.nearClippingPlane;
}

Camera.prototype.getFieldOfView = function (fcp) {

    return this.nearClippingPlane;
}
Camera.prototype.setNearClippingPlane = function (ncp) {
    if (ncp > 0) {
        this.nearClippingPlane = ncp;
    }
}

Camera.prototype.setFarClippingPlane = function (fcp) {
    if (fcp > 0) {
        this.farClippingPlane = fcp;
    }
}

Camera.prototype.setFieldOfView = function (fov) {
    if (fov > 0 && fov < 180)
    {
        this.fieldOfView = fov;
    }
}

Camera.prototype.apply = function (aspectRatio)
{
    var matView = mat4.create();
    var lookAtPosition = vec3.create();
    vec3.add(lookAtPosition, this.position, this.dir);
    mat4.lookAt(this.position, lookAtPosition, this.up, matView);
    mat4.translate(matView, [-this.position[0], -this.position[1], -this.position[2]]);
    this.viewMatrix = matView;
    this.projMatrix - mat4.create();
    mat4.perspective(this.projMatrix, (this.fieldOfView * 3.14)/180, this.aspectRatio, this.nearClippingPlane, this.farClippingPlane);
}