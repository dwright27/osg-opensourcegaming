﻿MouseInteractor = function(canvas , camera){
    this.dragging = false;
    this.canvas = canvas;
    this.camera = camera;
    this.x = 0;
    this.y = 0;
    this.lastX = 0;
    this.lastY = 0;
    this.button = null;
    this.shift = null;
    this.SENSITIVITY  =0.1;
    this.setUp();
};

MouseInteractor.prototype.mousePosX = function (x) {
    return 2 * (x / this.canvas.width) - 1;
};

MouseInteractor.prototype.mousePosY = function (y) {
    return 2 * (y / this.canvas.width) - 1;
};

MouseInteractor.prototype.setUp = function () {
    var self = this;
    this.canvas.onmousedown = function(event)
    {
        self.onMouseDown(event);
    };
    this.canvas.onmouseup = function(event)
    {
        self.onMouseUp(event);
    };
    this.canvas.onmousemove = function(event)
    {
        self.onMouseMove(event);
    };
};

MouseInteractor.prototype.onMouseUp = function(event){
    this.dragging = false;
};
MouseInteractor.prototype.onMouseDown = function (event) {
    this.dragging = true;
    this.x = event.clientX;
    this.y = event.clientY;
    this.button = event.button;
};

MouseInteractor.prototype.translate = function (value) {
    var translateSensitivity = 30 * this.SENSITIVITY;
    var c = this.camera;
    var dv = translateSensitivity * value / this.canvas.height;
    c.moveForward(dv);
}

MouseInteractor.prototype.rotate = function (dx, dy) {
    var camera = this.camera;
    camera.yaw(this.SENSITIVITY * dx);
    camera.pitch(this.SENSITIVITY * -dy);
};

MouseInteractor.prototype.onMouseMove = function (event) {
    this.lastX = this.x;
    this.lastY = this.y;
    this.x = event.clientX;
    this.y = event.clientY;

    if (this.dragging)
    {
        this.shift = event.shiftKey;
        if (this.button == 0)
        {
            if (this.shift) {
                var dx = this.mousePosX(this.x) - this.mousePosX(this.lastX);
                var dy = this.mousePosY(this.y) - this.mousePosY(this.lastY);
                this.rotate(dx, dy);
            }
            else {
                var dy = this.y - this.lastY;
                this.translate(dy);
            }
        }
    }

}
