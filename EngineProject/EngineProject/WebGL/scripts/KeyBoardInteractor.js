﻿KeyBoardInteractor  = function (camera, canvas) {
    this.cam = camera;
    this.canvas = canvas;
    this.setUp();
    this.MOVE_AMOUNT = 1;
}

KeyBoardInteractor.prototype.setUp = function () {
    var self = this;
    this.canvas.onkeydown = function (event)
    {
        self.handleKeys(event);
    }
}

KeyBoardInteractor.prototype.handleKeys = function (event) {
    var movement = Math.PI * 0.025;
    
    if (event.shiftKey) {
        switch (event.keyCode) {
            case 65:
                this.cam.roll(-movement); //tilt Left
                break;
            case 37:
                this.cam.yaw(-movement); // rotate Left
                break;
            case 68:
                this.cam.roll(movement); //tilt right  
                break;
            case 39:// right arrow
                this.cam.yaw(movement);
                break;
            case 83://s
            case 40://down
                this.cam.pitch(movement);
                break;
            case 87: //w
            case 38: // up arrow
                this.cam.pitch(-movement);
                break;



        }
    }
    else {
        var position = this.cam.getPosition();
        switch (event.keyCode) {
            case 65://a
            case 37:
                this.cam.setPosition([position[0]+ this.MOVE_AMOUNT, position[1], position[2]]);
                break;
            case 68:// d
            case 39:
                this.cam.setPosition([position[0] - this.MOVE_AMOUNT, position[1], position[2]]);
                break;
            case 83: // s
                this.cam.setPosition([position[0], position[1] - this.MOVE_AMOUNT, position[2]]);
                break;
            case 40:// down
                this.cam.setPosition([position[0], position[1], position[2] + this.MOVE_AMOUNT]);
                break;
            case 87://w
                this.cam.setPosition([position[0], position[1] + this.MOVE_AMOUNT, position[2]]);
                break;
            case 38:// up
                this.cam.setPosition([position[0], position[1], position[2] - this.MOVE_AMOUNT]);
        }
    }
}