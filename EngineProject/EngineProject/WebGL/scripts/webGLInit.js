




window.onload = start;
var shaderProgram;
var vertexBufffer;
var indexBuffer;
var colorBuffer;
var pMatrix = mat4.create();
var mvMatrix = mat4.create();
var normalMatrix = mat3.create();
var gl;

var cubeTexture;
var cubeVerticesTextureCoordBuffer;
var vs_source;
var fs_source;

var light = new Light();
var materialDiffuseColor;
var materialAmbientColor;
var materialSpecularColor;


var vertices = [
    // Front face
    -1.0, -1.0, 1.0,
     1.0, -1.0, 1.0,
     1.0, 1.0, 1.0,
    -1.0, 1.0, 1.0,

    // Back face
    -1.0, -1.0, -1.0,
    -1.0, 1.0, -1.0,
     1.0, 1.0, -1.0,
     1.0, -1.0, -1.0,

    // Top face
    -1.0, 1.0, -1.0,
    -1.0, 1.0, 1.0,
     1.0, 1.0, 1.0,
     1.0, 1.0, -1.0,

    // Bottom face
    -1.0, -1.0, -1.0,
     1.0, -1.0, -1.0,
     1.0, -1.0, 1.0,
    -1.0, -1.0, 1.0,

    // Right face
     1.0, -1.0, -1.0,
     1.0, 1.0, -1.0,
     1.0, 1.0, 1.0,
     1.0, -1.0, 1.0,

    // Left face
    -1.0, -1.0, -1.0,
    -1.0, -1.0, 1.0,
    -1.0, 1.0, 1.0,
    -1.0, 1.0, -1.0
];

var cubeVertexIndices = [
  0, 1, 2, 0, 2, 3,    // front
  4, 5, 6, 4, 6, 7,    // back
  8, 9, 10, 8, 10, 11,   // top
  12, 13, 14, 12, 14, 15,   // bottom
  16, 17, 18, 16, 18, 19,   // right
  20, 21, 22, 20, 22, 23    // left
];
var texCoords =
    [
        1.0, 1.0,
        0.0, 1.0,
        1.0, 0.0,
        0.0, 0.0
    ];


var KeyCode = {
    UP: 87,
    LEFT: 65,
    RIGHT: 68,
    DOWN: 83,
};

var cam = new FreeCamera();
var keyboard;
var mouse;

var textureCoordinates;

var path = "/Home/GetFile/monkey";

function loadShaderText() {
    var httpsRequest = new XMLHttpRequest();
    httpsRequest.open("GET", "Home/VertexShader/presentVS", false);
    httpsRequest.send();
    vs_source = httpsRequest.responseText.substring(1, httpsRequest.responseText.length - 1);

    httpsRequest.open("GET", "Home/PixelShader/presentPS", false);
    httpsRequest.send();
    fs_source = httpsRequest.responseText.substring(1, httpsRequest.responseText.length - 1);

}

function initAnimaitonFrame() {
    window.requestAnimationFrame = (function () {
        //var element = $('#squareWithDrawArrays')[0];
        return window.requestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function (callback, element) {
            window.setTimeout(callback, 1 / 60);
        };
    })();
}
var canvas;
function start() {
    canvas = $('#squareWithDrawArrays')[0];
    initAnimaitonFrame();
    loadShaderText();
    initGL(canvas);
    initCamera();
    initLight();
    initShaders();
    initBuffer();


    initTextures();
    gl.clearColor(0.4, 0.8, 0.5, 1.0);
    document.onkeydown = canvas.onkeydown;
    document.onmousedown = canvas.onmousedown;
    document.onmousemove = canvas.onmousemove;
    document.onmouseup = canvas.onmouseup;
    scene = new Scene(gl);
    initModels();

}

function initLight() {
    light.diffuseColor = [0.9, 0.4, 0.1];
    light.specularColor = [0.3, 0.1, 0.4];
    light.ambientColor = [0.7, 0.9, 0.3];
    light.direction = [0.5, 0.0, 0.5];
    light.position = [-5, 0, 7];
}

function calcMvMatrix() {
    cam.apply(gl.viewportWidth / gl.viewportHeight);
    mat4.multiply(mvMatrix, cam.viewMatrix, mvMatrix);
}

var cameraPosition = [0.0, 0.0, 2.0];

function initCamera() {
    cam.setPosition(cameraPosition);
    cam.setLookAtPoint([0.0, 0.0, 1.0]);
    keyboard = new KeyBoardInteractor(cam, canvas);
    mouse = new MouseInteractor(canvas, cam);
}

function initScene() {
    // initBuffers();
    gl.clearColor(0.2, 0.7, 0.4, 1.0);
    //enable depth and color
    gl.enable(gl.DEPTH_TEST);
    tick();

}


function initTextures() {
    cubeTexture = gl.createTexture();
    cubeImage = new Image();
    cubeImage.onload = function () { handleTextureLoaded(cubeImage, cubeTexture); };
    cubeImage.src = "Home/GetImage/cubetexture";
}

function tick() {
    requestAnimationFrame(tick);
    drawScene();
}



function initModels() {
    $.getJSON(path,
    function (data) {
        scene.addSceneObject(data, [-2, 0, -7.0], textureCoordinates);
        initScene();
    });

    //    $.getJSON("/Home/GetFile/cube",
    //function (data) {
    //    scene.addSceneObject(data, [2, 0, -7.0], textureCoordinates);
    //    initScene();
    //});


    $.getJSON("/Home/GetFile/cylinder",
function (data) {
    scene.addSceneObject(data, [2, 0, -7.0], textureCoordinates);
    initScene();
});
}

var rotateX = 0.0;
var rotateY = 0.0;
function drawScene() {

    gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    mat4.perspective(40, gl.viewportWidth / gl.viewportHeight, 0.1, 1000.0, pMatrix);
    //mat4.identity(mvMatrix);
    //calcMvMatrix();
    //mat4.translate(mvMatrix, [0, 0, -7], mvMatrix);
    //mat4.rotateX(mvMatrix, rotateX, mvMatrix);
    //mat4.rotateY(mvMatrix, rotateY, mvMatrix);
    //gl.activeTexture(gl.TEXTURE0);
    //gl.bindTexture(gl.TEXTURE_2D, cubeTexture);
    //gl.uniform1i(gl.getUniformLocation(shaderProgram, "uSampler"), 0);
    //gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);

    //gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexBuffer);
    //gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, cubeVertexBuffer.itemSize, gl.FLOAT, false, 0, 0);

    //gl.bindBuffer(gl.ARRAY_BUFFER, cubeTextureCoordBuffer);
    //gl.vertexAttribPointer(shaderProgram.vertexTextureAttribute, cubeTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);


    //setMatrixUniforms();
    //gl.drawElements(gl.TRIANGLES, 36, gl.UNSIGNED_SHORT, 0);
    drawSceneObjects();
}

var normalMatrix;
function drawSceneObjects() {
    for (var i = 0; i < scene.sceneObjects.length; i++) {

        mat4.identity(mvMatrix);
        mat4.identity(normalMatrix);
        calcMvMatrix();
        // mat4.identity(normalMatrix);
        mat4.translate(mvMatrix, scene.sceneObjects[i].location, mvMatrix);
        mat4.rotateX(mvMatrix, scene.sceneObjects[i].rotateX, mvMatrix);
        mat4.rotateY(mvMatrix, scene.sceneObjects[i].rotateY, mvMatrix);
        mat4.rotateZ(mvMatrix, scene.sceneObjects[i].rotateZ, mvMatrix);
        scene.sceneObjects[i].rotateX += 0.01;
        mat4.toInverseMat3(mvMatrix, normalMatrix);
        mat3.transpose(normalMatrix);


        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, cubeTexture);
        gl.uniform1i(gl.getUniformLocation(shaderProgram, "uSampler"), 0);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, scene.sceneObjects[i].ibo);

        gl.bindBuffer(gl.ARRAY_BUFFER, scene.sceneObjects[i].vbo);
        gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, scene.sceneObjects[i].vbo.itemSize, gl.FLOAT, false, 0, 0);
        
         gl.bindBuffer(gl.ARRAY_BUFFER, scene.sceneObjects[i].nbo);
        gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, scene.sceneObjects[i].nbo.itemSize, gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, scene.sceneObjects[i].tbo);
        gl.vertexAttribPointer(shaderProgram.vertexTextureAttribute, scene.sceneObjects[i].tbo.itemSize, gl.FLOAT, false, 0, 0);

        // gl.bindBuffer(gl.ARRAY_BUFFER, cubeTextureCoordBuffer);
        // gl.vertexAttribPointer(shaderProgram.vertexTextureAttribute, cubeTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);



        //gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexBuffer);
        //gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, cubeVertexBuffer.itemSize, gl.FLOAT, false, 0, 0);

        //gl.bindBuffer(gl.ARRAY_BUFFER, cubeTextureCoordBuffer);
        //gl.vertexAttribPointer(shaderProgram.vertexTextureAttribute, cubeTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);
        materialDiffuseColor = scene.sceneObjects[i].diffuseColor;
        materialAmbientColor = scene.sceneObjects[i].ambientColor;
        materialSpecularColor = scene.sceneObjects[i].specularColor;
        setMaterialUniforms();
        setMatrixUniforms();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, scene.sceneObjects[i].ibo);
        gl.drawElements(gl.TRIANGLES, scene.sceneObjects[i].geometry.indices.length, gl.UNSIGNED_SHORT, 0);

    }
}

function setMatrixUniforms() {
    gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
    gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
    gl.uniformMatrix4fv(shaderProgram.normalMatrixUniform, false, normalMatrix);
}

function setMaterialUniforms() {
    gl.uniform4fv(shaderProgram.materialDiffuse, materialDiffuseColor);
    gl.uniform4fv(shaderProgram.materialAmbient, materialAmbientColor);
    gl.uniform4fv(shaderProgram.materialSpecular, materialSpecularColor);
}


function setLightUniforms() {
    gl.uniform4fv(shaderProgram.lightDiffuse, light.diffuseColor);
    gl.uniform4fv(shaderProgram.lightAmbient, light.ambientColor);
    gl.uniform4fv(shaderProgram.lightSpecular, light.specularColor);
    gl.uniform4fv(shaderProgram.lightPosition, light.position);
    gl.uniform4fv(shaderProgram.lightDirection, light.direction);



}

//function initShaders() {
//    var vertexShader = createShader(gl, "vertex", vs_source);
//    var fragmentShader = createShader(gl, "fragment", fs_source);

//    shaderProgram = gl.createProgram();
//    gl.attachShader(shaderProgram, vertexShader);
//    gl.attachShader(shaderProgram, fragmentShader);
//    gl.linkProgram(shaderProgram);

//    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
//        alert("Shaders cannot be initalized");
//    }
//    gl.useProgram(shaderProgram);

//    shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
//    shaderProgram.vertexNormalAttribute = gl.getAttribLocation(shaderProgram, "aVertexNormal");
//    shaderProgram.vertexTextureAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");
//    gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);
//    gl.enableVertexAttribArray(shaderProgram.vertexTextureAttribute);
//    gl.enableVertexAttribArray(shaderProgram.vertexNormalAttribute);

//    shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "pMatrix");
//    shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "normalMatrix")
//    shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "mvMatrix");
//    shaderProgram.lightPositionUniform = gl.getUniformLocation(shaderProgram, "lightPosition");
//    shaderProgram.lightSpecularUniform = gl.getUniformLocation(shaderProgram, "lightSpecular");
//    shaderProgram.lightDiffuseUniform = gl.getUniformLocation(shaderProgram, "lightDiffuse");
//    shaderProgram.lightDirectionUniform = gl.getUniformLocation(shaderProgram, "lightDirection");
//    shaderProgram.lightAmbientUniform = gl.getUniformLocation(shaderProgram, "lightAmbient");
//    shaderProgram.materialSpecularUniform = gl.getUniformLocation(shaderProgram,"materialSpecularColor");
//    shaderProgram.materialDiffuseUniform = gl.getUniformLocation(shaderProgram, "materialDiffuseColor");
//    shaderProgram.materialAmbientUniform = gl.getUniformLocation(shaderProgram, "materialAmbientColor");

//}
function initBuffer() {
    cubeVertexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, null); // deactivate the current buffer
    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
    cubeVertexBuffer.itemSize = 3;
    cubeVertexBuffer.numItems = vertices.length / 3;
    indexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeVertexIndices), gl.STATIC_DRAW);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);



    cubeTextureCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, cubeTextureCoordBuffer);

    textureCoordinates = [
      // Front
      0.0, 0.0,
      1.0, 0.0,
      1.0, 1.0,
      0.0, 1.0,
      // Back
      0.0, 0.0,
      1.0, 0.0,
      1.0, 1.0,
      0.0, 1.0,
      // Top
      0.0, 0.0,
      1.0, 0.0,
      1.0, 1.0,
      0.0, 1.0,
      // Bottom
      0.0, 0.0,
      1.0, 0.0,
      1.0, 1.0,
      0.0, 1.0,
      // Right
      0.0, 0.0,
      1.0, 0.0,
      1.0, 1.0,
      0.0, 1.0,
      // Left
      0.0, 0.0,
      1.0, 0.0,
      1.0, 1.0,
      0.0, 1.0
    ];

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoordinates),
                  gl.STATIC_DRAW);

    cubeTextureCoordBuffer.itemSize = 2;
    cubeTextureCoordBuffer.numItems = textureCoordinates.length / 2;


    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexBuffer);
    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, cubeVertexBuffer.itemSize, gl.FLOAT, false, 0, 0);
    gl.bindBuffer(gl.ARRAY_BUFFER, cubeTextureCoordBuffer);
    gl.vertexAttribPointer(shaderProgram.texureCoordAttribute, cubeTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);


}


function handleTextureLoaded(image, texture) {
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
    gl.generateMipmap(gl.TEXTURE_2D);
    gl.uniform1i(gl.getUniformLocation(shaderProgram, "uSampler"), 0);
    gl.bindTexture(gl.TEXTURE_2D, null);

}

function createShader(gl, type, shaderCode) {

    // shaderCode = shaderCode.substring(2, shaderCode.length-2);
    var shader;
    if (type === "fragment") {
        shader = gl.createShader(gl.FRAGMENT_SHADER);
    }
    else if (type === "vertex") {
        shader = gl.createShader(gl.VERTEX_SHADER);
    }
    else {
        return null;
    }

    gl.shaderSource(shader, shaderCode);
    gl.compileShader(shader);


    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert(gl.getShaderInfoLog(shader));
        return null;
    }

    return shader;
}

function initShaders() {
    // initLights();
    var vertexShader = createShader(gl, "vertex", vs_source);
    var fragmentShader = createShader(gl, "fragment", fs_source);

    shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert("Shaders cannot be initalized");
    }
    gl.useProgram(shaderProgram);

    shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
    //normal
    shaderProgram.vertexNormalAttribute = gl.getAttribLocation(shaderProgram, "aVertexNormal");
    shaderProgram.vertexTextureAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");
    gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);
    //normal
    gl.enableVertexAttribArray(shaderProgram.vertexTextureAttribute);
    gl.enableVertexAttribArray(shaderProgram.vertexNormalAttribute);

    shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "pMatrix");
    shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "normalMatrix")
    shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "mvMatrix");
    shaderProgram.lightPositionUniform = gl.getUniformLocation(shaderProgram, "lightPosition");
    shaderProgram.lightSpecularUniform = gl.getUniformLocation(shaderProgram, "lightSpecular");
    shaderProgram.lightDiffuseUniform = gl.getUniformLocation(shaderProgram, "lightDiffuse");
    shaderProgram.lightDirectionUniform = gl.getUniformLocation(shaderProgram, "lightDirection");
    shaderProgram.lightAmbientUniform = gl.getUniformLocation(shaderProgram, "lightAmbient");
    shaderProgram.materialSpecularUniform = gl.getUniformLocation(shaderProgram, "materialSpecularColor");
    shaderProgram.materialDiffuseUniform = gl.getUniformLocation(shaderProgram, "materialDiffuseColor");
    shaderProgram.materialAmbientUniform = gl.getUniformLocation(shaderProgram, "materialAmbientColor");
}

//check
function initGL(canvas) {
    var names = ["webgl", "experimental-webgl", "webkit-3d", "moz-webgl"];

    for (var i = 0; i < names.length; ++i) {
        try {
            gl = canvas.getContext(names[i]);
        }
        catch (e)
        { }
        if (gl) {
            break;
        }
    }
    if (gl === null) {
        alert("Could not initialize WebGL");
        return null;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    return true;
}




