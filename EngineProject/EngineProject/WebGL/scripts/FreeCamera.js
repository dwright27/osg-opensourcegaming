﻿FreeCamera = inherit(Camera, function () {
    superc(this);
    this.linVel = [0.0, 0.0, 0.0];
    this.angVel = [0.0, 0.0, 0.0];
    this.velVelc = [0.0, 0.0, 0.0];
});

FreeCamera.prototype.setPosition = function (newPosition)
{
    this.position = [newPosition[0], newPosition[1], newPosition[2]]
}

FreeCamera.prototype.setLookAtPoint = function (lookAtPoint) {
    if (isVectorEqual(this.position, [0, 0, 0]) && isVectorEqual(lookAtPoint, [0, 0, 0]))
    {

    }
    else
    {
        vec3.subtract(this.dir, lookAtPoint, this.position);
        vec3.normalize(this.dir, this.dir);
        vec3.cross(this.left, [0, 1, 0], this.dir);
        vec3.normalize(this.left, this.left);
        vec3.cross(this.up, this.dir, this.left);
        vec3.normalize(this.up, this.up);
    }
}

FreeCamera.prototype.rotateOnAxis = function (axisVec, angle) {
    var quate = quat.create();
    quat.setAxisAngle(quate, axisVec, angle)
    vec3.transformQuat(this.dir, this.dir, quate);
    vec3.transformQuat(this.left, this.left, quate);
    vec3.transformQuat(this.up, this.up, quate);

    vec3.normalize(this.up, this.up);
    vec3.normalize(this.left, this.left);
    vec3.normalize(this.dir, this.dir);

};

FreeCamera.prototype.yaw = function (angle) {
    this.rotateOnAxis(this.up, angle);
};

FreeCamera.prototype.pitch = function (angle) {
    this.rotateOnAxis(this.left, angle);
};

FreeCamera.prototype.roll = function (angle) {
    this.rotateOnAxis(this.dir, angle);
};

FreeCamera.prototype.moveForward = function (s) {
    var newPosition = [this.position[0] - s * this.dir[0], this.position[1] - s * this.dir[1], this.position[2] - s * this.dir[2]];
    this.setPosition(newPosition);
};

FreeCamera.prototype.setAngularVel = function (newVelocity) {
    this.angVel[0] = newVelocity[0];
    this.angVel[1] = newVelocity[1];
    this.angVel[2] = newVelocity[2];
};

FreeCamera.prototype.getAngluarVel = function () {
    return vec3.clone(this.angVel);
};

FreeCamera.prototype.getLinearVel = function () {
    return vec3.clone(this.linVel);
};

FreeCamera.prototype.setLinearVel = function (newVelocity) {
    this.linVel[0] = newVelocity[0];
    this.linVel[1] = newVelocity[1];
    this.linVel[2] = newVelocity[2];
};

FreeCamera.prototype.update = function (timeStep) {
    if (vec3.squaredLength(this.linVel) == 0 && vec3.squaredLength(this.angVel) == 0) return false;
    if (vec3.squaredLength(this.linVel) > 0.0)
    {
        vec3.scale(this.velVec, this.velVec, timeStep);
        vec3.add(this.position, this.velVec, this.position);
    }
    if (vec3.squaredLength(this.angVel) > 0.0)
    {
        this.pitch(this.angVel[0] * timeStep);
        this.yaw(this.angVel[1] * timeStep);
        this.roll(this.angVel[2] * timeStep);
    }

    return true;
};