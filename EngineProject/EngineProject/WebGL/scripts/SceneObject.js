﻿


SceneObject = function () {
    this.name = "";
    this.geometry = new Geometry();
    this.location = [0, 0, 0];
    this.rotationX = 0.0;
    this.rotationY = 0.0;
    this.rotationZ = 0.0;
    this.ibo = null;
    this.vbo = null;
    this.nbo = null;
    this.tbo = null;
    this.diffuseColor = [1.0, 1.0, 1.0, 1.0];
    this.ambientColor = [1.0, 1.0, 1.0];
    this.specularColor = [0.000000000000001, 0.000000000000001, 0.000000000000001];
    this.loaded = false;
};

SceneObject.prototype = {
    loadObject: function (data) {
        this.geometry = parseJSON(data);
        this.name = data.metadata.sourceFile.split(".")[0];
        if (this.geometry.materials.length > 0) {
            if (!(this.geometry.materials[0].colorDiffuse === undefined)) {
                this.diffuseColor = this.geometry.materials[0].colorDiffuse;
                this.diffuseColor.push(1.0);
            }
            if (!(this.geometry.materials[0].colorAmbient === undefined))
                this.ambientColor = this.geometry.materials[0].colorAmbient;
            if (!(this.geometry.materials[0].colorSpecular === undefined))
                this.specularColor = this.geometry.materials[0].colorSpecular;
        }
        this.loaded = true;
    },

    createBuffers: function (gl) {
        this.vbo = gl.createBuffer();
        this.ibo = gl.createBuffer();
        this.nbo = gl.createBuffer();
        this.tbo = gl.createBuffer();

        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.geometry.vertices), gl.STATIC_DRAW);
        this.vbo.itemSize = 3;
        this.vbo.numItems = this.geometry.vertices.length / 3;
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.nbo);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.geometry.normals), gl.STATIC_DRAW);
        this.nbo.itemSize = 3;
        this.nbo.numItems = this.geometry.vertices.length / 3;
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.tbo);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.geometry.uvs[0]), gl.STATIC_DRAW);
        this.tbo.itemSize = 2;
        this.tbo.numItems = this.geometry.uvs[0].length / 2;
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ibo);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(this.geometry.indices), gl.STATIC_DRAW);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

    },

    clone: function () {
        var sceneObject = new SceneObject();
        sceneObject.geometry = this.geometry.clone();
        var i;
        for ( i = 0; i < this.diffuseColor.length; i++) {
            sceneObject.diffuseColor[i] = this.diffuseColor[i];
        }
        for ( i = 0; i < this.specularColor.length; i++) {
            sceneObject.specularColor[i] = this.specularColor[i];
        }
        for ( i = 0; i < this.ambientColor.length; i++) {
            sceneObject.ambientColor[i] = this.ambientColor[i];
        }

        sceneObject.rotationX = this.rotationX;
        sceneObject.rotationY = this.rotationY;
        sceneObject.rotationZ = this.rotationZ;
        sceneObject.loaded = true;
        return sceneObject;
    }
};