﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectOSG.ViewModel
{
    public class ConversationVM
    {
        public int conversationID { get; set; }

        public string Subject { get; set; }

        public int toUserID { get; set; }

        public int fromUserID { get; set; }

        public string tempTo { get; set; }

        public DateTime lastMessageDate { get; set; }
    }
}