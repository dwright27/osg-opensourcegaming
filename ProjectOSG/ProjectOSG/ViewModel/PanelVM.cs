﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectOSG.ViewModel
{
    public class PanelVM
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }
}