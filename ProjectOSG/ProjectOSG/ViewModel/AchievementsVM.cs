﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectOSG.ViewModel
{
    public class AchievementsVM
    {
        public int AchievementID { get; set; }
        public string description { get; set; }
        public string title { get; set; }
        public DateTime dateAchieved { get; set; }
        public int gameID { get; set; }
    }
}