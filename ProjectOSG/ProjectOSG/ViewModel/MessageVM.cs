﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectOSG.ViewModel
{
    public class MessageVM
    {
        public int convoID { get; set; }
        public DateTime Timestamp { get; set; }
        public string sentBy { get; set; }
        public string content { get; set; }
        public bool read { get; set; }
    }
}