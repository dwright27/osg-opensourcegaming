﻿if (jQuery.validator) {
    jQuery.validator.setDefaults({
        debug: true,
        errorClass: 'has-error',
        validClass: 'has-success',
        ignore: "",
        highlight: function (element, errorClass, validClass) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            $(element).closest('.form-group').find('.help-block').text(error.text());
        },
        submitHandler: function (form) {
            if ($(form).valid()) {
                form.submit();
            }
            else {
                return false;
            }
        }
    });
}