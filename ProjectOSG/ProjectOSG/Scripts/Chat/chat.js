﻿var names = [];
var currentDisplay = 0;
var chat;
$(function () {
    // Reference the auto-generated proxy for the hub.  
    chat = $.connection.chatHub;
    chat.client.updateList = function (names) {
        var list = $('#onlineUsers');
        list.empty();
        for (var i = 0; i < names.length; i++)
            list.append('<li>' + names[i] + '</li>');
    };

    var textArea = $('.postTextArea');
    for (var i = 0; i < textArea.length; i++) {
        textAreaAdjust(textArea[i]);
    }

    chat.client.notifyAlert = function (from, message) {
        $('#alerts').html(
            '<div class="alert alert-info"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
        '<a href="../Profile/DisplayName/' + from + '" class="alert-link">' + message + '</a>' +
        '</div>' + $('#alerts').html()
            );
    }




    chat.client.addNewMessageToPage = function (name, message, messages) {
        // Add the message to the page. 
        var found = false;
        $('#discussion' + name).empty();

        for (var i = 0; i < messages.length; i++) {
            var sent = (messages[i].sentBy != name) ? "Me" : messages[i].sentBy;
            $('#discussion' + name).append(sent + " : "
                     + messages[i].content + "\n")
            $('#discussion' + name)[0].scrollTop = $('#discussion' + name)[0].scrollHeight;

        }

        for (var i = 0; i < names.length && !found; i++) {
            if (names[i] == name) {
                $('#discussion' + name).append(htmlEncode(name)
                     + ' : ' + htmlEncode(message) + "\n");
                $('#discussion' + name).parents('.messageContainer').css('display', 'visiable');
                found = true;
            }
        }

        if (!found) {
            add(name);
            $('#discussion' + name).append(htmlEncode(name)
                     + ' : ' + htmlEncode(message) + "\n");
        }


    };






    // Get the user name and store it to prepend to messages.
    //var name = prompt('Enter name:', '');
    //add(name);
    // Set initial focus to message input box.  

    // Start the connection.
    $.connection.hub.start().done(function () {

        $('#listOfUsers').find('li').each(function (index) {
            //$(this).html(index);
            $(this)[0].onclick = function () {
                add($(this).html());

            }
        });


        $('#addPerson').click(function () {

            var name = prompt('Enter name:', '');
            add(name);


        });

        AchievementUnlocked();
    });
});


function notifyUser(username) {
    chat.server.notify(username, username + " : has added a new game ");
}

function htmlEncode(value) {

    var encodedValue = $('<div/>').text(value).html();//('<li>' + value + '</li>');
    return encodedValue;
}


function hideParent(self) {


    $(self).closest('.messageContainer').css('display', 'none');
}



function AchievementUnlocked() {
    var id = $('#GameIDInput').val();
    var input = $('#AchievementInput').val();
    var description = $('#AchieveDescriptionInput').val();
    var points = $('#AchievePointsInput').val();
    chat.server.achievementUnlocked(id, input, description, points);

    if (input != null && input != "" && description != "") {
        $('#alerts').html(
        '<div class="alert alert-info"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
    '<a" class="alert-link">' + input + ' : '  + description + '</a>' +
    '</div>' + $('#alerts').html()
        );

    }
}

function add(name) {

    var container = $('#messageGroup');
    // var container = container.find(".panel-group");
    var discussion = $('#discussion' + name)[0];
    if (discussion == null) {
        container.append(
            '<div class="messageContainer"><div class="pull-left imMessagenger" ><div class="panel panel-default"> <div class="panel-heading"> <h4 class="panel-title">  <a class="imUsername" data-toggle="collapse" data-parent="#messageGroup" href="#collapse' + name + '"> ' + name +
        '</a><a  class="exitLink" onclick="hideParent(this)"><span class="glyphicon glyphicon-remove"></span></a> </h4> </div>' +
    '<div id="collapse' + name + '" class="panel-collapse collapse in">' +
     ' <div class="panel-body">' + ' </div> </div> </div></div></div>'

            );
        var innerPanel = container.find('#collapse' + name);
        innerPanel.append('<input type="hidden" value=displayname' + name + '/>')
        $('#displayname' + name).val(name);
        names.push(name);
        //innerPanel.append('<ul class="personalMessage" id=discussion' + name + '></ul>')
        innerPanel.append('<div><textarea id=discussion' + name + ' class = "scrollTextArea" readonly="readonly" rows="7"></textarea></div>');
        innerPanel.append('<span class="messageInputBox"><input class="messageInput" type="text" id=' + name + 'message' + ' value=""' + ' />')
        innerPanel.append('<button type="button" class="submitMessage" id="' + name + 'sendmessage' + '"><span class="glyphicon glyphicon-pencil"></span></button></span>')
        $('#message' + name).focus();
        //$('#' + name + 'sendmessage').on('click', '#' + name + 'sendmessage', sendMesage);
        //<input type="button" id="sendmessage" value="Send" />

        $('#' + name + 'sendmessage').click(
            function () {
                // Call the Send method on the hub. 

                var name = $(this).attr('id');
                name = name.replace('sendmessage', '');

                //$('#discussion' + name).append('<li><strong>' + "Me"
                //        + '</strong>: ' + $('#' + name + 'message').val() + '</li>');
                $('#discussion' + name).append("Me : " + $('#' + name + 'message').val() + "\n");
                $('#discussion' + name)[0].scrollTop = $('#discussion' + name)[0].scrollHeight;
                chat.server.send(name, $('#' + name + 'message').val());
                // Clear text box and reset focus for next comment. 
                $('#' + name + 'message').val('');
                $('#' + name + 'message').focus();
            }
        );
    }
    else {
        $('#discussion' + name).closest('.messageContainer').css('display', 'block');

    }

    //$("#login-register").click(function () {
    //    $("#Login").


    //('hide');
    //    $("#register").modal('show');
    //});
}

function sendMesage() {
    // Call the Send method on the hub. 
    var name = $(this).val();
    $('#discussion' + name).append('<li><strong>' + "Me"
            + '</strong>: ' + $('#' + name + 'message').val() + '</li>');
    chat.server.send(name, $('#' + name + 'message').val());
    // Clear text box and reset focus for next comment. 
    $('#' + name + 'message').val('');
    $('#' + name + 'message').focus();
}

function textAreaAdjust(o) {
    o.style.height = "1px";
    o.style.height = (25 + o.scrollHeight) + "px";
}


function ratingChange(value) {
    $('#ratingValue').val(value)
}

//var names = [];
//var currentDisplay = 0;
//var chat;
//$(function () {
//    // Reference the auto-generated proxy for the hub.  
//    chat = $.connection.chatHub;

//    chat.client.updateList = function (names) {
//        var list = $('#onlineUsers');
//        list.empty();
//        for (var i = 0; i < names.length; i++)
//            list.append('<li>' + names[i] + '</li>');
//    };

//    chat.client.addNewMessageToPage = function (name, message) {
//        // Add the message to the page. 
//        var found = false;
//        for (var i = 0; i < names.length && !found; i++) {
//            if (names[i] == name) {
//                $('#discussion' + name).append('<li><strong>' + htmlEncode(name)
//                     + '</strong>: ' + htmlEncode(message) + '</li>');
//                found = true;
//            }
//        }

//        if (!found) {
//            add(name);
//            $('#discussion' + name).append('<li><strong>' + htmlEncode(name)
//                     + '</strong>: ' + htmlEncode(message) + '</li>');
//        }


//    };


//    // Get the user name and store it to prepend to messages.
//    //var name = prompt('Enter name:', '');
//    //add(name);
//    // Set initial focus to message input box.  

//    // Start the connection.
//    $.connection.hub.start().done(function () {

//        $('#listOfUsers').find('li').each(function (index) {
//            //$(this).html(index);
//            $(this)[0].onclick = function () {
//                add($(this).html());

//            }
//        });


//        $('#addPerson').click(function () {

//            var name = prompt('Enter name:', '');
//            add(name);


//        });
//    });
//});

//function htmlEncode(value) {

//    var encodedValue = $('<div/>').text(value).html();//('<li>' + value + '</li>');
//    return encodedValue;
//}


//function add(name) {

//    var container = $('#messageGroup');
//    // var container = container.find(".panel-group");
//    container.append(
//        '<li class="pull-right " ><div class="panel panel-default"> <div class="panel-heading"> <h4 class="panel-title">  <a data-toggle="collapse" data-parent="#accordion" href="#collapse' + name + '"> ' + name +
//    '</a> </h4> </div>' +
//'<div id="collapse' + name + '" class="panel-collapse collapse in">' +
// ' <div class="panel-body">' + ' </div> </div> </div></li>'

//        );
//    var innerPanel = container.find('#collapse' + name);
//    innerPanel.append('<input type="hidden" value=displayname' + name + '/>')
//    $('#displayname' + name).val(name);
//    names.push(name);
//    innerPanel.append('<ul class="personalMessage" id=discussion' + name + '></ul>')
//    innerPanel.append('<input type="text" id=' + name + 'message' + ' value=""' + ' />')
//    innerPanel.append('<input type="button" id=' + name + 'sendmessage' + ' value="' + name + '"/>')
//    $('#message' + name).focus();

//    //<input type="button" id="sendmessage" value="Send" />

//    $('#' + name + 'sendmessage').click(function () {
//        // Call the Send method on the hub. 
//        var name = $(this).val();
//        $('#discussion' + name).append('<li><strong>' + "Me"
//                + '</strong>: ' + $('#' + name + 'message').val() + '</li>');
//        chat.server.send(name, $('#' + name + 'message').val());
//        // Clear text box and reset focus for next comment. 
//        $('#' + name + 'message').val('');
//        $('#' + name + 'message').focus();
//    });
//}
