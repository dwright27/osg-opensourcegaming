﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectOSG.DAL;
using ProjectOSG.Models;

namespace ProjectOSG.Controllers
{
    public class FollowerController : Controller
    {
        //
        // GET: /Follower/
        static FollowerRepository FR = new FollowerRepository();
        public ActionResult Index()
        {
            return RedirectToAction("Index", "User");
        }
        public ActionResult Follow(int id)
        {
            Follower fl = new Follower();
            fl.follower = id;
            fl.userID = UserController.GrabID(User.Identity.Name);

            FR.InsertOrUpdate(fl);

            return RedirectToAction("Index", "User");
        }
        static public bool AlreadyFollower(int userID, int friendID)
        {
            foreach (var check in FR.AllFromUser(userID))
            {
                if (check == UserController.GrabName(friendID))
                {
                    return true;
                }
            }

            return false;
        }
        public ActionResult removeFollower(int userID, int friendID)
        {

            using (ProjectOSGContext poc = new ProjectOSGContext())
            {
                var result = poc.Followers.Where(f => f.userID == userID);

                var temp = result.Where(r => r.follower == friendID).FirstOrDefault();
                FR.Delete(temp.relId);
            }
            return RedirectToAction("Index", "User");
        }
    }
}
