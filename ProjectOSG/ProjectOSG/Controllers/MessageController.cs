﻿using ProjectOSG.DAL;
using ProjectOSG.Models;
using ProjectOSG.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ProjectOSG.Controllers
{
    public class MessageController : Controller
    {
        //
        // GET: /Message/
        static MessageRepository MR = new MessageRepository();

        static int currentConv = 0;
        public ActionResult Index(int convId)
        {
            List<MessageVM> send = new List<MessageVM>();
            foreach (var get in MR.AllFromConvo(convId))
            {
                MessageVM mvm = new MessageVM { content = get.content, sentBy = get.sentBy, Timestamp = get.Timestamp };
                send.Add(mvm);
            }
            currentConv = convId;
            return View(send);
        }

        public ActionResult Create()
        {
            MessageVM send = new MessageVM();
            send.convoID = currentConv;
            send.read = false;
            send.sentBy = User.Identity.Name;
            send.Timestamp = DateTime.Now;
            return View(send);
        }

        [HttpPost]
        public ActionResult Create(MessageVM MESS)
        {
           // if (ModelState.IsValid)
            //{

            MESS.convoID = currentConv;
            MESS.read = false;
            MESS.sentBy = User.Identity.Name;
            MESS.Timestamp = DateTime.Now;

                Message dataConv = new Message { content=MESS.content, convoID=MESS.convoID, read=MESS.read, sentBy=MESS.sentBy, Timestamp=MESS.Timestamp};
                MR.InsertOrUpdate(dataConv);
                ConversationController.replaceConvoDate(MESS.Timestamp, MESS.convoID);
                return RedirectToAction("Index", "Message", new { convId = dataConv.convoID });
            //}
            //else
            //{
             //   return View();
           // }
        }

        static public bool newMessageCheck(int convID, string name)
        {
            Message check=MR.AllFromConvo(convID).OrderBy(m=>m.Timestamp).LastOrDefault();

            if (check != null)
            {
                return (check.sentBy == name);
            }

            return true;
        }
    }
}
