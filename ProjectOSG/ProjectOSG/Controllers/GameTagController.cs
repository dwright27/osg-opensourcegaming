﻿using ProjectOSG.DAL;
using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectOSG.Controllers
{
    public class GameTagController : Controller
    {
        //
        // GET: /GameTag/
        static GameTagRepository GTR = new GameTagRepository();

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult addTag(int belongerID)
        {
            GameTag newGT = new GameTag();

            newGT.gameBelongerID = belongerID;

            return View(newGT);
        }

        [HttpPost]
        public ActionResult addTag(GameTag newGT)
        {
            string[] name = Request.Params["tagGets[]"].Split(',');

            foreach (var get in name)
            {
                GameTag addTag = new GameTag { gameBelongerID = newGT.gameBelongerID, TagType = get };

                GTR.InsertOrUpdate(addTag);
            }
            return RedirectToAction("DisplayGame", "Home", new { gameId = newGT.gameBelongerID });
        }

        static public IEnumerable<int> getIDToGameWithTag(string nameOfTag)
        {
            List<int> ret = new List<int>();

            foreach (var get in GTR.All())
            {
                if (get.TagType == nameOfTag)
                {
                    ret.Add(get.gameBelongerID);
                }
            }

            return ret;
        }

        static public IEnumerable<GameTag> GetTagsBelongingToGame(int gameID)
        {
            List<GameTag> ret = new List<GameTag>();

            ret = GTR.All().Where(g => g.gameBelongerID == gameID).ToList();

            return ret;
        }
    }
}
