﻿using ProjectOSG.DAL;
using ProjectOSG.Models;
using ProjectOSG.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectOSG.Controllers
{
    public class RelationshipController : Controller
    {
        //
        // GET: /Relationship/

        static RelationshipRepository RR = new RelationshipRepository();

        public ActionResult Index()
        {
            return RedirectToAction("Index", "User");
        }

        public ActionResult sendRequest(int id)
        {
            if (!AlreadyFriend(UserController.GrabID(User.Identity.Name), id))
            {
                Relationship rel = new Relationship();
                rel.friendID = id;
                rel.userID = UserController.GrabID(User.Identity.Name);
                rel.accepted = false;

                RR.InsertOrUpdate(rel);

                return View();
            }

            return View();
        }

        public ActionResult manageFriends(int id)
        {
            return View();
        }

        public ActionResult sendManageRequest(string friendAccept)
        {
            Relationship change = RR.FindRequest(friendAccept, User.Identity.Name);

            change.accepted = true;

            RR.InsertOrUpdate(change);

            Relationship rel = new Relationship();
            rel.friendID = UserController.GrabID(friendAccept);
            rel.userID = UserController.GrabID(User.Identity.Name);
            rel.accepted = true;

            RR.InsertOrUpdate(rel);

            return RedirectToAction("manageFriends", "Relationship", new { id = UserController.GrabID(User.Identity.Name) });
        }

        public ActionResult removeManageRequest(string friendRemove)
        {
            Relationship change = RR.FindRequest(friendRemove, User.Identity.Name);

            RR.Delete(change.relId);

            Relationship changeTwo = RR.FindRequest(User.Identity.Name, friendRemove);

            RR.Delete(changeTwo.relId);

            return RedirectToAction("manageFriends", "Relationship", new { id = UserController.GrabID(User.Identity.Name) });
        }

        static public IEnumerable<string> GrabFriends(int userID)
        {
            List<string> rvm = new List<string>();

            foreach(var get in RR.AllFromUser(userID))
            {
                RelationshipVM addRVM=new RelationshipVM{friendName=get};
                rvm.Add(addRVM.friendName);
            }

            return rvm;
        }

        static public IEnumerable<RelationshipVM> GrabWantingFriends(int userID)
        {
            List<RelationshipVM> rvm = new List<RelationshipVM>();

            foreach (var get in RR.WaitingForAcceptance(userID))
            {
                RelationshipVM addRVM = new RelationshipVM { friendName = get };
                rvm.Add(addRVM);
            }

            return rvm;
        }

        static public IEnumerable<RelationshipVM> FriendsRequesting(int userID)
        {
            List<RelationshipVM> rvm = new List<RelationshipVM>();

            foreach (var get in RR.GetRequests(userID))
            {
                RelationshipVM addRVM = new RelationshipVM { friendName = get };
                rvm.Add(addRVM);
            }

            return rvm;
        }

        static public bool AlreadyFriend(int userID, int friendID)
        {
            foreach (var check in RR.AllFromUser(userID))
            {
                if (check == UserController.GrabName(friendID))
                {
                    return true;
                }
            }

            return false;
        }

        static public bool SpecificRequestWaiting(int friendID, int userID)
        {
            foreach (var check in RR.GetRequests(userID))
            {
                if (check == UserController.GrabName(friendID))
                {
                    return true;
                }
            }

            foreach (var check in RR.WaitingForAcceptance(userID))
            {
                if (check == UserController.GrabName(friendID))
                {
                    return true;
                }
            }

            return false;
        }

        static public bool RequestWaiting(int userID)
        {
            if (RR.GetRequests(userID).Count > 0)
            {
                return true;
            }

            return false;
        }
    }
}