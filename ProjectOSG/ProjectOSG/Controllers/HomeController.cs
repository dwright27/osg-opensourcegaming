﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectOSG.Models;
using ProjectOSG.ViewModel;
using ProjectOSG.DAL;

namespace ProjectOSG.Controllers
{
    public class HomeController : Controller
    {                                                                  
        //
        // GET: /Home/

        private const int NUM_GAMES_PER_SECTION = 1;
      // GameRepository gr = new GameRepository();

        [AllowAnonymous]
        public ActionResult Index()
        {
            HomePageGames homeGames = new HomePageGames();
            homeGames.MostPlayed = new List<GameVM>();
            homeGames.TopRated = new List<GameVM>();
            homeGames.NewGames = new List<GameVM>();
            homeGames.FeaturedGames = new List<GameVM>();
            foreach (var item in GameController.getAllGames().OrderByDescending(g => g.PlayCount).Take(5))
	        {
	            GameVM temp = new GameVM { creater = item.Creator, description = item.Description, URL = item.URL, gameID = item.Id, rating = item.Rating, releaseDate = item.ReleaseDate, Title = item.Title, Height = item.Height, Width = item.Width };
                homeGames.MostPlayed.Add(temp);
	        }
              foreach (var item in GameController.getAllGames().OrderByDescending(g => g.Rating).Take(5))
	        {
	            GameVM temp = new GameVM { creater = item.Creator, description = item.Description, URL = item.URL, gameID = item.Id, rating = item.Rating, releaseDate = item.ReleaseDate, Title = item.Title, Height = item.Height, Width = item.Width };
                homeGames.TopRated.Add(temp);
	        }   
              foreach (var item in GameController.getAllGames().OrderByDescending(g => g.ReleaseDate).Take(5))
	        {
	            GameVM temp = new GameVM { creater = item.Creator, description = item.Description, URL = item.URL, gameID = item.Id, rating = item.Rating, releaseDate = item.ReleaseDate, Title = item.Title, Height = item.Height, Width = item.Width };
                homeGames.NewGames.Add(temp);
	        }   
                foreach (var item in GameController.getAllGames().Where(g => g.Featured).Take(5))
	        {
	            GameVM temp = new GameVM { creater = item.Creator, description = item.Description, URL = item.URL, gameID = item.Id, rating = item.Rating, releaseDate = item.ReleaseDate, Title = item.Title, Height = item.Height, Width = item.Width };
                homeGames.FeaturedGames.Add(temp);
	        }    
                
                
                
           
            //IEnumerable<Game> games = gr.All();
            //games = games.Where(g => g.Title != "Rayman" && g.Title != "Gears of War");
            //foreach (var g in games)
            //{
            //    gr.Delete(g.Id);
            //}

            return View(homeGames);
        }

        public ActionResult DisplayGame(int gameId)
        {
            ProjectOSG.Models.Game returnGame = GameController.getAllGames().FirstOrDefault(g => g.Id == gameId);
            GameVM gameVM = new GameVM { creater = returnGame.Creator, description = returnGame.Description, file = null, gameID = returnGame.Id, rating = returnGame.Rating, releaseDate = returnGame.ReleaseDate, Title = returnGame.Title, URL = returnGame.URL };
            
            
            return (returnGame == null) ? View("~/Views/Error/Index.cshtml", null)  : View(model: gameVM);
        }
        
        public ActionResult TypeOfGames(string panelTitle, Tag tag)
        {
            ViewBag.Title = panelTitle;
            ViewBag.PanelTitle = panelTitle;
            return View("AllGames");
        }

        public ActionResult MostPlayed()
        {
            ViewBag.Title = "MostPlayed";
            ViewBag.PanelTitle = "Most Played";
            List<GameVM> gvm = new List<GameVM>();
            foreach (var item in GameController.getAllGames().OrderByDescending(g => g.PlayCount))
            {
                GameVM temp = new GameVM { creater = item.Creator, description = item.Description, URL = item.URL, gameID = item.Id, rating = item.Rating, releaseDate = item.ReleaseDate, Title = item.Title, Height = item.Height, Width = item.Width };
                gvm.Add(temp);
            }
            return View("AllGames", gvm);
        }

        public ActionResult TopRated()
        {
            ViewBag.Title = "TopRated";
            ViewBag.PanelTitle = "Top Rated";
            List<GameVM> gvm = new List<GameVM>();
            foreach (var item in GameController.getAllGames().OrderByDescending(g => g.Rating))
            {
                GameVM temp = new GameVM { creater = item.Creator, description = item.Description, URL = item.URL, gameID = item.Id, rating = item.Rating, releaseDate = item.ReleaseDate, Title = item.Title, Height = item.Height, Width = item.Width };
                gvm.Add(temp);
            }
            return View("AllGames", gvm);
        }

        public ActionResult NewGames()
        {
            ViewBag.Title = "NewGames";
            ViewBag.PanelTitle = "New Games";
            List<GameVM> gvm = new List<GameVM>();
            foreach (var item in GameController.getAllGames().OrderByDescending(g => g.ReleaseDate))
            {
                GameVM temp = new GameVM { creater = item.Creator, description = item.Description, URL = item.URL, gameID = item.Id, rating = item.Rating, releaseDate = item.ReleaseDate, Title = item.Title, Height = item.Height, Width = item.Width };
                gvm.Add(temp);
            }
            return View("AllGames", gvm);
        }



        [HttpPost]
        public ActionResult SearchGames()
        {
            IEnumerable<ProjectOSG.Models.Game> gamesReturned = null;
            string title = Request.Params["gameTitle"];
            List<GameVM> gvm = new List<GameVM>();
            if (title != null)
            {
                ViewBag.Link = "Index";
                ViewBag.PanelTitle = "Games Found";
                
                foreach (var item in GameController.getAllGames().Where(g => g.Title.ToLower().Contains(title.ToLower().Trim())))
                {
                    GameVM temp = new GameVM { creater = item.Creator, description = item.Description, URL = item.URL, gameID = item.Id, rating = item.Rating, releaseDate = item.ReleaseDate, Title = item.Title, Height = item.Height, Width = item.Width };
                    gvm.Add(temp);
                }
                
            }

            return (gvm == null) ? View("GameNotFound") : View("AllGames", gvm);
        }
    }
}
