﻿using ProjectOSG.Models;
using ProjectOSG.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;
using ProjectOSG.ViewModel;

namespace ProjectOSG.Controllers
{
    [Authorize]
    public class ConversationController : Controller
    {
        static ConversationRepository CR = new ConversationRepository();

        public ActionResult UserConversations()
        {
            List<ConversationVM> send = new List<ConversationVM>();
            foreach (var pick in CR.All())
            {
                if(pick.fromUserID==UserController.GrabID(User.Identity.Name) || pick.toUserID==UserController.GrabID(User.Identity.Name))
                {
                    //this is where you would do view model stuff
                    ConversationVM cvmPick = new ConversationVM { conversationID = pick.conversationID, fromUserID = pick.fromUserID, lastMessageDate = pick.lastMessageDate, Subject = pick.Subject, toUserID = pick.toUserID };
                    send.Add(cvmPick);
                }
            }
            ViewBag.Model = new ConversationVM();
            ViewBag.fromUserID = UserController.GrabID(User.Identity.Name);
            return View(send.OrderByDescending(d=>d.lastMessageDate));
        }

        public ActionResult LookAtConversation(int id)
        {
            return RedirectToAction("Index", "Message", new { convId = id });
        }

        public ActionResult Create()
        {
            ConversationVM send = new ConversationVM();
            send.fromUserID = UserController.GrabID(User.Identity.Name);
            return View(send);
        }

        [HttpPost]
        public ActionResult Create(ConversationVM CONV)
        {
            if (ModelState.IsValid)
            {
                Conversation dataConv = new Conversation { fromUserID = UserController.GrabID(User.Identity.Name), toUserID = UserController.GrabID(CONV.tempTo), Subject = CONV.Subject, lastMessageDate = DateTime.UtcNow };
                CR.InsertOrUpdate(dataConv);
                return RedirectToAction("UserConversations", "Conversation");
            }
            else
            {
                return View();
            }
        }

        static public void replaceConvoDate(DateTime replace, int index)
        {
            Conversation temp=CR.Find(index);

            temp.lastMessageDate = replace;

            CR.InsertOrUpdate(temp);
        }

        static public bool allNewMessageCheck(string name)
        {
            bool ret;
            foreach (var check in CR.All().Where(m => m.fromUserID == UserController.GrabID(name) || m.toUserID == UserController.GrabID(name)))
            {
                ret=MessageController.newMessageCheck(check.conversationID, name);

                if (!ret)
                {
                    return ret;
                }
            }

            return true;
        }
    }
}