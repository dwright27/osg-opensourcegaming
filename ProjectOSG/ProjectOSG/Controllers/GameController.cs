﻿using ProjectOSG.Models;
using ProjectOSG.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectOSG.ViewModel;
using System.IO;
using System.IO.Compression;
namespace ProjectOSG.Controllers
{
    public class GameController : Controller
    {
        //
        // GET: /Game/
        static GameRepository GR = new GameRepository();

        public ActionResult Index()
        {
            List<GameVM> GVM = new List<GameVM>();

            foreach (var VM in GR.All())
            {
                GameVM temp = new GameVM();
                temp.gameID = VM.Id;
                temp.rating = VM.Rating;
                temp.releaseDate = VM.ReleaseDate;
                temp.Title = VM.Title;

                GVM.Add(temp);
            }
            ViewBag.PanelTitle = "All Games";
            return View("~/Views/Game/AllGames.cshtml", GVM);
        }
        public ActionResult SingleGame(GameVM singleGame)
        {
            return View(singleGame);
        }

        public ActionResult Rate(int gameId)
        {
            Game game = GR.All().FirstOrDefault(g => g.Id == gameId);
            float rating = float.Parse(Request.Form["rating"]);
            double newRating = game.Rating * game.NumberOfRatings + rating;
            int newNumRatings = game.NumberOfRatings + 1;
            newRating = newRating / newNumRatings;
            game.NumberOfRatings = newNumRatings;
            game.Rating = newRating;
            GR.Update(game);
            GameVM gameVM = new GameVM { creater = game.Creator, URL = game.URL, Title = game.Title, releaseDate = game.ReleaseDate, description = game.Description, gameID = game.Id, rating = game.Rating, file = null };


            return View("~/Views/Home/DisplayGame.cshtml", gameVM);
        }

        public ActionResult Display(int gameId)
        {
            GameRepository games = new GameRepository();
            return View(games.Find(gameId));
        }

        public ActionResult DisplayTagged(String tag)
        {
            ViewBag.Title = tag;
            ViewBag.PanelTitle = tag;
            List<GameVM> tagGames = new List<GameVM>();
          //  tagGames = GR.All().Where(g => g);
            
            foreach (var item in GameTagController.getIDToGameWithTag(tag))
            {
                Game game = GR.Find(item);
                if (game != null)
                {
                    GameVM temp = new GameVM();
                    temp.gameID = game.Id;
                    temp.rating = game.Rating;
                    temp.releaseDate = game.ReleaseDate;
                    temp.Title = game.Title;
                    tagGames.Add(temp);
                }
            }

            ViewBag.PanelTitle = tag;
            return View("~/Views/Game/AllGames.cshtml", tagGames);
        }

        public ActionResult MostPlayed()
        {
            GameRepository games = new GameRepository();

            ViewBag.Title = "MostPlayed";
            ViewBag.PanelTitle = "Most Played";
            List<GameVM> gvm = new List<GameVM>();
            foreach (var item in GR.All().OrderByDescending(g => g.PlayCount))
            {
                GameVM temp = new GameVM { creater = item.Creator, description = item.Description, URL = item.URL, gameID = item.Id, rating = item.Rating, releaseDate = item.ReleaseDate, Title = item.Title, Height = item.Height, Width = item.Width };
                gvm.Add(temp);
            }
            return View("AllGames", gvm);
        }

        public ActionResult TopRated()
        {
            GameRepository games = new GameRepository();

            ViewBag.Title = "TopRated";
            ViewBag.PanelTitle = "Top Rated";
            List<GameVM> gvm = new List<GameVM>();
            foreach (var item in GR.All().OrderByDescending(g => g.Rating))
            {
                GameVM temp = new GameVM { creater = item.Creator, description = item.Description, URL = item.URL, gameID = item.Id, rating = item.Rating, releaseDate = item.ReleaseDate, Title = item.Title, Height = item.Height, Width = item.Width };
                gvm.Add(temp);
            }
            return View("AllGames", gvm);
        }

        public ActionResult NewGames()
        {
            GameRepository games = new GameRepository();

            ViewBag.Title = "NewGames";
            ViewBag.PanelTitle = "New Games";
            List<GameVM> gvm = new List<GameVM>();
            foreach (var item in GR.All().OrderByDescending(g => g.ReleaseDate))
            {
                GameVM temp = new GameVM { creater = item.Creator, description = item.Description, URL = item.URL, gameID = item.Id, rating = item.Rating, releaseDate = item.ReleaseDate, Title = item.Title, Height = item.Height, Width = item.Width };
                gvm.Add(temp);
            }
            return View("AllGames", gvm);
        }


        [HttpPost]
        public ActionResult SearchGames()
        {
            IEnumerable<ProjectOSG.Models.Game> gamesReturned = null;
            string title = Request.Params["gameTitle"];
            GameRepository games = new GameRepository();
            List<GameVM> gvm = new List<GameVM>();
            if (title != null)
            {
                ViewBag.Link = "Index";
                ViewBag.PanelTitle = "Games Found";

                foreach (var item in games.All().Where(g => g.Title.ToLower().Contains(title.ToLower().Trim())))
                {
                    GameVM temp = new GameVM { creater = item.Creator, description = item.Description, URL = item.URL, gameID = item.Id, rating = item.Rating, releaseDate = item.ReleaseDate, Title = item.Title, Height = item.Height, Width = item.Width };
                    gvm.Add(temp);
                }

            }

            return (gvm == null) ? View("GameNotFound") : View("AllGames", gvm);
        }
        [HttpGet]
        public ActionResult AddGame()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddGame(GameVM gvm)
        {
            //GR.Insert(gvm);
            var file = Request.Form["file"];
            var url = UploadZip(gvm.file, UserController.GrabID(User.Identity.Name));
            gvm.creater = User.Identity.Name;
            gvm.releaseDate = DateTime.UtcNow;

            if (!String.IsNullOrEmpty(url))
            {
                int tempID = default(int);
                //IEnumerable<Game> tempList = getAllGames();
                gvm.URL = url;
                //if (tempList.OrderByDescending(n => n.Id).Count() > 0)
                //{
                // tempID = tempList.OrderByDescending(n => n.Id).First().Id++;
                GameRepository games = new GameRepository();
                Game game = new Game { Creator = gvm.creater, Description = gvm.description, Instructions = "some instructions", NumberOfRatings = 0, PlayCount = 0, Rating = 0, Title = gvm.Title, ReleaseDate = DateTime.UtcNow, URL = url };
                games.Insert(game);
                gvm.gameID = game.Id;
                //}
            }
            return RedirectToAction("addTag", "GameTag", new { belongerID = gvm.gameID });
        }
        [HttpGet]
        public ActionResult EditGame(string Title)
        {
            var gameResult = GR.All().Where(m => m.Title == Title).FirstOrDefault();
            var result = new GameVM();

            result.gameID = gameResult.Id;
            result.rating = gameResult.Rating;
            result.releaseDate = gameResult.ReleaseDate;
            result.Title = gameResult.Title;

            return View("AddGame", result);
        }
        [HttpPost]
        public ActionResult EditGame(GameVM gvm)
        {
            //GR.Update(gvm);
            return View("SingleGames", gvm);
        }

        static public IEnumerable<Game> getAllGames()
        {
            return GR.All();
        }

        static public string getNameOfGame(int id)
        {
            return GR.Find(id).Title;
        }



        public string UploadZip(HttpPostedFileBase file, int userId)
        {
            int UserId = userId; //Dummy data
            string url = "";

            if (file == null)
            {
                ModelState.AddModelError("File", "Please upload a file.");
            }
            else if (file.ContentLength > 0)
            {
                int MaxContentLength = 1024 * 1024 * 5; //3 MB
                string[] AllowedFileExtensions = new string[] { ".zip" };

                if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
                {
                    ModelState.AddModelError("File", "Please only upload files of type(s): " + string.Join(", ", AllowedFileExtensions));
                }
                else if (file.ContentLength > MaxContentLength)
                {
                    ModelState.AddModelError("File", "Your file is too large, maximum allowed size is: " + MaxContentLength + " MB");
                }
                else
                {
                    //TO:DO
                    var fileName = Path.GetFileName(file.FileName);

                    var fileDirectory = Server.MapPath("~/Content/UploadedGames/");
                    var fullFileDirectory = fileDirectory + UserId.ToString() + "/";

                    if (!System.IO.Directory.Exists(fullFileDirectory))
                    {
                        System.IO.Directory.CreateDirectory(fullFileDirectory);
                    }

                    var path = Path.Combine(Path.GetDirectoryName(fullFileDirectory), fileName);

                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }

                    file.SaveAs(path);
                    var extractPath = path.Remove(path.Length - 3);

                    if (System.IO.Directory.Exists(extractPath))
                    {
                        DirectoryInfo dir = new DirectoryInfo(extractPath);
                        foreach (System.IO.FileInfo filer in dir.GetFiles()) filer.Delete();
                        foreach (System.IO.DirectoryInfo subDirectory in dir.GetDirectories()) subDirectory.Delete(true);
                        System.IO.Directory.Delete(extractPath);
                        System.IO.Directory.CreateDirectory(extractPath);
                    }

                    ZipFile.ExtractToDirectory(path, extractPath);

                    var viewFileDir = Server.MapPath("~/Views/Shared/");
                    var viewFileName = "_" + fileName.Remove(fileName.Length - 4) + ".cshtml";
                    var viewPath = Path.Combine(viewFileDir, viewFileName);

                    if (System.IO.File.Exists(viewPath))
                    {
                        System.IO.File.Delete(viewPath);
                    }

                    System.IO.File.Copy(Path.Combine(extractPath.Remove(extractPath.Length - 1), viewFileName), viewPath);

                    url = extractPath.Remove(extractPath.Length - 1);

                    ModelState.Clear();
                    ViewBag.Message = "File uploaded successfully";
                }
            }

            return url;
        }
    }
}
