﻿using ProjectOSG.DAL;
using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectOSG.Controllers
{
    public class PostController : Controller
    {
        //
        // GET: /Post/

        static private PostRepository postsRepo = new PostRepository();


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DisplayAll(int threadId)
        {
            ThreadRepository threadRepo = new ThreadRepository();

            ViewBag.Title = threadRepo.Find(threadId).TopicName;
            ViewBag.ThreadId = threadId;
            IEnumerable<Post> posts = postsRepo.All();
            return View(postsRepo.All().Where(p=>p.ThreadId == threadId));
        }

        public ActionResult Create(int threadId)
        {
            return View(threadId);
        }

        [HttpPost]
        public ActionResult Create(Post post)
        {
            post.PostDate = DateTime.UtcNow;
            post.Username = User.Identity.Name;
            ThreadRepository threads = new ThreadRepository();
            Thread thread = threads.All().FirstOrDefault(t => t.Id == post.ThreadId);
            thread.LastModified = post.PostDate;
            threads.Update(thread);

            ViewBag.Title = thread.TopicName;
            ViewBag.ThreadId = post.ThreadId;
            postsRepo.Insert(post);
            
            return View("DisplayAll",postsRepo.All().Where(p =>post.ThreadId == p.ThreadId));
        }


        [HttpPost]
        public ActionResult Edit(Post post)
        {
            //post.PostDate = DateTime.UtcNow;
            postsRepo.Update(post);
            ThreadRepository threads = new ThreadRepository();
            Thread thread = threads.All().FirstOrDefault(t => t.Id == post.ThreadId);
            thread.LastModified = post.PostDate;
            threads.Update(thread);

            ViewBag.Title = thread.TopicName;
            ViewBag.ThreadId = post.ThreadId;
            return View("DisplayAll", postsRepo.All().Where(p => post.ThreadId == p.ThreadId));
        }
    }
}
