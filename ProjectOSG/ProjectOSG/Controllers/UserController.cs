﻿using ProjectOSG.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectOSG.Models;
namespace ProjectOSG.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/

        static UserRepository UR = new UserRepository();

        public ActionResult Index()
        {
            return View(UR.All());
        }
        [HttpPost]
        public ActionResult searchResult()
        {

            List<User> us = new List<User>();
            string name = Request.Params["SearchResult"];
            if (name != null)
            {
                using (ProjectOSGContext poc = new ProjectOSGContext())
                {
                    us.AddRange(poc.Users.Where(p => p.UserName.Contains(name)).ToList());
                }
            }

            return View("Index", us);

        }

        static public string GrabName(int id)
        {
            string ret=UR.Find(id).UserName;

            return ret;
        }

        static public int GrabID(string name)
        {
            int ret  = 0;

            if(!string.IsNullOrEmpty(name))
                return UR.Find(name).Id;

            return ret;
        }
    }
}
