﻿using ProjectOSG.DAL;
using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectOSG.Controllers
{
    public class AchievementController : Controller
    {
        //
        // GET: /Achievement/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public void GamePage(int gameID, string achievId, string description, int points)
        {
            AchievementRepository repo = new AchievementRepository();
            Achievement temp = repo.FindByGameIdAndName(gameID, achievId);

            if (temp == null)
            {
                repo.Insert(new Achievement{GameBelongerID = gameID,Description = description, AchievementPoints = points, Name = achievId});
                temp = repo.FindByGameIdAndName(gameID, achievId);
            
            }
            UserAchievementRepository userRepo = new UserAchievementRepository();

            if (!userRepo.UserHasAchieve(UserController.GrabID(User.Identity.Name), temp.AchievementID))
            {
                userRepo.Insert(new UserAchievement { AchievementBelongerID = temp.AchievementID, UserBelongerID = UserController.GrabID(User.Identity.Name) });
            }
            //return View(model: vm);
        }




    }
}
