﻿using ProjectOSG.DAL;
using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectOSG.Controllers
{
    public class TagController : Controller
    {
        //
        // GET: /Tag/
        static TagRepository TR = new TagRepository();

        public ActionResult Index()
        {
            return View();
        }

        static public IEnumerable<string> GrabAllTags()
        {
            List<string> ret=new List<string>();

            foreach(var get in TR.All())
            {
                ret.Add(get.tagType);
            }

            return ret;
        }
    }
}
