﻿using OSG.ViewModel;
using ProjectOSG.DAL;
using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectOSG.Controllers
{
    public class ProfileController : Controller
    {
        //
        // GET: /Profile/

        static ProfileRepository pr = new ProfileRepository();
        UserRepository ur = new UserRepository();

        public ActionResult ProfilePage()
        {
            ur.Find("Cvach");
            User temp = ur.Find(User.Identity.Name);
            Profile pv = pr.Find(temp.Id);
            if (pv == null)
            {
                pr.Insert(new Profile { AboutMe = "", FirstName = User.Identity.Name, LastName = "", UserID = temp.Id });
                pv = pr.Find(temp.Id);
            }
            return View(model: pv);
        }

        public ActionResult Display(User user)
        {
            Profile profile = pr.Find(user.Id);
            List<User> friends = new List<User>();

            ProfileVM profileVM;

            if (profile == null)
            {
                pr.Insert(new Profile { AboutMe = "", FirstName = User.Identity.Name, LastName = "", UserID = user.Id });
                profile = pr.Find(user.Id);
            }

            foreach (var name in RelationshipController.GrabFriends(user.Id))
            {
                friends.Add(ur.Find(name));
            }


            profileVM = new ProfileVM { userID = user.Id, profileID = profile.ProfileID, firstName = profile.FirstName, lastName = profile.LastName, AboutMe = profile.AboutMe };
            profileVM.Friends = friends;


            return View("~/Views/Profile/Display.cshtml",model: profileVM);
        }


        public ActionResult DisplayName(string username)
        {
            return Display(ur.Find(username));
        }

        [HttpGet]
        public ActionResult ProfileEditPage()
        {
            User temp = ur.Find(User.Identity.Name);
            Profile pv = pr.Find(temp.Id);
            return View(model: pv);
        }

        [HttpPost]
        public ActionResult ProfileEditPage(ProfileVM pv)
        {
            Profile p = new Profile { AboutMe = pv.AboutMe, FirstName = pv.firstName, LastName = pv.lastName, ProfileID = pv.profileID, UserID = pv.userID };
            pr.Update(p);


            User user = ur.All().FirstOrDefault(u => p.UserID == u.Id);
            Profile profile = pr.Find(user.Id);
            List<User> friends = new List<User>();

            ProfileVM profileVM;

            if (profile == null)
            {
                pr.Insert(new Profile { AboutMe = "", FirstName = User.Identity.Name, LastName = "", UserID = user.Id });
                profile = pr.Find(user.Id);
            }

            foreach (var name in RelationshipController.GrabFriends(user.Id))
            {
                friends.Add(ur.All().FirstOrDefault(u => u.Id == UserController.GrabID(name)));
            }


            profileVM = new ProfileVM { userID = user.Id, profileID = profile.ProfileID, firstName = profile.FirstName, lastName = profile.LastName, AboutMe = profile.AboutMe };
            profileVM.Friends = friends;


            return View("Display", model: profileVM);
        }
    }
}
