﻿using ProjectOSG.DAL;
using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectOSG.Controllers
{
    public class ThreadController : Controller
    {
        //
        // GET: /Thread/
        private static ThreadRepository threadRepo = new ThreadRepository();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DisplayAll()
        {
          
            return View(threadRepo.All());
        }

        public ActionResult Display(int threadId)
        {
            return View(threadRepo.All().FirstOrDefault(t => t.Id == threadId));
        }

        public ActionResult Create()
        {
            string username = User.Identity.Name;
            return  string.IsNullOrEmpty(username) ? View("~/Views/Account/Login.cshtml"): View();
        }
        [HttpPost]
        public ActionResult Create(Thread thread)
        {
            thread.Username = User.Identity.Name;
            thread.Started = DateTime.UtcNow;
            thread.LastModified = DateTime.UtcNow;
           
            threadRepo.Insert(thread);
            ViewBag.ThreadId = thread.Id;
            
            PostRepository posts = new PostRepository();
            return View("~/Views/Post/DisplayAll.cshtml",posts.All().Where(p=>p.ThreadId== thread.Id));
        }

        public ActionResult Remove(int threadId)
        {
            threadRepo.Delete(threadId);
            return View("DisplayAll",(threadRepo.All()));
        }

        public ActionResult Edit(int threadId)
        {

            Thread thread =threadRepo.All().FirstOrDefault(t => t.Id == threadId);
            ViewBag.Name = thread.TopicName;
            ViewBag.Description = thread.Description;
            return View(thread);
        }

        [HttpPost]
        public ActionResult Edit(Thread thread)
        {
            thread.LastModified = DateTime.UtcNow;
            
            threadRepo.Update(thread);

            return View("DisplayAll",threadRepo.All());
        }


        public static Thread Find(int id)
        {
            return threadRepo.All().FirstOrDefault(t => t.Id == id);
        }
    }
}
