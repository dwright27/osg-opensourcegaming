﻿using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace ProjectOSG.DAL
{
    public class MessageRepository : IRepository<Message>
    {
        public IEnumerable<Message> All()
        {
            using (var db = new ProjectOSGContext())
            {
                return db.Messages.ToList();
            }
        }

        public List<Message> AllFromConvo(int convID)
        {
            using (var db = new ProjectOSGContext())
            {
                List<Message> ret=new List<Message>();
                foreach (var data in db.Messages)
                {
                    if (data.convoID == convID)
                    {
                        ret.Add(data);
                    }
                }

                return ret;
            }
        }

        public IEnumerable<Message> AllIncluding(params Expression<Func<Message, object>>[] includeProperties)
        {
            using (var db = new ProjectOSGContext())
            {
                List<Message> query = db.Messages.ToList();
                foreach (var includeProperty in includeProperties)
                {
                    query = query;
                }
                return query;
            }
        }

        public Message Find(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                return db.Messages.Find(id);
            }
        }

        public void Insert(Message insert)
        {

        }

        public void Update(Message update)
        {

        }

        public void InsertOrUpdate(Message info)
        {
            using (var db = new ProjectOSGContext())
            {
                if (info.messageID == default(int))
                {
                    // New entity
                    db.Messages.Add(info);
                }
                else
                {
                    // Existing entity
                    db.Entry(info).State = System.Data.Entity.EntityState.Modified;
                }

                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                var info = db.Messages.Find(id);
                db.Messages.Remove(info);
                db.SaveChanges();
            }
        }
    }
}