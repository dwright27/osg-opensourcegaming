﻿using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace ProjectOSG.DAL
{
    public class ConversationRepository : IConversationRepository
    {
        public IEnumerable<Conversation> All()
        {
            using (var db = new ProjectOSGContext())
            {
                return db.Conversations.ToList();
            }
        }

        public IEnumerable<Conversation> AllIncluding(params Expression<Func<Conversation, object>>[] includeProperties)
        {
            using (var db = new ProjectOSGContext())
            {
                List<Conversation> query = db.Conversations.ToList();
                foreach (var includeProperty in includeProperties)
                {
                    query = query;
                }
                return query;
            }
        }

        public Conversation Find(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                return db.Conversations.Find(id);
            }
        }

        public void InsertOrUpdate(Conversation info)
        {
            using (var db = new ProjectOSGContext())
            {
                if (info.conversationID == default(int))
                {
                    // New entity
                    db.Conversations.Add(info);
                }
                else
                {
                    // Existing entity
                    db.Entry(info).State = System.Data.Entity.EntityState.Modified;
                }

                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                var info = db.Conversations.Find(id);
                db.Conversations.Remove(info);
                db.SaveChanges();
            }
        }
    }

    public interface IConversationRepository
    {
        IEnumerable<Conversation> All();
        IEnumerable<Conversation> AllIncluding(params Expression<Func<Conversation, object>>[] includeProperties);
        Conversation Find(int id);
        void InsertOrUpdate(Conversation info);
        void Delete(int id);
    }
}