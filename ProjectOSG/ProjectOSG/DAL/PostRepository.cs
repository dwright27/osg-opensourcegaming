﻿using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectOSG.DAL
{
    public class PostRepository : IRepository<Post>
    {
        public IEnumerable<Post> All()
        {
            using (var db = new ProjectOSGContext())
            {
                return db.Posts.ToList();
            }
        }

        public Post Find(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                return db.Posts.Find(id);

            }
        }

        public void Insert(Post insert)
        {
            using (var db = new ProjectOSGContext())
            {
                if (insert.Id == default(int))
                {
                    // New entity
                    db.Posts.Add(insert);
                }
                else
                {
                    // Existing entity
                    db.Entry(insert).State = System.Data.Entity.EntityState.Modified;
                }

                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                var info = db.Posts.Find(id);
                db.Posts.Remove(info);
                db.SaveChanges();
            }
        }

        public void Update(Post update)
        {
            using (var db = new ProjectOSGContext())
            {

                // Existing entity
                db.Entry(update).State = System.Data.Entity.EntityState.Modified;

                db.SaveChanges();
            }
        }
    }
}