﻿using ProjectOSG.Controllers;
using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace ProjectOSG.DAL
{
    public class RelationshipRepository :IRepository<Relationship>
    {
        public IEnumerable<Relationship> All()
        {
            using (var db = new ProjectOSGContext())
            {
                return db.Relationships.ToList();
            }
        }

        public List<string> AllFromUser(int userID)
        {
            using (var db = new ProjectOSGContext())
            {
                List<string> ret = new List<string>();
                foreach (var data in db.Relationships)
                {
                    if (data.userID == userID && data.accepted)
                    {
                        ret.Add(UserController.GrabName(data.friendID));
                    }
                }

                return ret;
            }
        }

        public List<string> AllUser(int userID)
        {
            using (var db = new ProjectOSGContext())
            {
                List<string> ret = new List<string>();
                foreach (var data in db.Relationships)
                {
                    if (data.userID == userID)
                    {
                        ret.Add(UserController.GrabName(data.friendID));
                    }
                }

                return ret;
            }
        }

        public List<string> WaitingForAcceptance(int userID)
        {
            using (var db = new ProjectOSGContext())
            {
                List<string> ret = new List<string>();
                foreach (var data in db.Relationships)
                {
                    if (data.userID == userID && !data.accepted)
                    {
                        ret.Add(UserController.GrabName(data.friendID));
                    }
                }

                return ret;
            }
        }

        public List<string> GetRequests(int userID)
        {
            using (var db = new ProjectOSGContext())
            {
                List<string> ret = new List<string>();
                foreach (var data in db.Relationships)
                {
                    if (data.friendID == userID && !data.accepted)
                    {
                        ret.Add(UserController.GrabName(data.userID));
                    }
                }

                return ret;
            }
        }

        public IEnumerable<Relationship> AllIncluding(params Expression<Func<Relationship, object>>[] includeProperties)
        {
            using (var db = new ProjectOSGContext())
            {
                List<Relationship> query = db.Relationships.ToList();
                foreach (var includeProperty in includeProperties)
                {
                    query = query;
                }
                return query;
            }
        }

        public Relationship Find(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                return db.Relationships.Find(id);
            }
        }

        public Relationship FindRequest(string name, string userName)
        {
            using (var db = new ProjectOSGContext())
            {
                foreach (var check in db.Relationships)
                {
                    if (check.userID == UserController.GrabID(name) && check.friendID == UserController.GrabID(userName))
                    {
                        return check;
                    }
                }

                return null;
            }
        }

        public void Insert(Relationship insert)
        {

        }

        public void Update(Relationship update)
        {

        }

        public void InsertOrUpdate(Relationship info)
        {
            using (var db = new ProjectOSGContext())
            {
                if (info.relId == default(int))
                {
                    // New entity
                    db.Relationships.Add(info);
                }
                else
                {
                    // Existing entity
                    db.Entry(info).State = System.Data.Entity.EntityState.Modified;
                }

                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                var info = db.Relationships.Find(id);
                db.Relationships.Remove(info);
                db.SaveChanges();
            }
        }
    }
}