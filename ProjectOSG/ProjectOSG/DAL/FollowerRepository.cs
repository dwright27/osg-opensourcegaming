﻿using ProjectOSG.Controllers;
using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectOSG.DAL
{
   public class FollowerRepository : IRepository<Follower>
    {
        public IEnumerable<Follower> All()
        {
            using (var db = new ProjectOSGContext())
            {
                return db.Followers.ToList();
            }
        }
        public List<string> AllFromUser(int userID)
        {
            using (var db = new ProjectOSGContext())
            {
                List<string> ret = new List<string>();
                foreach (var data in db.Followers)
                {
                    if (data.userID == userID )
                    {
                        ret.Add(UserController.GrabName(data.follower));
                    }
                }

                return ret;
            }
        }
        public Follower Find(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                return db.Followers.Find(id);
            }
        
        }

        public void Insert(Follower insert)
        {
            
        }
        public void InsertOrUpdate(Follower info)
        {
            using (var db = new ProjectOSGContext())
            {
                if (info.relId == default(int))
                {
                    // New entity
                    db.Followers.Add(info);
                }
                else
                {
                    // Existing entity
                    db.Entry(info).State = System.Data.Entity.EntityState.Modified;
                }

                db.SaveChanges();
            }
        }
        public void Delete(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                var info = db.Followers.Find(id);
                db.Followers.Remove(info);
                db.SaveChanges();
            }
        }

        public void Update(Follower update)
        {
            
        }
    }

}

