﻿using ProjectOSG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectOSG.DAL
{
    public class ProfileRepository : IRepository<Profile>
    {
        public IEnumerable<Profile> All()
        {
            using (var db = new ProjectOSGContext())
            {
                return db.Profiles.ToList();
            }
        }

        public Profile Find(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                return db.Profiles.Where(m=>m.UserID==id).FirstOrDefault();

            }
        }

        public void Insert(Profile insert)
        {
            using (var db = new ProjectOSGContext())
            {
                if (insert.ProfileID == default(int))
                {
                    // New entity
                    db.Profiles.Add(insert);
                }
                else
                {
                    // Existing entity
                    db.Entry(insert).State = System.Data.Entity.EntityState.Modified;
                }

                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var db = new ProjectOSGContext())
            {
                var info = db.Profiles.Find(id);
                db.Profiles.Remove(info);
                db.SaveChanges();
            }
        }

        public void Update(Profile update)
        {
            using (var db = new ProjectOSGContext())
            {

                // Existing entity
                db.Entry(update).State = System.Data.Entity.EntityState.Modified;

                db.SaveChanges();
            }
        }
    }
}