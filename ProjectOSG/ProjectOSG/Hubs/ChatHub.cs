﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using ProjectOSG.Controllers;
using ProjectOSG.DAL;
using ProjectOSG.Models;
using System.Web.Security;
using Microsoft.AspNet.SignalR.Hubs;
namespace ProjectOSG.Hubs

{
    [HubName("chatHub")]
    public class ChatHub : Hub
    {
        //public void Send(string name, string message)
        //{
        //    Clients.All.addNewMessageToPage(name, message);
        //}

        //public void Send(string userId, string message)
        //{
        //    var check = Clients.User(userId);
        //    Clients.User(userId).send(message);
        //}

        public readonly static ConnectionMapping<string> _connections =
      new ConnectionMapping<string>();
        private static UserRepository users = new UserRepository();
        private static MessageRepository messages = new MessageRepository();
        private static ConversationRepository conversations = new ConversationRepository();
        private static FollowerRepository followings = new FollowerRepository();
        public void Send(string who, string message)
        {

            if (!string.IsNullOrEmpty(message))
            {
                string name = Context.User.Identity.Name;
                bool fromExists = UserExists(name);
                bool toExists = UserExists(who);
                if (toExists && fromExists)
                {
                    foreach (var connectionId in _connections.GetConnections(who))
                    {

                        Conversation conversation = conversations.All().FirstOrDefault(c => c.Subject == "IM") == null ? new Conversation { lastMessageDate = DateTime.UtcNow, fromUserID = UserController.GrabID(name), tempTo = who, toUserID = UserController.GrabID(who), Subject = "IM" } : conversations.All().FirstOrDefault(c => c.Subject == "IM");

                        conversations.InsertOrUpdate(conversation);
                        Message newMessage = new Message { Timestamp = DateTime.Now, content = message, sentBy = name, read = true, convoID = conversation.conversationID};
                        var pastMessages = messages.All().Where(m => m.convoID == conversation.conversationID);
                        messages.InsertOrUpdate(newMessage);
                        
                        Clients.Client(connectionId).addNewMessageToPage(name, message, pastMessages.ToArray());

                    }
                    Clients.All.updateList(RelationshipController.GrabFriends(UserController.GrabID(name)));
                }
            }
        }


        public void AchievementUnlocked(int gameID, string achievementName, string description, int points)
        {
            AchievementRepository repo = new AchievementRepository();
            Achievement temp = repo.FindByGameIdAndName(gameID, achievementName);

            if (temp == null)
            {
                repo.Insert(new Achievement { GameBelongerID = gameID, Description = description, AchievementPoints = points, Name = achievementName });
                temp = repo.FindByGameIdAndName(gameID, achievementName);

            }
            UserAchievementRepository userRepo = new UserAchievementRepository();

            if (!userRepo.UserHasAchieve(UserController.GrabID(Context.User.Identity.Name), temp.AchievementID))
            {
                userRepo.Insert(new UserAchievement { AchievementBelongerID = temp.AchievementID, UserBelongerID = UserController.GrabID(Context.User.Identity.Name) });
            }
        }

        public void Notify(string from, string message)
        {
           // Clients.All.NotifyUser

            IEnumerable<Follower> followers = followings.All().Where(f => f.follower == UserController.GrabID(from));
            List<string> followerNames = new List<string>();
            foreach (var f in followers)
            {
                followerNames.Add(UserController.GrabName(f.userID));
            }



            foreach (var f in followerNames)
            {
                foreach (var connectionId in _connections.GetConnections(f))
                {

                    Clients.Client(connectionId).notifyAlert(from, message);

                }
            }
                 

        }



        private bool UserExists(string name)
        {
            return null != users.All().FirstOrDefault(u => u.UserName == name);
        }

        public override Task OnConnected()
        {
            string name = Context.User.Identity.Name;
            if (!string.IsNullOrEmpty(name))
            {
                if(_connections.Users.ToList().IndexOf(name) == -1)
                {
                  _connections.Add(name, Context.ConnectionId);
                  Clients.All.updateList(RelationshipController.GrabFriends(UserController.GrabID(name)));

                }
            }

  
            return base.OnConnected();
        }

        public override Task OnDisconnected()
        {
            string name = Context.User.Identity.Name;

            _connections.Remove(name, Context.ConnectionId);
            Clients.All.updateList(RelationshipController.GrabFriends(UserController.GrabID(name)));

            return base.OnDisconnected();
        }

        public override Task OnReconnected()
        {
            string name = Context.User.Identity.Name;

            if (!_connections.GetConnections(name).Contains(Context.ConnectionId))
            {
                _connections.Add(name, Context.ConnectionId);
            }
            Clients.All.updateList(RelationshipController.GrabFriends(UserController.GrabID(name)));
            


            return base.OnReconnected();
        }

    }
}