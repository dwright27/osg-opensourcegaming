﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjectOSG.Models
{
    [Table("Follower")]
    public class Follower
    {
        [Key]
        [Required]
        public int relId { get; set; }

        [Required]
        public int follower { get; set; }

        [Required]
        public int userID { get; set; }
    }

}