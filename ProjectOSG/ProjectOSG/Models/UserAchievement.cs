﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjectOSG.Models
{
    [Table("UserAchievements")]
    public class UserAchievement
    {
        [Key]
        public int UserAchievementsID { get; set; }
        public int AchievementBelongerID { get; set; }
        public int UserBelongerID { get; set; }

    }
}