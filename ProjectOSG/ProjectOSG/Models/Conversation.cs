﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjectOSG.Models
{
    [Table("Conversations")]
    public class Conversation
    {
        [Key]
        public int conversationID { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        public int toUserID { get; set; }

        [Required]
        public int fromUserID { get; set; }

        public string tempTo { get; set; }

        public DateTime lastMessageDate { get; set; }
    }
}