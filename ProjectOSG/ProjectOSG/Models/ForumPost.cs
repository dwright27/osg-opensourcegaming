﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectOSG.Models
{
    public class ForumPost
    {
        public string Message { get; set; }
        public List<ForumPost> SubPosts { get; set; }

        private static int topicNumber = 0;

        public ForumPost()
        {
            Id = topicNumber++;
            SubPosts = new List<ForumPost>();
        }

        public int Id { get; set; }
    }
}