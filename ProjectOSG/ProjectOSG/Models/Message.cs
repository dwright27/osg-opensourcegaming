﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjectOSG.Models
{
    [Table("Messages")]
    public class Message
    {
        [Key]
        [Required]
        public int messageID { get; set; }

        public int convoID { get; set; }

        [Required]
        public DateTime Timestamp { get; set; }

        [Required]
        public string sentBy { get; set; }

        [Required]
        public string content { get; set; }

        [Required]
        public bool read { get; set; }
    }
}