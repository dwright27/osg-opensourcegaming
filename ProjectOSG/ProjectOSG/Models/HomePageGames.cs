﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectOSG.Models
{
    public class HomePageGames
    {
        public List<ProjectOSG.ViewModel.GameVM> MostPlayed { get; set; }
        public List<ProjectOSG.ViewModel.GameVM> TopRated { get; set; }
        public List<ProjectOSG.ViewModel.GameVM> NewGames { get; set; }
        public List<ProjectOSG.ViewModel.GameVM> FeaturedGames { get; set; }
        
    }
}
