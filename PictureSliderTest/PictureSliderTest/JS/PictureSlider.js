﻿$(document).ready(function () {

    //Canvas stuff
    var canvas = $("#SliderCanvas")[0];
    var ctx = canvas.getContext("2d");
    var w = $("#SliderCanvas").width();
    var h = $("#SliderCanvas").height();
    
    var numDivisions = 4;
    var won = false;
    var cellWidth = w / numDivisions;
    var cellHeight = h / numDivisions;

    var numSections = numDivisions * numDivisions;
    var selectedCellX = 0;
    var selectedCellY = 0;

    var blankCellX = 0;
    var blankCellY = 0;


    var pictureArray = new Array(numDivisions);
    for (var i = 0; i < numDivisions; i++)
    {
        pictureArray[i] = new Array(numDivisions);
    }

    for (var i = 0; i < numDivisions; i++) {
        for (var j = 0; j < numDivisions; j++) {
            pictureArray[i][j] = i + (j * numDivisions);
        }
    }
    
    var img = document.getElementById("picture");
    
    

    function Init()
    {
        randomizeTiles();
        log(String(isSolvable(numDivisions, numDivisions, getEmptyRowY()+1)));
        while (!isSolvable(numDivisions, numDivisions, getEmptyRowY()+1))
        {
            randomizeTiles();
        }
        log(String(isSolvable(numDivisions, numDivisions, getEmptyRowY()+1)));
        if (typeof game_loop != "undefined") clearInterval(game_loop);
        game_loop = setInterval(paint, 60);
    }


    function paint()
    {
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, w, h);
        //ctx.drawImage(img, 90, 130, 50, 60, 10, 10, 50, 60);
        //ctx.drawImage(img, 0, 0);
        
        for (var i = 0; i < numDivisions; i++) {
            for (var j = 0; j < numDivisions; j++) {
                var picX = pictureArray[i][j] % numDivisions;
                var picY = (pictureArray[i][j] - (pictureArray[i][j] % numDivisions)) / numDivisions;
                drawImageCell(picX, picY, i, j);
            }
        }
            
        if (!won) {
            paintDebugCell(selectedCellX, selectedCellY, "selector");
        }
        else
        {
            ctx.fillStyle = "SkyBlue";
            ctx.fillRect(w / 3, (h / 5) * 2.5, w / 3, (h / 10));
            ctx.fillStyle = "Black"
            ctx.font = "20px Georgia";
            ctx.fillText("Victory!", h/2-30, (w/2)+30);
        }

    }

    function drawImageCell(picX, picY, x, y)
    {
        if (picX == 0 && picY == 0 && !won)
        {
            ctx.fillStyle = "white";
            ctx.fillRect(x * cellWidth, y * cellHeight, cellWidth, cellHeight);
            ctx.strokeStyle = "black";
            ctx.strokeRect(x * cellWidth, y * cellHeight, cellWidth, cellHeight);
        }
        else
        {
            ctx.drawImage(img, picX * cellWidth, picY * cellHeight, cellWidth, cellHeight, x * cellWidth, y * cellHeight, cellWidth, cellHeight);
            if (!won) {
                ctx.strokeStyle = "black";
                ctx.strokeRect(x * cellWidth, y * cellHeight, cellWidth, cellHeight);
            }
        }
    }

    function paintDebugCell(x, y, text) {
        ctx.strokeStyle = "yellow";
        ctx.strokeRect(x * cellWidth, y * cellHeight, cellWidth, cellHeight);
        ctx.strokeRect((x * cellWidth) + 1, (y * cellHeight) + 1, cellWidth - 2, cellHeight - 2);
        ctx.strokeRect((x * cellWidth) + 2, (y * cellHeight) + 2, cellWidth - 4, cellHeight - 4);
    }

    function moveSelectorLeft() {
        if (selectedCellX > 0) selectedCellX--;
    }

    function moveSelectorRight() {
        if (selectedCellX < numDivisions-1) selectedCellX++;
    }

    function moveSelectorUp() {
        if (selectedCellY > 0) selectedCellY--;
    }

    function moveSelectorDown() {
        if (selectedCellY < numDivisions-1) selectedCellY++;
    }

    function selectSpace(xOne,yOne)
    {
        
        var tx = xOne;
        var ty = yOne;
        if (tx > 0 && pictureArray[tx - 1][ty] == 0) {
            pictureArray[tx - 1][ty] = pictureArray[tx][ty];
            pictureArray[tx][ty] = 0;
        }
        else
        if (ty > 0 && pictureArray[tx][ty - 1] == 0) {
            pictureArray[tx][ty - 1] = pictureArray[tx][ty];
            pictureArray[tx][ty] = 0;
        }
            else
        if (tx < numDivisions-1 && pictureArray[tx +1][ty] == 0) {
            pictureArray[tx + 1][ty] = pictureArray[tx][ty];
            pictureArray[tx][ty] = 0;
        }
        else
        if (ty < numDivisions - 1 && pictureArray[tx][ty+1] == 0) {
            pictureArray[tx][ty + 1] = pictureArray[tx][ty];
            pictureArray[tx][ty] = 0;
        }

    }

    function swapTiles(i, j, x, y)
    {
        var temp = pictureArray[i][j];
        pictureArray[i][j] = pictureArray[x][y];
        pictureArray[x][y] = temp;
    }

    function randomizeTiles()
    {
        var i = numSections - 1;
        while (i > 0)
        {
            var j = Math.floor(Math.random() * i);
            var xi = i % numDivisions;
            var yi = Math.floor(i / numDivisions);
            var xj = j % numDivisions;
            var yj = Math.floor(j / numDivisions);
            swapTiles(xi, yi, xj, yj);
            --i;
        }
    }

    function countInversions(i, j)
    {
        var inversions = 0;
        var tile = pictureArray[i][j];
        if (tile != 0) {
            for (var q = tile + 1; q < numSections; q++) {
                var k = q % numDivisions;
                var l = Math.floor(q / numDivisions);

                var compValue = pictureArray[k][l];
                if (tile > compValue && tile != (numSections - 1)) {
                    inversions++;
                }
            }
        }
        return inversions;
    }

    function sumAllInversions() {
        var inversions = 0;
        for (var j = 0; j < numDivisions; j++)
        {
            for (var i = 0; i < numDivisions; i++)
            {
                if (pictureArray[i][j] != 0) {
                    inversions += countInversions(i, j);
                }
            }
        }
        return inversions;
    }

    function checkVictory() {
        var done = true;
        for (var i = 0; i < numDivisions; i++) {
            for (var j = 0; j < numDivisions; j++) {
                if (!(pictureArray[i][j] == i + (j * numDivisions))) {
                    done = false;
                }
            }
        }

        won = done;
    }


    function isSolvable(width, height, emptyRow) {
        if (width % 2 == 1) {
            return (sumAllInversions() % 2 == 0)
        } else {
            return ((sumAllInversions() + height - emptyRow) % 2 == 0)
        }
    }

    

    function getEmptyRowX() {
        var ret = 0;
        for (var i = 0; i < numDivisions; i++) {
            for (var j = 0; j < numDivisions; j++) {
                if (pictureArray[i][j] == 0) {
                    ret = i;
                }
            }
        }
        return ret;
    }

    function getEmptyRowY() {
        var ret = 0;
        for (var i = 0; i < numDivisions; i++) {
            for (var j = 0; j < numDivisions; j++) {
                if (pictureArray[i][j] == 0) {
                    ret = j;
                }
            }
        }
        return ret;
    }

    function log(msg) {
        setTimeout(function () {
            throw new Error(msg);
        }, 0);
    }

    $(document).keydown(function (e) {
        if (!won) {
            var key = e.which;
            
            if (key == "37") 
            {
                moveSelectorLeft();
            }
            else if (key == "38") {
                moveSelectorUp();
            }
            else if (key == "39") {
                moveSelectorRight();
            }
            else if (key == "40") {
                moveSelectorDown();
            }
            else if (key == "32") {

                selectSpace(selectedCellX, selectedCellY);
                checkVictory();
            }
        }
    })

    Init();
})